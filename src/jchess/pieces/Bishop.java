package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.RunMovesCollector;

/**
 * Class to represent the bishop piece.
 * The bishop can move diagonally across the chessboard. <br>
 *
 *|_|_|_|_|_|_|_|X|7	<br>
 *|X|_|_|_|_|_|X|_|6	<br>
 *|_|X|_|_| |X|_|_|5	<br>
 *|_|_|X|_|X|_|_|_|4	<br>
 *|_|_|_|B|_|_|_|_|3	<br>
 *|_| |X|_|X|_|_|_|2	<br>
 *|_|X|_|_|_|X|_|_|1	<br>
 *|X|_|_|_|_|_|X|_|0	<br>
 * 0 1 2 3 4 5 6 7  	<br>
 */
public class Bishop extends Piece
{   
	/**
	 * Method for initializing the bishop. For reference he gets the symbol 'B'.
	 * @param chessboard it is needed to determine the possible positions of the bishop
	 * @param player the player assigned to that bishop
	 */
    public Bishop(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("B");
        this.setImage();   
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color){
        	case white:
        		image = FileLoader.loadImageFromTheme("Bishop-W.png");
        		break;
        	case black: 
        		image = FileLoader.loadImageFromTheme("Bishop-B.png");
        		break;
        	case gray: 
        		image = FileLoader.loadImageFromTheme("Bishop-G.png");
        		break;
        	default:
        		break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the bishop.
     * @return the list of possible moves
     */  
    public ArrayList<Square> allMoves(){
    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
    	
    	RunMovesCollector rmc = new RunMovesCollector(this);
    	
    	//-1 if piece has unlimited range
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.northwest,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.northeast,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.southwest,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.southeast,false,false,false));

    	return this.composeMoves(movelist);
    }
}
