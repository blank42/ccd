/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sawomir Lach ( matlak, msl )
 * Damian Marciniak
 */

package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.RunMovesCollector;

/**
 * Class to represent the rook piece. <br>
 * <br>
 * Moves: <br>
 * |_|_|_|X|_|_|_|_|7	<br>
 * |_|_|_|X|_|_|_|_|6	<br>
 * |_|_|_|X|_|_|_|_|5	<br>
 * |_|_|_|X|_|_|_|_|4	<br>
 * |X|X|X|B|X|X|X|X|3	<br>
 * |_|_|_|X|_|_|_|_|2	<br>
 * |_|_|_|X|_|_|_|_|1	<br>
 * |_|_|_|X|_|_|_|_|0	<br>
 *  0 1 2 3 4 5 6 7		<br>
 */
public class Rook extends Piece
{

    public boolean wasMotion = false;

    /**
	 * Method for initializing the rook. For reference he gets the symbol 'R'.
	 * @param chessboard it is needed to determine the possible positions of the rook
	 * @param player the player assigned to that rook
	 */
    public Rook(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("R");;
        this.setImage();
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color)
        {
		    case white:
		    	image = FileLoader.loadImageFromTheme("Rook-W.png");
		    	break;
		    case black:
		    	image = FileLoader.loadImageFromTheme("Rook-B.png");
		    	break;
		    case gray:
		    	image = FileLoader.loadImageFromTheme("Rook-G.png");
		    	break;
			default:
				break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the queen.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {
    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
        
    	RunMovesCollector rmc = new RunMovesCollector(this);
    	//-1 if piece has unlimited range
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.north, false, false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.south, false, false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.west, false, false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.east, false, false,false));


        return this.composeMoves(movelist);
    }
}
