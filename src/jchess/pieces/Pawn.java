package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.RunMovesCollector;

/**
 * Class to represent the pawn piece.
 * It can move only foreward and can beat only across.
 * If it is the first move then it can move 2 squares. <br>
 * <br>
 * First move:			<br>
 * |_|_|_|_|_|_|_|_|7	<br>
 * |_|_|_|_|_|_|_|_|6	<br>
 * |_|_|_|X|_|_|_|_|5	<br>
 * |_|_|_|X|_|_|_|_|4	<br>
 * |_|_|_|P|_|_|_|_|3	<br>
 * |_|_|_|_|_|_|_|_|2	<br>
 * |_|_|_|_|_|_|_|_|1	<br>
 * |_|_|_|_|_|_|_|_|0	<br>
 *  0 1 2 3 4 5 6 7		<br>
 * <br>
 * Normal move:			<br>
 * |_|_|_|_|_|_|_|_|7	<br>
 * |_|_|_|_|_|_|_|_|6	<br>
 * |_|_|_|_|_|_|_|_|5	<br>
 * |_|_|_|X|_|_|_|_|4	<br>
 * |_|_|_|P|_|_|_|_|3	<br>
 * |_|_|_|_|_|_|_|_|2	<br>
 * |_|_|_|_|_|_|_|_|1	<br>
 * |_|_|_|_|_|_|_|_|0	<br>
 *  0 1 2 3 4 5 6 7		<br>
 * <br>
 * Capture moves:		<br>
 * |_|_|_|_|_|_|_|_|7	<br>
 * |_|_|_|_|_|_|_|_|6	<br>
 * |_|_|_|_|_|_|_|_|5	<br>
 * |_|_|X|_|X|_|_|_|4	<br>
 * |_|_|_|P|_|_|_|_|3	<br>
 * |_|_|_|_|_|_|_|_|2	<br>
 * |_|_|_|_|_|_|_|_|1	<br>
 * |_|_|_|_|_|_|_|_|0	<br>
 *  0 1 2 3 4 5 6 7		<br>
 */
public class Pawn extends Piece
{

    boolean otherside;
    private Square previousSquare;
    Chessboard.cardinalDirections movedir;

    /**
	 * Method for initializing the pawn. For reference he gets the symbol ''.
	 * @param chessboard it is needed to determine the possible positions of the pawn
	 * @param player the player assigned to that pawn
	 */
    public Pawn(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.previousSquare=this.square;
        this.setSymbol("");
        this.setImage();
        
        this.setMoveDir(false);
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color)
        {
	        case white:
	        	image = FileLoader.loadImageFromTheme("Pawn-W.png");
	        	break;
	        case black:
	        	image = FileLoader.loadImageFromTheme("Pawn-B.png");
	        	break;
	        case gray:
	        	image = FileLoader.loadImageFromTheme("Pawn-G.png");
	        	break;
			default:
				break;
        }
        orgImage = image;
    }

    /**
     * Method that sets the moving direction of the pawn.
     * @param ifOtherside if true then the pawn crossed the middle of the circular chessboard
     */
    public void setMoveDir(boolean ifOtherside){
    	
    	this.otherside=ifOtherside;

    	if((this.getChessboard().getPlayercount()==2 && !this.getPlayer().goDown) || otherside)
    	{
    		movedir=Chessboard.cardinalDirections.south;
    	}
    	else
    	{
    		movedir=Chessboard.cardinalDirections.north;
    	}
    }
    
    /**
     * Method that provides the moves for capturing a piece in a specified direction
     * normally and via en passant.
     * @param direction Direction you look if you can capture
     * @return List filled with capture moves
     */
    public ArrayList<Square> CaptureMoves(Chessboard.cardinalDirections direction){
    	
    	ArrayList<Square> list= new ArrayList<Square>();
    	
    	//out of bounds protection
    	if (this.square.getSquareInDir(movedir).getSquareInDir(direction) != null)
        {
            //capture
            Square sq = this.square.getSquareInDir(movedir).getSquareInDir(direction);
            if (sq.getPiece() != null)
            {
            	//check if can hit left
                if (this.getPlayer() != sq.getPiece().getPlayer() && !sq.getPiece().name.equals("King"))
                {
                    if (this.getChessboard().getKing(this.getPlayer().color).willBeSafeWhenMoveOtherPiece(this.square, sq))
                    {
                        list.add(sq);
                    }
                }
            }

            
            //En passant
            Square epsq = this.square.getSquareInDir(direction);
            if (epsq.getPiece() != null
                    && this.getChessboard().twoSquareMovedPawn != null
                    && epsq == this.getChessboard().twoSquareMovedPawn.square)
            {
            	//check if can hit left
                if (this.getPlayer() != epsq.getPiece().getPlayer() && !epsq.getPiece().name.equals("King"))
                {
                    if (this.getChessboard().getKing(this.getPlayer().color).willBeSafeWhenMoveOtherPiece(this.square, this.square.getSquareInDir(movedir).getSquareInDir(direction)))
                    {
                        list.add(this.square.getSquareInDir(movedir).getSquareInDir(direction));
                    }
                }
            }
        }
    	return list;
    }
    
    /**
     * Method that creates a list of possible moves for the pawn.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {
    	RunMovesCollector rmc = new RunMovesCollector(this);
    	
    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
    	
        movelist.add(rmc.movesInDir(this.square, 1, movedir,false,false, true));
    	

        if (((this.getPlayer().goDown 
        		|| this.getChessboard().getPlayercount()>2) && this.square.getPozY() == 1) 
        		|| (!this.getPlayer().goDown && this.square.getPozY() == 6))				//if pawn is still on start position
        {
        	
        	movelist.add(rmc.movesInDir(this.square, 2, movedir,false,false, true));
        }

        String diagDirEast=movedir.toString()+"east";
        String diagDirWest=movedir.toString()+"west";
        
        
        movelist.add(rmc.movesInDir(this.square, 1, Chessboard.cardinalDirections.valueOf(diagDirEast),false,true, false));
        movelist.add(rmc.movesInDir(this.square, 1, Chessboard.cardinalDirections.valueOf(diagDirWest),false,true, false));


        return this.composeMoves(movelist);
    }

    
    /**
     * Method that changes the pawn to another piece if the pawn has reached the end of the
     * chessboard.
     * @param newPiece the piece to which the pawn changes
     */
    void promote(Piece newPiece)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Method that tells if the pawn already reached the other side on circular chessboard.
     * @return true if that is the case, false if not
     */
	public boolean isOtherside() {
		return otherside;
	}

	public void setOtherside(boolean otherside) {
		this.otherside = otherside;
	}

	public Chessboard.cardinalDirections getMovedir() {
		return movedir;
	}

	public void setMovedir(Chessboard.cardinalDirections movedir) {
		this.movedir = movedir;
	}

	public Square getPreviousSquare() {
		return previousSquare;
	}

	public void setPreviousSquare(Square previousSquare) {
		this.previousSquare = previousSquare;
	}
    
}
