package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.RunMovesCollector;

/**
 * Class to represent a queen piece. <br>
 * <br>
 * Moves: <br>
 * |_|_|_|X|_|_|_|X|7	<br>
 * |X|_|_|X|_|_|X|_|6	<br>
 * |_|X|_|X|_|X|_|_|5	<br>
 * |_|_|X|X|x|_|_|_|4	<br>
 * |X|X|X|Q|X|X|X|X|3	<br>
 * |_|_|X|X|X|_|_|_|2	<br>
 * |_|X|_|X|_|X|_|_|1	<br>
 * |X|_|_|X|_|_|X|_|0	<br>
 *  0 1 2 3 4 5 6 7		<br>
 */
public class Queen extends Piece
{
	/**
	 * Method for initializing the queen. For reference he gets the symbol 'Q'.
	 * @param chessboard it is needed to determine the possible positions of the queen
	 * @param player the player assigned to that queen
	 */
    public Queen(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("Q");
        this.setImage();
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color)
        {
	        case white:
	        	image = FileLoader.loadImageFromTheme("Queen-W.png");
	        	break;
	        case black:
	        	image = FileLoader.loadImageFromTheme("Queen-B.png");
	        	break;
	        case gray:
	        	image = FileLoader.loadImageFromTheme("Queen-G.png");
	        	break;
			default:
				break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the queen.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {
    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
        
    	RunMovesCollector rmc = new RunMovesCollector(this);
    	//-1 if piece has unlimited range
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.northwest,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.northeast,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.southwest,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.southeast,false,false,false));
		
		
    	//-1 if piece has unlimited range
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.north,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.south,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.west,false,false,false));
    	movelist.add(rmc.movesInDir(this.square, -1, Chessboard.cardinalDirections.east,false,false,false));

    	
        return this.composeMoves(movelist);
    }
}