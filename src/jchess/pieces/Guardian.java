/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sławomir Lach ( matlak, msl )
 * Damian Marciniak
 */
package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.GuardMovesCollector;
import jchess.pieceBehaviors.RunMovesCollector;

/**
 * Class to represent the guardian piece.
 */
public class Guardian extends Piece
{
	/**
	 * Method for initializing the guardian. For reference he gets the symbol 'G'.
	 * @param chessboard it is needed to determine the possible positions of the guardian
	 * @param player the player assigned to that guardian
	 */
    public Guardian(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("G");
        this.setImage();
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color){
        	case white: 
        		image = FileLoader.loadImageFromTheme("Guardian-W.png");
        		break;
        	case black: 
        		image = FileLoader.loadImageFromTheme("Guardian-B.png");
        		break;
        	case gray: 
        		image = FileLoader.loadImageFromTheme("Guardian-G.png");
        		break;
        	default:
        		break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the guardian.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {
    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
        
    	RunMovesCollector rmc = new RunMovesCollector(this);
    	GuardMovesCollector gmc = new GuardMovesCollector(this, rmc);

    	Chessboard.cardinalDirections[] dirs =
    		{
    			Chessboard.cardinalDirections.northwest, Chessboard.cardinalDirections.northeast, 
    			Chessboard.cardinalDirections.southwest, Chessboard.cardinalDirections.southeast,
    			Chessboard.cardinalDirections.north, Chessboard.cardinalDirections.south,
    			Chessboard.cardinalDirections.west, Chessboard.cardinalDirections.east
    		};  
    
    	// add positions around 
	    for(int i = 0; i < dirs.length; i++)
	    {
	    	movelist.add(rmc.movesInDir(this.square, 1, dirs[i], false, false, true));
	    }
	    
	    // add guarding positions
	    for(int i = 4; i < dirs.length; i++)
	    {
	       	movelist.add(gmc.getGuardMoves(this.square, dirs[i], "King", true, dirs));
	    }

        return this.composeMoves(movelist);
    }
}
