package jchess.pieces;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import jchess.Chessboard;
import jchess.Player;
import jchess.Square;

import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to represent any kinds of pieces the the chess.
 */
public abstract class Piece
{

	private static final Logger log = Logger.getLogger(Piece.class.getName());
	
    public Chessboard chessboard;
    public Square square;
    private Player player;
    protected String name;
    private String symbol;
    
    protected static Image imageBlack;
    protected static Image imageWhite;
    protected static Image imageGray;
    
    public Image orgImage;
    public Image image;

    boolean wasMotion = false;
   

    /**
     * Method that sets the chessboard and player for the piece.
     * @param chessboard input chessboard data		
     * @param player input player data
     */
    protected Piece(Chessboard chessboard, Player player)
    {
        this.chessboard = chessboard;
        this.player = player;
        
        this.setImage();
        this.name = this.getClass().getSimpleName();

    }
    
    protected void setImage() { }
    abstract public ArrayList<Square> allMoves();
    
    /**
     * Method to draw the piece on the chessboard.
     * @param g graphical input
     */
    public final void draw(Graphics g)
    {
        try
        {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            int height;
            int x;
            int y;
            
            if(this.chessboard.getPlayercount()==2)
            {
	            height = this.chessboard.get_square_height();
	            Point topLeft = this.chessboard.getTopLeftPoint();
	            
	            x = (this.square.getPozX() * height) + topLeft.x;
	            y = (this.square.getPozY() * height) + topLeft.y;
            }
            else
            {
            	height = 29;
            	
            	int center = this.chessboard.getCenterOfImage();

            	double rotangle = this.square.getPozX() * 15+188;
            	double distance = (5 - this.square.getPozY())*this.chessboard.getSquare_height()+this.chessboard.getCenterOffset()+20;
            	
            	x = (int)(Math.sin(Math.toRadians(rotangle))*distance)+center-15;
            	y = (int)(Math.cos(Math.toRadians(rotangle))*distance)+center-15;
            }

            if (image != null && g != null)
            {
                Image tempImage = orgImage;
                BufferedImage resized = new BufferedImage(height, height, BufferedImage.TYPE_INT_ARGB_PRE);
                Graphics2D imageGr = (Graphics2D) resized.createGraphics();
                imageGr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                imageGr.drawImage(tempImage, 0, 0, height, height, null);
                imageGr.dispose();
                image = resized.getScaledInstance(height, height, 0);
                g2d.drawImage(image, x, y, null);
            }
            else
            {
            	log.log( Level.WARNING, "image is null! " + this.player.color);
            }
        }
        catch (java.lang.NullPointerException exc)
        {
        	log.log( Level.SEVERE, "Something wrong when painting piece: " + exc.getMessage());
        }
    }

    
    /**
     * Method that checks whether a piece is movable to new square.
     * @param x y position on chessboard
     * @param y  y position on chessboard
     * @return true if can move, false otherwise
     */
    public boolean checkPiece(int x, int y)
    {
        if (chessboard.getSquares()[x][y].getPiece() != null
                && chessboard.getSquares()[x][y].getPiece().name.equals("King"))
        {
            return false;
        }
        
        Piece piece = chessboard.getSquares()[x][y].getPiece();
        
        if (piece == null || 					//if this square is empty
                piece.player != this.player) 	//or piece is another player
        {
            return true;
        }
        
        return false;
    }

    /**
     * Method that checks if the piece has another owner than the calling piece.
     * @param x x position on chessboard
     * @param y y position on chessboard
     * @return true if owner(player) is different
     */
    public boolean otherOwner(int x, int y)
    {
        Square sq = chessboard.getSquares()[x][y];
        if (sq.getPiece() == null)
        {
            return false;
        }
        if (this.player != sq.getPiece().player)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Method that combines two lists with squares.
     * @param moves the list containing two seperate square lists
     * @return the composed list of those two lists
     */
    protected ArrayList<Square> composeMoves(ArrayList<ArrayList<Square>> moves){
    	
    	ArrayList<Square> composedMoves = new ArrayList<Square>();
    	for(ArrayList<Square> moveindir : moves)
    	{
    		composedMoves.addAll(moveindir);
    	}
		return composedMoves;
    }

    
    public String getSymbol(){
        return this.symbol;
    }
    
    public void setSymbol(String symbol){
    	this.symbol= symbol;
    }
    
    public Chessboard getChessboard(){
    	return this.chessboard;
    }
    
    public Square getSquare(){
    	return this.square;
    }
    
    public void setSquare(Square square){
    	this.square=square;
    }
    
    public Player getPlayer(){
    	return this.player;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Image getImageBlack() {
		return imageBlack;
	}

	public static void setImageBlack(Image imageBlack) {
		Piece.imageBlack = imageBlack;
	}

	public static Image getImageWhite() {
		return imageWhite;
	}

	public static void setImageWhite(Image imageWhite) {
		Piece.imageWhite = imageWhite;
	}

	public static Image getImageGray() {
		return imageGray;
	}

	public static void setImageGray(Image imageRed) {
		Piece.imageGray = imageRed;
	}

	public Image getOrgImage() {
		return orgImage;
	}

	public void setOrgImage(Image orgImage) {
		this.orgImage = orgImage;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public static Logger getLog() {
		return log;
	}

	public void setChessboard(Chessboard chessboard) {
		this.chessboard = chessboard;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
    
	public void setWasMotion(boolean wm){
		wasMotion = wm;
	}
		   
	public boolean getWasMotion(){
	   	return wasMotion;
	}
}
