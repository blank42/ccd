package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.RunMovesCollector;
import jchess.pieceBehaviors.SwitchMovesCollector;

/**
 * Class to represent the ninja piece.
 */
public class Ninja extends Piece
{
	/**
	 * Method for initializing the ninja. For reference he gets the symbol 'J'. (For N is
	 * already occupied by the knight)
	 * @param chessboard it is needed to determine the possible positions of the ninja
	 * @param player the player assigned to that ninja
	 */
    public Ninja(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("J");
        this.setImage();
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color){
        	case white: 
        		image = FileLoader.loadImageFromTheme("Ninja-W.png");
        		break;
        	case black: 
        		image = FileLoader.loadImageFromTheme("Ninja-B.png");
        		break;
        	case gray: 
        		image = FileLoader.loadImageFromTheme("Ninja-G.png");
        		break;
        	default:
        		break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the ninja.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {
    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
        
    	RunMovesCollector rmc = new RunMovesCollector(this);

    	Chessboard.cardinalDirections[] dirs =
    		{
    			Chessboard.cardinalDirections.northwest, Chessboard.cardinalDirections.northeast, 
    			Chessboard.cardinalDirections.southwest, Chessboard.cardinalDirections.southeast, Chessboard.cardinalDirections.southeast,
    			Chessboard.cardinalDirections.north, Chessboard.cardinalDirections.south, Chessboard.cardinalDirections.west, Chessboard.cardinalDirections.east
    		};  
   
	    for(Chessboard.cardinalDirections dir : dirs)
	    {
	    	movelist.add(rmc.movesInDir(this.square, 1, dir, false, true, false));
	    }
	    
	    SwitchMovesCollector smc = new SwitchMovesCollector(this);
	    
	    movelist.add(smc.getSwitchMoves());

        return this.composeMoves(movelist);
    }
}
