package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.JumpMovesCollector;

/**
 * Class to represent the knight piece. <br>
 * 
 *  |_|_|_|_|_|_|_|_|7	<br>
 *  |_|_|_|_|_|_|_|_|6	<br>
 *  |_|_|2|_|3|_|_|_|5	<br>
 *  |_|1|_|_|_|4|_|_|4	<br>
 *  |_|_|_|K|_|_|_|_|3	<br>
 *  |_|8|_|_|_|5|_|_|2	<br>
 *  |_|_|7|_|6|_|_|_|1	<br>
 *  |_|_|_|_|_|_|_|_|0	<br>
 *   0 1 2 3 4 5 6 7	<br>
 *   
 */
public class Knight extends Piece
{
	/**
	 * Method for initializing the knight. For reference he gets the symbol 'N'. (For K is
	 * already occupied by the king)
	 * @param chessboard it is needed to determine the possible positions of the knight
	 * @param player the player assigned to that knight
	 */
    public Knight(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("N");
        this.setImage();
    }

    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color){
        	case white: 
        		image = FileLoader.loadImageFromTheme("Knight-W.png");
        		break;
        	case black: 
        		image = FileLoader.loadImageFromTheme("Knight-B.png");
        		break;
        	case gray: 
        		image = FileLoader.loadImageFromTheme("Knight-G.png");
        		break;
        	default:
        		break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the knight.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {
    	JumpMovesCollector jmc = new JumpMovesCollector(this);
        
    	ArrayList <Square> moveslist =new ArrayList <Square>();

    	Chessboard.cardinalDirections[] directions =
    		{
    			Chessboard.cardinalDirections.north, Chessboard.cardinalDirections.south, 
    			Chessboard.cardinalDirections.west, Chessboard.cardinalDirections.east
    		};

    	Chessboard.cardinalDirections[] diagonalDirs = new Chessboard.cardinalDirections[2];

    	for(Chessboard.cardinalDirections dir : directions)
    	{
    		switch(dir)
    		{
	    		case north:	
	    			if(this.chessboard.getPlayercount()>2 && this.square.getPozY()==5)
	    			{
		    			diagonalDirs[0] = Chessboard.cardinalDirections.southeast;
		    			diagonalDirs[1] = Chessboard.cardinalDirections.southwest;
	    			}
	    			else
	    			{
	        			diagonalDirs[0] = Chessboard.cardinalDirections.northeast;
	        			diagonalDirs[1] = Chessboard.cardinalDirections.northwest;
	    			}
	    			break;
	    		case south:		
	    			diagonalDirs[0] = Chessboard.cardinalDirections.southeast;
	    			diagonalDirs[1] = Chessboard.cardinalDirections.southwest;
	    			break;
	    		case west:		
	    			diagonalDirs[0] = Chessboard.cardinalDirections.northwest;
	    			diagonalDirs[1] = Chessboard.cardinalDirections.southwest;
	    			break;
	    		case east:		
	    			diagonalDirs[0] = Chessboard.cardinalDirections.northeast;
	    			diagonalDirs[1] = Chessboard.cardinalDirections.southeast;
	    			break;
	    		default: break;
    		}
    		
    		for(Chessboard.cardinalDirections diaDir : diagonalDirs)
    		{
    			Square jumpMove = jmc.getJumpMove(this.square, 1, dir, 1, diaDir, false);

    			if(jumpMove!=null) moveslist.add(jumpMove);
    		}

    	}

        return moveslist;
    }
}
