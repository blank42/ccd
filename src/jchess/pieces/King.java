package jchess.pieces;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.FileLoader;
import jchess.Player;
import jchess.Square;
import jchess.pieceBehaviors.CastlingMovesCollector;
import jchess.pieceBehaviors.RunMovesCollector;

/**
 * Class to represent the king piece. The king is the most important
 * piece for the game. Losing the king means the end of the game.
 * When he is endangered by the opponent then he is 'Checked'. When he has
 * no other escape then stay on a square "in danger" by the opponent
 * then he is 'CheckedMated', and the game will be over. <br>
 *
 *|_|_|_|_|_|_|_|_|7	<br>
 *|_|_|_|_|_|_|_|_|6	<br>
 *|_|_|_|_|_|_|_|_|5	<br>
 *|_|_|X|X|X|_|_|_|4	<br>
 *|_|_|X|K|X|_|_|_|3	<br>
 *|_|_|X|X|X|_|_|_|2	<br>
 *|_|_|_|_|_|_|_|_|1	<br>
 *|_|_|_|_|_|_|_|_|0	<br>
 * 0 1 2 3 4 5 6 7		<br>
 */
public class King extends Piece
{

    private CastlingMovesCollector cmc;
    private RunMovesCollector rmc;
    
    /**
     * The states to determine the end of a game.
     * ok - the king can move freely
     * checkmated - the king cannot move any further, the game will be over
     * stalemated - if it is the king's turn and he cannot move anywhere
     */
    public static enum states
    {
        ok, checkmated, stalemated
    }
    
    /**
	 * Method for initializing the king. For reference he gets the symbol 'K'.
	 * @param chessboard it is needed to determine the possible positions of the king
	 * @param player the player assigned to that king
	 */
    public King(Chessboard chessboard, Player player)
    {
        super(chessboard, player);
        this.setSymbol("K");
        this.setImage();
        
        cmc = new CastlingMovesCollector(this);
        rmc = new RunMovesCollector(this);
    }
    
    /**
     * Method setting the right images for every player (by color).
     */
    @Override
    protected void setImage()
    {
        switch(this.getPlayer().color)
        {
        	case white: 
        		image = FileLoader.loadImageFromTheme("King-W.png");
        		break;
        	case black: 
        		image = FileLoader.loadImageFromTheme("King-B.png");
        		break;
        	case gray: 
        		image = FileLoader.loadImageFromTheme("King-G.png");
        		break;
        	default:
        		break;
        }
        orgImage = image;
    }

    /**
     * Method that creates a list of possible moves for the king.
     * @return the list of possible moves
     */
    @Override
    public ArrayList<Square> allMoves()
    {

    	ArrayList<ArrayList<Square>> movelist = new ArrayList<ArrayList<Square>>();
        
    	Chessboard.cardinalDirections[] dirs =
    		{
    			Chessboard.cardinalDirections.northwest, Chessboard.cardinalDirections.northeast, 
    			Chessboard.cardinalDirections.southwest, Chessboard.cardinalDirections.southeast, Chessboard.cardinalDirections.southeast,
    			Chessboard.cardinalDirections.north, Chessboard.cardinalDirections.south, Chessboard.cardinalDirections.west, Chessboard.cardinalDirections.east
    		};  
   
	    for(Chessboard.cardinalDirections dir : dirs)
	    {
	    	movelist.add(rmc.movesInDir(this.square, 1, dir,false, false, false));
	    }

    	movelist.add(cmc.getAllCastlingMoves());

    	return this.composeMoves(movelist);
    }

    /**
     * Method to check if the king is checked.
     * @return bool true if king is not save, else returns false
     */
    public boolean isChecked()
    {
        return !cmc.isSafe(this.square);
    }

    /**
     * Method to check if the king is checked or stalemated.
     * @return the corresponding King.states
     */
    public states isCheckmatedOrStalemated()
    {
        if (this.allMoves().size() == 0)
        {
            for (int i = 0; i < this.getChessboard().getSquares().length; ++i)
            {
                for (int j = 0; j < this.getChessboard().getSquares()[i].length; ++j)
                {
                    if (this.getChessboard().getSquares()[i][j].getPiece() != null
                            && this.getChessboard().getSquares()[i][j].getPiece().getPlayer() == this.getPlayer()
                            && this.getChessboard().getSquares()[i][j].getPiece().allMoves().size() != 0)
                    {
                        return states.ok;
                    }
                }
            }

            if (this.isChecked())
            {
                return states.checkmated;
            }
            else
            {
                return states.stalemated;
            }
        }
        else
        {
            return states.ok;
        }
    }
    
    /**
     * Method to check if the king will still be safe when he moves.
     * @param sqIsHere square from which the king starts
     * @param sqWillBeThere square on which the king will land on
     * @return bool true if king is save, else returns false
     */
    public boolean willBeSafeWhenMoveOtherPiece(Square sqIsHere, Square sqWillBeThere)
    {
    	// movement
        Piece tmp = sqWillBeThere.getPiece();
        sqWillBeThere.setPiece(sqIsHere.getPiece());
        sqIsHere.setPiece(null);

        boolean ret = cmc.isSafe(this.square);

        // reset
        sqIsHere.setPiece(sqWillBeThere.getPiece());
        sqWillBeThere.setPiece(tmp);

        return ret;
    }
}