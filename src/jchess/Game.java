package jchess;

import java.awt.event.ComponentEvent;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;

import jchess.pieces.King;

import java.awt.*;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.awt.event.ComponentListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible for the starts of new games, loading games,
 * saving it, and for ending it.
 * This class is also responsible for appoing player with have
 * a move at the moment
 */
public class Game extends JPanel implements MouseListener, ComponentListener
{
	private static final long serialVersionUID = -5897130932067350063L;

	private static final Logger log = Logger.getLogger( Game.class.getName() );
	
    public Settings settings;
    public boolean blockedChessboard;
    public Chessboard chessboard;
    protected Player activePlayer;
    public GameClock gameClock;
    public Client client;
    public Moves moves;
    public Chat chat;
	protected Square sq;
	public int playerCount = 2;

    public Game() { }

	/**
	 * Method to save actual state of game
     * @param path address of place where game will be saved
     */
    public void saveGame(File path){ }

    @Override
    public void setSize(int width, int height) {
	    Dimension min = this.getMinimumSize();
	    if(min.getHeight() < height && min.getWidth() < width) {
	    	super.setSize(width, height);
	    } else if(min.getHeight() < height) {
	    	super.setSize(width, (int)min.getHeight());
	    } else if(min.getWidth() < width) {
	    	super.setSize((int)min.getWidth(), height);
	    } else {
	    	super.setSize(width, height);
	    }
    }
    
    /**
     * Method that loads the game. Especially a game state from an earlier saved file.
     * @param file File where is saved game
     * @return a string that tells which saved game is loaded
     */
    static public String getSaveGameTyp(File file)
    {
        FileReader fileR = null;
        try
        {
            fileR = new FileReader(file);
        }
        catch (java.io.IOException exc)
        {
        	log.log( Level.SEVERE,"Something wrong reading file: " + exc);
            return "";
        }
        BufferedReader br = new BufferedReader(fileR);
        String tempStr = new String();
        try
        {
            tempStr = getLineWithVar(br, new String("[Event"));
            return getValue(tempStr);
        }
        catch (ReadGameError err)
        {
        	log.log( Level.SEVERE,"Error reading file: " + err);
            return "";
        }
    }

    /** Method checking in with of line there is an error
     *  @param  br BufferedReader class object to operate on
     *  @param  srcStr String class object with text which variable you want to get in file
     *  @return String with searched variable in file (whole line)
     *  @throws ReadGameError class object when something goes wrong when reading file
     */
    static public String getLineWithVar(BufferedReader br, String srcStr) throws ReadGameError
    {
        String str = new String();
        while (true)
        {
            try
            {
                str = br.readLine();
            }
            catch (java.io.IOException exc)
            {
            	log.log( Level.SEVERE,"Something wrong reading file: " + exc);
            }
            if (str == null)
            {
                throw new ReadGameError();
            }
            if (str.startsWith(srcStr))
            {
                return str;
            }
        }
    }

    /** Method to get value from loaded txt line
     *  @param line Line which is readed
     *  @return result String with loaded value
     *  @throws ReadGameError object class when something goes wrong
     */
    static public String getValue(String line) throws ReadGameError
    {
        int from = line.indexOf("\"");
        int to = line.lastIndexOf("\"");
        int size = line.length() - 1;
        String result = new String();
        if (to < from || from > size || to > size || to < 0 || from < 0)
        {
            throw new ReadGameError();
        }
        try
        {
            result = line.substring(from + 1, to);
        }
        catch (java.lang.StringIndexOutOfBoundsException exc)
        {
        	log.log( Level.SEVERE,"error getting value: " + exc);
            return "none";
        }
        return result;
    }

    /**
     * Method to Start new game.
     */
    public void newGame(){ }

    /**
     * Method to end game
     * @param message what to show player(s) at end of the game (for example "draw", "black wins" etc.)
     */
    public void endGame(String message)
    {
        this.blockedChessboard = true;
        log.log( Level.INFO,message);
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * Method that swiches the active player after one player has made his move.
     * @param forward if true then the next player will be active
     */
    public void switchActive(boolean forward){ }

    /**
     * Method of getting accurately active player
     * @return  player The player which have a move
     */
    public Player getActivePlayer()
    {
        return this.activePlayer;
    }
    
    public void setActivePlayer(Player player)
    {
        this.activePlayer = player;
    }

    /** Method to go to next move (checks if game is local/network etc.)
     */
    public void nextMove()
    {
        switchActive(true);

        log.log( Level.INFO,"next move, active player: " + activePlayer.name + ", color: " + activePlayer.color.name() + ", type: " + activePlayer.playerType.name());
        if (activePlayer.playerType == Player.playerTypes.localUser)
        {
            this.blockedChessboard = false;
        }
        else if (activePlayer.playerType == Player.playerTypes.networkUser)
        {
            this.blockedChessboard = true;
        }
        else if (activePlayer.playerType == Player.playerTypes.computer)
        {
        	
        }
    }

    /** Method to simulate Move to check if it's correct etc. (usable for network game).
     * @param beginX from which X (on chessboard) move starts
     * @param beginY from which Y (on chessboard) move starts
     * @param endX   to   which X (on chessboard) move go
     * @param endY   to   which Y (on chessboard) move go
     * @return true if move was correct
     * */
    public boolean simulateMove(int beginX, int beginY, int endX, int endY)
    {
        try 
        {
            chessboard.select(chessboard.getSquares()[beginX][beginY]);
            if (chessboard.getActiveSquare().getPiece().allMoves().indexOf(chessboard.getSquares()[endX][endY]) != -1) //move
            {
                chessboard.move(chessboard.getSquares()[beginX][beginY], chessboard.getSquares()[endX][endY]);
            }
            else
            {
                log.log( Level.WARNING,"Bad move");
                return false;
            }
            chessboard.unselect();
            nextMove();

            return true;
            
        } 
        catch(StringIndexOutOfBoundsException exc) 
        {
            return false;
        }    
        catch(ArrayIndexOutOfBoundsException exc) 
        {
            return false;
        }
        catch(NullPointerException exc)
        {
            return false;
        }
        finally
        {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, "ERROR");
        }
    }

    public void mouseClicked(MouseEvent arg0) { }
    
    public boolean undo()
    {
        boolean status = false;
        
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            status = chessboard.undo();
            if( status )
            {
                this.switchActive(false);
            }
            else
            {
                chessboard.repaint();
            }
        }
        else if( this.settings.gameType == Settings.gameTypes.network )
        {
            this.client.sendUndoAsk();
            status = true;
        }
        return status;
    }
    
    public boolean rewindToBegin()
    {
        boolean result = false;
        
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            while( chessboard.undo() )
            {
                result = true;
            }
        }
        else
        {
            throw new UnsupportedOperationException( Settings.lang("operation_supported_only_in_local_game") );
        }
        
        return result;
    }
    
    public boolean rewindToEnd() throws UnsupportedOperationException
    {
        boolean result = false;
        
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            while( chessboard.redo() )
            {
                result = true;
            }
        }
        else
        {
            throw new UnsupportedOperationException( Settings.lang("operation_supported_only_in_local_game") );
        }
        
        return result;
    }
    
    public boolean redo()
    {
        boolean status = chessboard.redo();
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            if ( status )
            {
                this.nextMove();
            }
            else
            {
                chessboard.repaint();
            }
        }
        else
        {
            throw new UnsupportedOperationException( Settings.lang("operation_supported_only_in_local_game") );
        }
        return status;
    }
    
    

    public void mousePressed(MouseEvent event)
    {
        if (event.getButton() == MouseEvent.BUTTON3) //right button
        {
            this.undo();
        }
        else if (event.getButton() == MouseEvent.BUTTON2 && settings.gameType == Settings.gameTypes.local)
        {
            this.redo();
        }
        else if (event.getButton() == MouseEvent.BUTTON1) //left button
        {

            if (!blockedChessboard)
            {
                try 
                {
                    int x = event.getX();//get X position of mouse
                    int y = event.getY();//get Y position of mouse

                    sq = chessboard.getClickedSquare(x, y);
                    if ((sq == null && sq.getPiece() == null && chessboard.getActiveSquare() == null)
                            || (this.chessboard.getActiveSquare() == null && sq.getPiece() != null && sq.getPiece().getPlayer() != this.activePlayer))
                    {
                        return;
                    }

                    if (sq.getPiece() != null && sq.getPiece().getPlayer() == this.activePlayer && sq != chessboard.getActiveSquare())
                    {
                        chessboard.unselect();
                        chessboard.select(sq);
                    }
                    else if (chessboard.getActiveSquare() == sq) //unselect
                    {
                        chessboard.unselect();
                    }
                    else if (chessboard.getActiveSquare() != null && chessboard.getActiveSquare().getPiece() != null
                            && chessboard.getActiveSquare().getPiece().allMoves().indexOf(sq) != -1) //move
                    {
                        if (settings.gameType == Settings.gameTypes.local)
                        {
                            chessboard.move(chessboard.getActiveSquare(), sq);
                        }
                        else if (settings.gameType == Settings.gameTypes.network)
                        {
                            client.sendMove(chessboard.getActiveSquare().getPozX(), chessboard.getActiveSquare().getPozY(), sq.getPozX(), sq.getPozY());
                            chessboard.move(chessboard.getActiveSquare(), sq);
                        }

                        chessboard.unselect();

                        //switch player
                        this.nextMove();

                        //checkmate or stalemate
                        King king;
                        // check if active player is white
                        if (this.activePlayer == settings.player[0])
                        {
                            king = chessboard.getKingWhite();
                        }
                        else
                        {
                            king = chessboard.getKingBlack();
                        }

                        switch (king.isCheckmatedOrStalemated())
                        {
                            case checkmated:
                                this.endGame("Checkmate! " + king.getPlayer().color.toString() + " player lose!");
                                break;
                            case stalemated:
                                this.endGame("Stalemate! Draw!");
                                break;
                            default:
                            	break;
                        }
                    }
                    
                } 
                catch(NullPointerException exc)
                {
                	log.log( Level.SEVERE, exc.getMessage());
                    chessboard.repaint();
                    return;
                }
            }
            else if (blockedChessboard)
            {
            	log.log( Level.WARNING, "Chessboard is blocked");
            }
        }
    }

    public void mouseReleased(MouseEvent arg0) { }

    public void mouseEntered(MouseEvent arg0) { }

    public void mouseExited(MouseEvent arg0) { }

    public void componentResized(ComponentEvent e)
    {
        int height = this.getHeight() >= this.getWidth() ? this.getWidth() : this.getHeight();
        int chess_height = (int)Math.round( (height * 0.8)/8 )*8;
        this.chessboard.resizeChessboard((int)chess_height);
        chess_height = this.chessboard.getHeight();
        this.moves.getScrollPane().setLocation(new Point(chess_height + 5, 100));
        this.moves.getScrollPane().setSize(this.moves.getScrollPane().getWidth(), chess_height - 100);
        this.gameClock.setLocation(new Point(chess_height + 5, 0));
        if (this.chat != null)
        {
            this.chat.setLocation(new Point(0, chess_height + 5));
            this.chat.setSize(new Dimension(chess_height, this.getHeight() - (chess_height + 5))); 
        }
    }
    
    protected int computeChessboardHeight(){
        int height = this.getHeight() >= this.getWidth() ? this.getWidth() : this.getHeight();
        double scaleFactor;
        if(this.playerCount>2){
        	scaleFactor=1;
        }else{
        	scaleFactor=0.5;
        }
       return (int)Math.round( (height*scaleFactor)/8 )*8;
    }

    public void componentMoved(ComponentEvent e) { }

    public void componentShown(ComponentEvent e) { }

    public void componentHidden(ComponentEvent e) { }
}

class ReadGameError extends Exception
{
	private static final long serialVersionUID = -189011964487101662L;
}