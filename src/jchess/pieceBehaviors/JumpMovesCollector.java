package jchess.pieceBehaviors;

import jchess.Chessboard;
import jchess.Square;
import jchess.pieces.Piece;

/**
 * Class that calculates the possible moves for a jumping move behaviour. (e.g. the knight)
 */
public class JumpMovesCollector extends MovesCollector{

	public JumpMovesCollector(Piece piece){
		super(piece);
	}
	
	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	
	
	public Square getJumpMove(Square start, int distance1, Chessboard.cardinalDirections direction1, int distance2, Chessboard.cardinalDirections direction2, boolean allMovesMustBeSafe){
		
		Square considsquare1=start.getSquareInDir(distance1, direction1);
		
		Square considsquare2=null;
		
		if(considsquare1 != null && considsquare1.getSquareInDir(distance2, direction2) != null)
		{
			considsquare2=considsquare1.getSquareInDir(distance2, direction2);
		}
		else
		{
			return null;
		}
		
		if (this.piece.checkPiece(considsquare2.getPozX(), considsquare2.getPozY()) && 
				this.piece.getChessboard().getKing(this.piece.getPlayer().color).willBeSafeWhenMoveOtherPiece(this.piece.getSquare(),considsquare2))
		{
			if(allMovesMustBeSafe && !this.isSafe(considsquare2))
			{
				return null;
			}

            return considsquare2;
		}
		else
		{
			return null;
		}
	}
}
