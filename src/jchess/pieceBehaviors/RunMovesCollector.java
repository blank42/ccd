package jchess.pieceBehaviors;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.Square;
import jchess.pieces.Piece;

/**
 * Class that calculates the possible moves in the directions vertical, horizontal and diagonal.
 */
public class RunMovesCollector extends MovesCollector{
	
	public RunMovesCollector(Piece piece){
		super(piece);
	}

	/**
	 * Method that provides all possible moves in a specified direction.
	 * @param start the square of a piece which move should be considered
	 * @param range the range of the movement
	 * @param direction the direction you want to move to
	 * @param allMovesMustBeSafe move is only possible is the square is safe
	 * @param onlyCapture move is only possible if on this square is a piece of another player
	 * @param onlyEmptySquares move is only possible if no piece is on the considered squares
	 * @return list filled with all possible moves in a specified direction
	 */
    public ArrayList<Square> movesInDir(Square start, int range, Chessboard.cardinalDirections direction, boolean allMovesMustBeSafe, boolean onlyCapture, boolean onlyEmptySquares)
    {
  
    	ArrayList<Square> list = new ArrayList<Square>();
    	Square considsquare= start;


    	int i =0;
    	//as long as we've not reached the end of the chessboard
    	while(considsquare.getSquareInDir(direction) != null 
    			&& i != range 
    			&& !considsquare.getSquareInDir(direction).equals(start)
    			&& !this.isBorderCrossing(considsquare, considsquare.getSquareInDir(direction)))
    	{
    		
    		
    		if((direction==Chessboard.cardinalDirections.north
    				||direction==Chessboard.cardinalDirections.northeast
    				||direction==Chessboard.cardinalDirections.northwest) 
    				&& considsquare.getSquareInDir(direction).getPozY() == considsquare.getPozY())
    		{ 
    			//if the number of players is > 2 and we've field reached the other side

    			if(direction==Chessboard.cardinalDirections.north)
    			{
    				considsquare=considsquare.getSquareInDir(Chessboard.cardinalDirections.north);
    			}
    			else if(direction==Chessboard.cardinalDirections.northeast)
    			{
    				considsquare=considsquare.getSquareInDir(Chessboard.cardinalDirections.north).getSquareInDir(2,Chessboard.cardinalDirections.east);
    			}
    			else if(direction==Chessboard.cardinalDirections.northwest)
    			{
    				considsquare=considsquare.getSquareInDir(Chessboard.cardinalDirections.north).getSquareInDir(2,Chessboard.cardinalDirections.west);
    			}


    			direction=this.getOtherSideVector(direction);
    		}
    		else
    		{
    			considsquare=considsquare.getSquareInDir(direction);
    		}

    		
    		
    		if (this.piece.checkPiece(considsquare.getPozX(), considsquare.getPozY()) && 
    				this.piece.getChessboard().getKing(this.piece.getPlayer().color).willBeSafeWhenMoveOtherPiece(this.piece.getSquare(),considsquare))
    		{
    			if((allMovesMustBeSafe && !this.isSafe(considsquare))
    					||(onlyEmptySquares && considsquare.getPiece()!=null)){
        			break;
        		}
    			
    			if (!onlyCapture || (onlyCapture && this.piece.otherOwner(considsquare.getPozX(), considsquare.getPozY())))
    			{
    				list.add(considsquare);
    			}
    		
                if (this.piece.otherOwner(considsquare.getPozX(), considsquare.getPozY()))
                {
                	break;
                }
    		}
    		else
    		{
    			break;
    		} 
    		i++;
    	}

    	return list;
    }
    

}
