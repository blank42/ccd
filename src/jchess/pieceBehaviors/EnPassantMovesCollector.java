package jchess.pieceBehaviors;

import jchess.Chessboard;
import jchess.Square;
import jchess.pieces.Piece;

/**
 * Class that calculates the possible moves for a enhanced capture behaviour. (e.g. the pawn)
 */
public class EnPassantMovesCollector extends MovesCollector{
	
	public EnPassantMovesCollector(Piece piece){
		super(piece);
	}
	
	public Square getEnPassantMove(Square s, Chessboard.cardinalDirections dirOfEnPassantPiece, Chessboard.cardinalDirections dirOfEnPassant, boolean MustBeTwoSquareMovedPawn){
		
		Square EnPassantPieceSquare = s.getSquareInDir(dirOfEnPassantPiece);
		
		if(EnPassantPieceSquare!=null 
				&& EnPassantPieceSquare.getPiece()!=null 
				&& EnPassantPieceSquare.getPiece().getPlayer()!=this.piece.getPlayer())
		{
			if(!MustBeTwoSquareMovedPawn||(MustBeTwoSquareMovedPawn 
					&& this.piece.getChessboard().twoSquareMovedPawn != null
					&& this.piece.getChessboard().twoSquareMovedPawn == EnPassantPieceSquare.getPiece()))
			return EnPassantPieceSquare.getSquareInDir(dirOfEnPassant);
		}
		
		return null;
	}
}
