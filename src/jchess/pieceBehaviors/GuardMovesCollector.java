package jchess.pieceBehaviors;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.Square;
import jchess.pieces.Piece;

/**
 * Class that calculates the possible moves for a guarding behaviour. (e.g. the guardian)
 */
public class GuardMovesCollector extends MovesCollector{
	
	RunMovesCollector rmc;
	
	/**
     * Constructor
     * @param piece current piece
     * @param rmc RunMovesCollector of calling method
     */
	public GuardMovesCollector(Piece piece, RunMovesCollector rmc){
		super(piece);
		this.rmc = rmc;
	}

	/**
     * Method provides all possible moves around square in direction to guard
     * @param square current square of piece
     * @param direction direction of guarded piece
     * @param toGuardPieceName guarded piece name
     * @param onlyOwnPlayerPieces if false, "guards" also other placer pieces
     * @param guardedDirection directions to guard around target piece
     * @return list filled with all possible moves in around specified direction
     */
	public ArrayList<Square> getGuardMoves(Square square, Chessboard.cardinalDirections direction, String toGuardPieceName, boolean onlyOwnPlayerPieces, Chessboard.cardinalDirections[] guardedDirection){
		ArrayList<Square> ownPieces = new ArrayList<Square>();
		
		Square squareInDir = square.getSquareInDir(direction);
		
		if (squareInDir == null)
		{
			return ownPieces;
		}
		
    	if (!this.isBorderCrossing(square, square.getSquareInDir(direction))
    			&& squareInDir.getPiece() != null
    			&& squareInDir.getPiece().getName().equals(toGuardPieceName)
    			&& (!onlyOwnPlayerPieces || squareInDir.getPiece().getPlayer().equals(piece.getPlayer()))
    			&& this.piece.getChessboard().getKing(this.piece.getPlayer().color).willBeSafeWhenMoveOtherPiece(this.piece.getSquare(),square))
    	{
    		
    		
    		for(int j = 0; j < guardedDirection.length; j++){
    			ownPieces.addAll(rmc.movesInDir(squareInDir, 1, guardedDirection[j], false, false, false));
    		}
    	}
	
		return ownPieces;
	}
}
