package jchess.pieceBehaviors;

import java.util.ArrayList;

import jchess.Square;
import jchess.pieces.Piece;

/**
 * Class that calculates the possible moves for a place switching behaviour. (e.g. the ninja)
 */
public class SwitchMovesCollector extends MovesCollector{
	
	/**
     * Constructor
     * @param piece current piece
     */
	public SwitchMovesCollector(Piece piece){
		super(piece);
	}

	/**
     * Method provides all possible moves to switch positions where are own pieces besides king
     * @return list filled with all possible moves to switch
     */
	public ArrayList<Square> getSwitchMoves(){
		ArrayList<Square> ownPieces = new ArrayList<Square>();
		
	    for(Square lineStart[] : this.piece.chessboard.getSquares())
	    {
	    	for(Square square : lineStart)
		    {
	    		if(square.getPiece() == null)
	    		{
	    			continue;
	    		}
	    		

		    	if(square.getPiece().getPlayer().equals(this.piece.getPlayer()) 
		    			&& !square.getPiece().getName().equals("King")
		    			&& this.piece.getChessboard().getKing(this.piece.getPlayer().color).willBeSafeWhenMoveOtherPiece(this.piece.getSquare(),square)
		    			&& !square.getPiece().equals(this.piece))
		    	{
		    			ownPieces.add(square);
		    	}
		    }
	    }
	
		return ownPieces;
	}
}
