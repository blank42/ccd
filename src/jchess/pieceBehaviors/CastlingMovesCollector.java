package jchess.pieceBehaviors;

import java.util.ArrayList;

import jchess.Chessboard;
import jchess.Square;
import jchess.pieces.Piece;
import jchess.pieces.Rook;

/**
 * Class that calculates the possible moves for a castling behaviour. (e.g. the king)
 */
public class CastlingMovesCollector extends MovesCollector{
	
	public enum castlingTypes{
		longCastling, shortCastling
	}

	public CastlingMovesCollector(Piece piece){
		super(piece);
	}
	
	
	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	

	
	public Square getCastlingMove(CastlingMovesCollector.castlingTypes castlingType){
	
	    Square sq;
	    Square sq1;
		
		if (!this.piece.getWasMotion())
	    {
			//check if king was not moved before
			int x;
			Chessboard.cardinalDirections dir;
	
			if(castlingType==CastlingMovesCollector.castlingTypes.longCastling)
			{
				if(this.piece.chessboard.getPlayercount()>2)
				{
					x=7+(this.piece.getPlayer().getPlayerNumber()*8);
				}
				else
				{
					x=7;
				}
				
				dir=Chessboard.cardinalDirections.east;
				
			}
			else
			{
				if(this.piece.chessboard.getPlayercount()>2)
				{
					x=0+(this.piece.getPlayer().getPlayerNumber()*8);
				}
				else
				{
					x=0;
				}		
				dir=Chessboard.cardinalDirections.west;			
			}
			
	
	        if (this.piece.getChessboard().getSquares()[x][this.piece.square.getPozY()].getPiece() != null
	                && this.piece.getChessboard().getSquares()[x][this.piece.square.getPozY()].getPiece().getName().equals("Rook"))
	        {
	            boolean canCastling = true;
	
	            Rook rook = (Rook) this.piece.getChessboard().getSquares()[x][this.piece.square.getPozY()].getPiece();
	            if (!rook.wasMotion)
	            {
	
	                Square considsquare = this.piece.square;
	                while(considsquare.getSquareInDir(dir)!=null && considsquare.getSquareInDir(dir).getPozX()!=x)
	                {
	                	considsquare=considsquare.getSquareInDir(dir);
	                    if (considsquare.getPiece() != null)
	                    {
	                        canCastling = false;
	                        break;
	                    }
	                }
	                
	                sq = this.piece.getSquare().getSquareInDir(dir).getSquareInDir(dir);
	                sq1 = this.piece.getSquare().getSquareInDir(dir);
	                if (canCastling && this.isSafe(sq) && this.isSafe(sq1))
	                { 
	                	//can do castling when none of Sq,sq1 is checked
	                	return sq;
	                }
	            }
	        }
	    }
	 
		return null;
	}


	public ArrayList<Square> getAllCastlingMoves(){
		
		ArrayList<Square> list= new ArrayList<Square>();
		
		if(this.getCastlingMove(CastlingMovesCollector.castlingTypes.shortCastling)!=null)
			list.add(this.getCastlingMove(CastlingMovesCollector.castlingTypes.shortCastling));
		
		if(this.getCastlingMove(CastlingMovesCollector.castlingTypes.longCastling)!=null)
			list.add(this.getCastlingMove(CastlingMovesCollector.castlingTypes.longCastling));
		
		return list;
	}
}



