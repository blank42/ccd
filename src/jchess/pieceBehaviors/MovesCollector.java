package jchess.pieceBehaviors;


import jchess.Border;
import jchess.Chessboard;
import jchess.Square;
import jchess.pieces.Piece;

import java.util.ArrayList;

/**
 * Class that calculates the possible moves dependent on desired direction.
 */
public class MovesCollector {
	
	protected Piece piece;

	public MovesCollector(Piece piece){
		this.piece=piece;
	}

    /**
     * Method that checks if the king is checked by an opponent.
     * @param s Square where the king is standing
     * @return bool true if king is safe, else returns false
     */
    public boolean isSafe(Square s)
    {
  	
    	Chessboard.cardinalDirections[] directions =
    		{
    			Chessboard.cardinalDirections.north, Chessboard.cardinalDirections.south, Chessboard.cardinalDirections.west, Chessboard.cardinalDirections.east, 
    			Chessboard.cardinalDirections.northwest, Chessboard.cardinalDirections.northeast, Chessboard.cardinalDirections.southwest, Chessboard.cardinalDirections.southeast
    		};
    	
    	for (int i = 0; i < 3; i++)
    	{
    		if(s.getSquareInDir(directions[i])!=null
    				&& s.getSquareInDir(directions[i]).getPiece() != null
					&& s.getSquareInDir(directions[i]).getPiece().getPlayer().equals(this.piece.getPlayer())
    				&& s.getSquareInDir(directions[i]).getPiece().getName().equals("Guardian"))
    		{
    			return true;
    		}
    	}
    	
    	for(Chessboard.cardinalDirections dir : directions)
    	{
    		if(checkForThreateningPieceInDir(s, dir))
    		{
    			return false;
    		}
    	}

    	return true;
    }
    
    /**
     * Method called in Method 'isSafe' to check whether there are opponent pieces in any direction of the king
     * @param s Square where is a king
     * @param direction direction to check
     * @return bool true if an opponent piece is found in any direction
     */
    protected boolean checkForThreateningPieceInDir(Square s, Chessboard.cardinalDirections direction){
    	
    	Square considsquare =s;
    	
    	boolean posThreateningPieceFound=false;
    	boolean ownPieceFound=false;
    	String posThreateningPieceSymbol=null;
    	Piece considPiece=null;
    	
    	//for counting the steps taken in the specified direction
    	int step=0;
    	
    	//check for threatening knights
    	Chessboard.cardinalDirections dirDiagonal1=null;
    	Chessboard.cardinalDirections dirDiagonal2=null;
    	boolean straightDir=true;
    	
    
    	switch(direction)
    	{
	    	case north:
	    		dirDiagonal1=Chessboard.cardinalDirections.northwest;
	    		dirDiagonal2=Chessboard.cardinalDirections.northeast;
	    		break;
	    	case south:
	    		dirDiagonal1=Chessboard.cardinalDirections.southwest;
	    		dirDiagonal2=Chessboard.cardinalDirections.southeast;
	    		break;
	    	case west:
	    		dirDiagonal1=Chessboard.cardinalDirections.northwest;
	    		dirDiagonal2=Chessboard.cardinalDirections.southwest;
	    		break;
	    	case east:
	    		dirDiagonal1=Chessboard.cardinalDirections.northeast;
	    		dirDiagonal2=Chessboard.cardinalDirections.southeast;
	    		break;
	
			default: straightDir=false;
    	}
    	

    	while(considsquare.getSquareInDir(direction)!=null 
    			&& !considsquare.getSquareInDir(direction).equals(s)
    			&& !this.isBorderCrossing(considsquare, considsquare.getSquareInDir(direction)))
    	{
    		step++;

    		
       		if(this.piece.chessboard.getPlayercount()>2
       				&&(direction==Chessboard.cardinalDirections.northeast
    				||direction==Chessboard.cardinalDirections.northwest
    				||direction==Chessboard.cardinalDirections.north)
       				&&considsquare.getSquareInDir(direction).getPozY() == considsquare.getPozY())
    		{ 
       			
       			//if the number of players is > 2 and we've field reached the other side
   			
       			if(direction==Chessboard.cardinalDirections.north)
       			{
    				considsquare=considsquare.getSquareInDir(Chessboard.cardinalDirections.north);
    			}
       			else if(direction==Chessboard.cardinalDirections.northeast)
       			{
    				considsquare=considsquare.getSquareInDir(Chessboard.cardinalDirections.north).getSquareInDir(2,Chessboard.cardinalDirections.east);
    			}
       			else if(direction==Chessboard.cardinalDirections.northwest)
       			{
    				considsquare=considsquare.getSquareInDir(Chessboard.cardinalDirections.north).getSquareInDir(2,Chessboard.cardinalDirections.west);
    			}

    			direction=this.getOtherSideVector(direction);
    		}
       		else
       		{
    			considsquare=considsquare.getSquareInDir(direction);
    		}
    		
   		
       		
    		considPiece = considsquare.getPiece();
    		   		
    		if(considPiece!=null)
    		{
    			if(!considPiece.getPlayer().equals(this.piece.getPlayer()))
    			{
	    			posThreateningPieceSymbol = considPiece.getSymbol();
	
	    			posThreateningPieceFound=true;
    			}
    			else
    			{
    				ownPieceFound=true;
    			}
    		}
 		
    		if(straightDir){

    			//if its the first step then check for threatening knights
    			if(step==1){

		    		if( considsquare.getSquareInDir(dirDiagonal1)!=null 
		    			&& considsquare.getSquareInDir(dirDiagonal1).getPiece()!=null
						&& !considsquare.getSquareInDir(dirDiagonal1).getPiece().getPlayer().equals(this.piece.getPlayer())
		            	&& considsquare.getSquareInDir(dirDiagonal1).getPiece().getSymbol()=="N")
		    		{
		    			return true;
		            }
		    		else if( considsquare.getSquareInDir(dirDiagonal2)!=null 
		            			&& considsquare.getSquareInDir(dirDiagonal2).getPiece()!=null
		    					&& !considsquare.getSquareInDir(dirDiagonal2).getPiece().getPlayer().equals(this.piece.getPlayer())
		            			&& considsquare.getSquareInDir(dirDiagonal2).getPiece().getSymbol()=="N")
		    		{
		            	return true;
		            }
	    		}
	    			
	    			
    			if(posThreateningPieceFound)
    			{
    				if(step==1 && posThreateningPieceSymbol=="K")
    				{
    					return true;	
    				}
    				else if(posThreateningPieceSymbol=="R"||posThreateningPieceSymbol=="Q")
    				{
    					return true;
    				}
    				else
    				{
    					return false;
    				}
    			}
    				
    		}
    		else	//if direction is diagonal
    		{
    			
    			if(posThreateningPieceFound)
    			{ 
    				if(step==1 && (posThreateningPieceSymbol==""||posThreateningPieceSymbol=="K"))
    				{
    					return true;
    				}
    				else if(posThreateningPieceSymbol=="B"||posThreateningPieceSymbol=="Q")
    				{
    					return true;
    				}
    				else
    				{
    					return false;
    				}
    			}
    		}
    		
    		if(ownPieceFound)
    		{
				return false;
			}

    	}
    	return false;
    }
    

    /**
     * Method for the circular chessboard which changes the directions accordingly after crossing
     * the middle.
     * @param direction the previous direction of the piece before crossing the middle
     * @return the new direction for the piece to go
     */
    public Chessboard.cardinalDirections getOtherSideVector(Chessboard.cardinalDirections direction){
    	switch(direction)
    	{
	    	case north:		return Chessboard.cardinalDirections.south;
	    	case northwest: return Chessboard.cardinalDirections.southwest;
	    	case northeast: return Chessboard.cardinalDirections.southeast;
	    	default: 		return null;
    	}
    }
    

    /**
     * Method that checks if the piece would cross a border when moving in an specified direction.
     * @param s1 the last square on which the piece stood
     * @param s2 the square on which the piece will land
     * @return true if this move would mean that there is a border between thosw squares
     */
    public boolean isBorderCrossing(Square s1, Square s2){
    	
    	ArrayList<Border> borders = this.piece.getChessboard().borders;
    	
    	for(Border border : borders)
    	{
    		if((border.getLeftBorderSquares().contains(s1)||border.getLeftBorderSquares().contains(s2))
    				&& (border.getRightBorderSquares().contains(s1)||border.getRightBorderSquares().contains(s2)))
    		{
    			return true;
    		}
    	}
    	return false;
    }
}
