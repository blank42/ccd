/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sławomir Lach ( matlak, msl )
 * Damian Marciniak
 */
package jchess;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JPanel;
import jchess.Moves.castling;
import jchess.pieces.*;

import java.util.logging.Level;
import java.util.logging.Logger;

/** Class to represent chessboard. Chessboard is made from squares.
 * It is setting the squers of chessboard and sets the pieces(pawns)
 * witch the owner is current player on it.
 */
public class Chessboard extends JPanel
{
	private static final long serialVersionUID = 1108092533984543487L;

	private static final Logger log = Logger.getLogger( Chessboard.class.getName() );

	private int playerCount;
    public static final int top = 0;
    public static final int bottom = 7;
    private Square squares[][];//squares of chessboard
    private Image image;//image of chessboard
    private Image sel_square;//image of highlited square
    private Image able_square;//image of square where piece can go
    private Square activeSquare;
    private Image upDownLabel = null;
    private Image LeftRightLabel = null;
    private float square_height;//height of square
    private float square_width;//height of square

    public int img_widht;//image width
    public int img_height = img_widht;//image height
    
    public int border_height;
    public ArrayList<Border> borders = new ArrayList<Border>();
    public Game game;
    
    private ArrayList<?> moves;
    private Settings settings;
    private King kingWhite;
    private King kingBlack;
    private King kingGray;

    //-------- for undo ----------
    private Square undo1_sq_begin = null;
    private Square undo1_sq_end = null;
    private Piece undo1_piece_begin = null;
    private Piece undo1_piece_end = null;
    private Piece ifWasEnPassant = null;
    private Piece ifWasCastling = null;
    private boolean breakCastling = false; //if last move break castling
    //----------------------------
    //For En passant:
    //|-> Pawn whose in last turn moved two square
    public Pawn twoSquareMovedPawn = null;
    public Pawn twoSquareMovedPawn2 = null;
    private Moves moves_history;
    
    public enum cardinalDirections
    {

        north, east, south, west, northeast, southeast, southwest, northwest
    }

    /**
     * Chessboard class constructor
     * @param game the gametype which has to be considered
     * @param settings reference to Settings class object for this chessboard
     * @param moves_history reference to Moves class object for this chessboard
     * @param playerCount describes how many players play on this chessboard and thus decides it's appearance
     * @param borderHeight needed to determine the borders for 3 person chess
     */
    public Chessboard(Game game, Settings settings, Moves moves_history, int playerCount, int borderHeight)
    {
        this.settings = settings;
        this.activeSquare = null;
        this.playerCount=playerCount;
        this.border_height=borderHeight;
        this.game=game;

        
        
        int xlimit;
        int ylimit;
        
        if(this.playerCount == 2){
        	this.image = FileLoader.loadImageFromTheme("chessboard2.png");
        	
	        xlimit=8;
	        ylimit=8;
	        
	        this.square_height = img_height / 8;//we need to devide to know height of field
	        this.drawLabels((int) this.square_height);
        }
        else
        {
        	this.image = FileLoader.loadImageFromTheme("chessboard_redturn.png");
            xlimit=8*this.playerCount;
            ylimit=6;

        	this.square_width=360/(8*this.playerCount);//the angle of the squares in x-direction
        }
        

        this.squares = new Square[xlimit][ylimit];
        
        for (int x = 0; x < xlimit; x++)
        {//create object for each square
            for (int y = 0; y < ylimit; y++)
            {
                this.squares[x][y] = new Square(x, y, null, this);
            }
        }
        if(this.playerCount>2){
        this.setBorders();
        }
        this.sel_square = FileLoader.loadImageFromTheme("sel_square.png");
        this.able_square = FileLoader.loadImageFromTheme("able_square.png");
        this.moves_history = moves_history;
        this.setDoubleBuffered(true);
        
        this.setVisible(true);

        this.setSize(this.img_widht, this.img_height);
        this.setLocation(new Point(0, 0));
        
    }/*--endOf-Chessboard--*/

    
    public void setBorders(){
    	
		Player[] players = settings.player;
    	
    	for(Player player : players){

    		ArrayList<Square> leftBorderSquares = new ArrayList<Square>();
    		ArrayList<Square> rightBorderSquares = new ArrayList<Square>();

    		Square rightStartSquare = this.squares[0+(8*player.getPlayerNumber())][0];

    		
    		for(int i = 1; i<=this.border_height; i++){
    			rightBorderSquares.add(rightStartSquare);
    			leftBorderSquares.add(rightStartSquare.getSquareInTheWestDir(1));
    			
    			rightStartSquare=rightStartSquare.getSquareInTheNorthDir(1);
    		}

    		this.borders.add(new Border(rightBorderSquares, leftBorderSquares, this.border_height));
    	}
    }

    /** Method setPieces on begin of new game or loaded game
     * @param places string with pieces to set on chessboard
     * @param Players array of players with size of player number
     */
	public void setPieces4NewGame(String places, Player[] Players) {

		if (places.equals("")) // if newGame
		{

			if (this.playerCount == 2)
			{
				this.setFigures4NewGame(0, Players[0]);
				this.setPawns4NewGame(1, Players[0]);
				this.setFigures4NewGame(7, Players[1]);
				this.setPawns4NewGame(6, Players[1]);
			}
			else
			{
				int beginfield = 0;
				for (int i = 0; i < Players.length; i++) {
					this.setFigures4NewGame(beginfield, Players[i]);
					beginfield += 8;
				}
			}

        } 
        else //if loadedGame
        {
            return;
        }
    }/*--endOf-setPieces--*/

	
    /**  Method set Figures in row (and set Queen and King to right position)
     *  @param i row where to set figures (Rook, Knight etc.)
     *  @param player which is owner of pawns
     *  @brief Placement of Pawns and Figures for all players. Distinction between 2 and more than 2 players.
     * */
    private void setFigures4NewGame(int i, Player player)//here
    {
        if ((this.playerCount==2 && i != 0 && i != 7) )
        {
        	log.log( Level.WARNING, "error setting figures like rook etc.");
            return;
        }
        else if (this.playerCount==2 && i == 0)
        {
            player.goDown = true;
        }
        
        if(this.playerCount == 2){
        	
        	switch(player.color)
        	{
    		case black: 
    			this.squares[4][i].setPiece(kingBlack = new King(this, player));
    			break;
    		case white: 
    			this.squares[4][i].setPiece(kingWhite = new King(this, player));
    			break;
    		default:
    			break;
        	}
        	
        	this.squares[0][i].setPiece(new Rook(this, player));
        	this.squares[7][i].setPiece(new Rook(this, player));
        	this.squares[1][i].setPiece(new Knight(this, player));
        	this.squares[6][i].setPiece(new Knight(this, player));
        	this.squares[2][i].setPiece(new Bishop(this, player));
        	this.squares[5][i].setPiece(new Bishop(this, player));
        	this.squares[3][i].setPiece(new Queen(this, player));
        }
        else
        {	
        	switch(player.color){
    		case black: 
    			this.squares[i+3][0].setPiece(kingBlack = new King(this, player));
    			break;
    		case white: 
    			this.squares[i+3][0].setPiece(kingWhite = new King(this, player));
    			break;
    		case gray:
    			this.squares[i+3][0].setPiece(kingGray = new King(this, player));
    			break;
    		default:
    			break;
        	}
        	
        	this.squares[i+1][1].setPiece(new Pawn(this, player));
        	this.squares[i+3][1].setPiece(new Pawn(this, player));
        	this.squares[i+4][1].setPiece(new Pawn(this, player));
        	this.squares[i+6][1].setPiece(new Pawn(this, player));
        	
        	this.squares[i][1].setPiece(new Guardian(this, player));
        	this.squares[i+7][1].setPiece(new Guardian(this, player));
        	
        	this.squares[i+2][1].setPiece(new Ninja(this, player));
        	this.squares[i+5][1].setPiece(new Ninja(this, player));

        	this.squares[i][0].setPiece(new Rook(this, player));

        	
        	this.squares[i+7][0].setPiece(new Rook(this, player));
        	
            this.squares[i+1][0].setPiece(new Knight(this, player));
            this.squares[i+6][0].setPiece(new Knight(this, player));
            
            this.squares[i+2][0].setPiece(new Bishop(this, player));
            this.squares[i+5][0].setPiece(new Bishop(this, player));

            this.squares[i+4][0].setPiece(new Queen(this, player));
            
            System.out.println(this.squares[i+4][0].getPiece().getSymbol());
            for(int y=0; y<=1; y++){
            	for(int x=0; x<=7; x++){
            		player.getPieces().add(this.squares[i+x][y].getPiece());
            	}
            	
            }
        }
        

    }

    
    /**  method set Pawns in row
     *  @param i if 2 players ->i=row where to set pawns | if n players ->i=column from where to begin setting the pawns
     *  @param player player which is owner of pawns
     * */
    private void setPawns4NewGame(int i, Player player)
    {

        if (this.playerCount==2 && (i != 1 && i != 6))
        {
        	log.log( Level.SEVERE, "error setting pawns for 2 player");
            return;
        }
        if (this.playerCount == 3 && (i%8 != 0))
        {
        	log.log( Level.SEVERE, "error setting pawns for 3 player");
            return;
        }
        
        if (this.playerCount==2)
        {
	        for (int x = 0; x < 8; x++)
	        {
	            this.squares[x][i].setPiece(new Pawn(this, player));
	        }
        }
        else
        {
	        for (int x = i; x <= i+7; x++)
	        {
	            this.squares[x][1].setPiece(new Pawn(this, player));
	        }
        }
    }


    /** method to get reference to square from given x and y integers
     * @param x x position on chessboard
     * @param y y position on chessboard
     * @return reference to searched square
     */
    public Square getClickedSquare(int x, int y)
    { 
        if ((x > this.get_height()) || (y > this.get_widht())) //test if click is out of chessboard
        {
        	log.log( Level.INFO, "click out of chessboard.");
            return null;
        }
        
        double square_x=0;//count which field in X was clicked
        double square_y=0;//count which field in Y was clicked
        
        int i;
        
        
        if(this.playerCount==2){
        	i=1;
        if (this.settings.renderLabels)
        {
            x -= this.upDownLabel.getHeight(null);
            y -= this.upDownLabel.getHeight(null);
        }
        
        square_x = x / square_height;//count which field in X was clicked
        square_y = y / square_height;//count which field in Y was clicked

        if (square_x > (int) square_x) //if X is more than X parsed to Integer
        {
            square_x = (int) square_x + 1;//parse to integer and increment
        }
        if (square_y > (int) square_y) //if X is more than X parsed to Integer
        {
            square_y = (int) square_y + 1;//parse to integer and increment
        }

        
        }else{
        	
        	i=0;
        	double center =this.getCenterOfImage();
        	double vectorlength=Math.round(Math.sqrt(Math.pow(x-center, 2)+Math.pow(y-center, 2)));
        	square_y= 6-Math.ceil( (vectorlength - this.getCenterOffset()) / this.getSquare_height() );
        	
     
        	double angle = Math.toDegrees(Math.atan2(center-x, center-y));
        	if(angle < 0){
        		angle=angle+360;
        	}
        			
        	square_x = Math.ceil(angle / this.square_width)-1;
 	
        }
        
        log.log( Level.INFO, "square_x: " + square_x + " square_y: " + square_y + " \n");
        Square result;
        try
        {
            result = this.squares[(int) square_x - i][(int) square_y - i];
        }
        catch (java.lang.ArrayIndexOutOfBoundsException exc)
        {
        	log.log( Level.SEVERE, "!!Array out of bounds when getting Square with Chessboard.getSquare(int,int) : " + exc);
            return null;
        }
        
     
        return result;
    }
    
    
    

    /** Method selecting piece in chessboard
     * @param  sq square to select (when clicked))
     */
    public void select(Square sq)
    {
        this.activeSquare = sq;


        log.log( Level.INFO, "active_x: " + this.activeSquare.getPozX() + " active_y: " + this.activeSquare.getPozY());//needs to be modified, but is not important
        repaint();

    }/*--endOf-select--*/


    /** Method that sets variables active_x_square and active_y_square to value 0.
     */
    public void unselect()
    {
        this.activeSquare = null;

        repaint();
    }/*--endOf-unselect--*/
    
    public int get_widht()
    {
        return this.get_widht(false);
    }
    
    public int get_height()
    {
        return this.get_height(false);
    }

    public int get_widht(boolean includeLables)
    {
        return this.getHeight();
    }

    int get_height(boolean includeLabels)
    {
        if (this.settings.renderLabels)
        {
            return this.image.getHeight(null) + upDownLabel.getHeight(null);
        }
        return this.image.getHeight(null);
    }

    public int get_square_height()
    {
        int result = (int) this.square_height;
        return result;
    }
    
    
    
    
    public void promotion(Piece pawn){
    	String color;
    	
    
        switch(pawn.getPlayer().color){
        case white: color = "W";
        break;
        case black: color = "B";
        break;
        case gray: color = "G";
        break;

        default: color=null;
        }

        String newPiece = JChessApp.jcv.showPawnPromotionBox(color); //return name of new piece
        Piece promotedPiece;


        if (newPiece.equals("Queen")) // transform pawn to queen
        {
             promotedPiece = new Queen(this, pawn.getPlayer());

        }
        else if (newPiece.equals("Rook")) // transform pawn to rook
        {
             promotedPiece = new Rook(this, pawn.getPlayer());

        }
        else if (newPiece.equals("Bishop")) // transform pawn to bishop
        {
             promotedPiece = new Bishop(this, pawn.getPlayer());

        }
        else // transform pawn to knight
        {
             promotedPiece = new Knight(this, pawn.getPlayer());

        }
        
       promotedPiece.setChessboard(pawn.getChessboard());
        promotedPiece.setPlayer(pawn.getPlayer());
        promotedPiece.setSquare(pawn.getSquare());
        pawn.getSquare().setPiece(promotedPiece);

    }

    
    
    public void move(Square begin, Square end)
    {
        move(begin, end, true);
    }

    /** Method to move piece over chessboard
     * @param xFrom from which x move piece
     * @param yFrom from which y move piece
     * @param xTo to which x move piece
     * @param yTo to which y move piece
     */
    public void move(int xFrom, int yFrom, int xTo, int yTo)
    {
        Square fromSQ = null;
        Square toSQ = null;
        try
        {
            fromSQ = this.squares[xFrom][yFrom];
            toSQ = this.squares[xTo][yTo];
            this.move(fromSQ, toSQ, true);
        }
        catch (java.lang.IndexOutOfBoundsException exc)
        {
        	log.log( Level.INFO, "error moving piece: " + exc);
            return;
        }
    }

    public void move(Square begin, Square end, boolean refresh)
    {
        this.move(begin, end, refresh, true);
    }

    //needs to be modified, but at the moment I cannot tell what
    /** Method move piece from square to square
     * @param begin square from which move piece
     * @param end square where we want to move piece
     * @param refresh chessboard, default: true
     * @param clearForwardHistory boolean that defines if history should be cleared
     * */
    public void move(Square begin, Square end, boolean refresh, boolean clearForwardHistory)
    {

        castling wasCastling = Moves.castling.none;
        Piece promotedPiece = null;
        boolean wasEnPassant = false;
        if (end.getPiece() != null)
        {
            end.getPiece().square = null;
        }

        Square tempBegin = new Square(begin, this);//4 moves history
        Square tempEnd = new Square(end, this);  //4 moves history
        //for undo
        undo1_piece_begin = begin.getPiece();
        undo1_sq_begin = begin;
        undo1_piece_end = end.getPiece();
        undo1_sq_end = end;
        ifWasEnPassant = null;
        ifWasCastling = null;
        breakCastling = false;
        // ---

        twoSquareMovedPawn2 = twoSquareMovedPawn;

    	if(begin.getPiece().getName().equals("Ninja") && end.getPiece().getPlayer().equals(begin.getPiece().getPlayer()))
    	{
    		Piece piece = end.getPiece();
    		piece.square = begin;
    		begin.getPiece().square = end;//set square of piece to ending
    		end.setPiece(begin.getPiece());//for ending square set piece from beginin square
            begin.setPiece(piece);//make null piece for begining square
    	}
    	else
    	{
    		begin.getPiece().square = end;//set square of piece to ending
            end.setPiece(begin.getPiece());//for ending square set piece from beginin square
            begin.setPiece(null);//make null piece for begining square
    	}

        //if the king was moved
        if (end.getPiece().getName().equals("King"))
        {
            if (!((King) end.getPiece()).getWasMotion())
            {
            	//then castling isnt possible anymore
                breakCastling = true;
                ((King) end.getPiece()).setWasMotion(true);
            }

            int x;
            int offset;
            if(this.playerCount>2){
            offset=end.getPiece().getPlayer().getPlayerNumber()*8;
            }else{
            	offset=0;
            }
            //Castling
            if (begin.getPozX() + 2 == end.getPozX())
            {
            	x = 7+offset;
                move(squares[x][begin.getPozY()], squares[end.getPozX() - 1][begin.getPozY()], false, false);
                ifWasCastling = end.getPiece();  //for undo
                wasCastling = Moves.castling.shortCastling;

            }
            else if (begin.getPozX() - 2 == end.getPozX())
            {
            	x = 0+offset;
                move(squares[x][begin.getPozY()], squares[end.getPozX() + 1][begin.getPozY()], false, false);
                ifWasCastling = end.getPiece();  // for undo
                wasCastling = Moves.castling.longCastling;

            }
            //endOf Castling
        }
        
        
        //if the rook was moved
        else if (end.getPiece().getName().equals("Rook"))
        {
            if (!((Rook) end.getPiece()).wasMotion)
            {
            	//castling isnt possible anymore
                breakCastling = true;
                ((Rook) end.getPiece()).wasMotion = true;
            }
        }
        
        //if the pawn was moved
        else if (end.getPiece().getName().equals("Pawn"))
        {
        	if(begin.getPozY()==end.getPozY()){
        		Pawn pawn = (Pawn) end.getPiece();
        		pawn.setMoveDir(true);
        	}
        	
            if (twoSquareMovedPawn != null && squares[end.getPozX()][begin.getPozY()] == twoSquareMovedPawn.getSquare()) //en passant
            {
                ifWasEnPassant = squares[end.getPozX()][begin.getPozY()].getPiece(); //for undo

                tempEnd.setPiece(squares[end.getPozX()][begin.getPozY()].getPiece()); //ugly hack - put taken pawn in en passant plasty do end square

                squares[end.getPozX()][begin.getPozY()].setPiece(null);
                wasEnPassant = true;
            }

            if (begin.getPozY() - end.getPozY() == 2 || end.getPozY() - begin.getPozY() == 2) //moved two square
            {
                breakCastling = true;
                twoSquareMovedPawn = (Pawn) end.getPiece();
            }
            else
            {
                twoSquareMovedPawn = null; //erase last saved move (for En passant)
            }

            if (end.getPiece().getSquare().getPozY() == 0 || end.getPiece().getSquare().getPozY() == 7) //promote Pawn
            {
                if (clearForwardHistory)
                {
                   this.promotion(end.getPiece());
                }
            }
        }
        else if (!end.getPiece().getName().equals("Pawn"))
        {
            twoSquareMovedPawn = null; //erase last saved move (for En passant)
        }
        //}

        if (refresh)
        {
            this.unselect();//unselect square
            repaint();
        }

        if (clearForwardHistory)
        {
            this.moves_history.clearMoveForwardStack();
            this.moves_history.addMove(tempBegin, tempEnd, true, wasCastling, wasEnPassant, promotedPiece);
        }
        else
        {
            this.moves_history.addMove(tempBegin, tempEnd, false, wasCastling, wasEnPassant, promotedPiece);
        }
    }/*endOf-move()-*/



    
    /** Method - redo of players figure movements
     * @return true-- redo only for local game. do nothing on false
     * */    
    public boolean redo()
    {
        if ( this.settings.gameType == Settings.gameTypes.local ) 
        {
            Move first = this.moves_history.redo();

            Square from = null;
            Square to = null;

            if (first != null)
            {
                from = first.getFrom();
                to = first.getTo();

                this.move(this.squares[from.getPozX()][from.getPozY()], this.squares[to.getPozX()][to.getPozY()], true, false);
                if (first.getPromotedPiece() != null)
                {
                    Pawn pawn = (Pawn) this.squares[to.getPozX()][to.getPozY()].getPiece();
                    pawn.setSquare(null);

                    this.squares[to.getPozX()][to.getPozY()].setPiece(first.getPromotedPiece());
                    Piece promoted = this.squares[to.getPozX()][to.getPozY()].getPiece();
                    promoted.setSquare(this.squares[to.getPozX()][to.getPozY()]);
                }
                return true;
            }
            
        }
        return false;
    }

    public boolean undo()
    {
        return undo(true);
    }

    /** Method - undo of players figure movements
    * @param refresh (little note - no idea what it is)
    * @return undo if there was some movements else do nothing
    * */
    public synchronized boolean undo(boolean refresh) //undo last move 
    {
        Move last = this.moves_history.undo();


        if (last != null && last.getFrom() != null)
        {
            Square begin = last.getFrom();
            Square end = last.getTo();
            try
            {
                Piece moved = last.getMovedPiece();
                this.squares[begin.getPozX()][begin.getPozY()].setPiece(moved);

                moved.setSquare(this.squares[begin.getPozX()][begin.getPozY()]);

                Piece taken = last.getTakenPiece();
                if (last.getCastlingMove() != castling.none)
                {
                    Piece rook = null;
                    if (last.getCastlingMove() == castling.shortCastling)
                    {
                        rook = this.squares[end.getPozX() - 1][end.getPozY()].getPiece();
                        this.squares[7][begin.getPozY()].setPiece(rook);
                        rook.setSquare(this.squares[7][begin.getPozY()]);
                        this.squares[end.getPozX() - 1][end.getPozY()].setPiece(null);
                    }
                    else
                    {
                        rook = this.squares[end.getPozX() + 1][end.getPozY()].getPiece();
                        this.squares[0][begin.getPozY()].setPiece(rook);
                        rook.setSquare(this.squares[0][begin.getPozY()]);
                        this.squares[end.getPozX() + 1][end.getPozY()].setPiece(null);
                    }
                    ((King) moved).setWasMotion(false);
                    ((Rook) rook).wasMotion = false;
                    this.breakCastling = false;
                }
                else if (moved.getName().equals("Rook"))
                {
                    ((Rook) moved).wasMotion = false;
                }
                
                else if (moved.getName().equals("Pawn") && last.wasEnPassant())
                {
                    Pawn pawn = (Pawn) last.getTakenPiece();
                    this.squares[end.getPozX()][begin.getPozY()].setPiece(pawn);
                    pawn.setSquare(this.squares[end.getPozX()][begin.getPozY()]);

                }
                else if (moved.getName().equals("Pawn") && last.getPromotedPiece() != null)
                {
                    Piece promoted = this.squares[end.getPozX()][end.getPozY()].getPiece();
                    promoted.setSquare(null);
                    this.squares[end.getPozX()][end.getPozY()].setPiece(null);
                }

                //check one more move back for en passant
                Move oneMoveEarlier = this.moves_history.getLastMoveFromHistory();
                if (oneMoveEarlier != null && oneMoveEarlier.wasPawnTwoFieldsMove())
                {
                    Piece canBeTakenEnPassant = this.squares[oneMoveEarlier.getTo().getPozX()][oneMoveEarlier.getTo().getPozY()].getPiece();
                    if (canBeTakenEnPassant.getName().equals("Pawn"))
                    {
                        this.twoSquareMovedPawn = (Pawn) canBeTakenEnPassant;
                    }
                }

                if (taken != null && !last.wasEnPassant())
                {
                    this.squares[end.getPozX()][end.getPozY()].setPiece(taken);
                    taken.setSquare(this.squares[end.getPozX()][end.getPozY()]);
                }
                else
                {
                    this.squares[end.getPozX()][end.getPozY()].setPiece(null);
                }

                if (refresh)
                {
                    this.unselect();//unselect square
                    repaint();
                }

            }
            catch (java.lang.ArrayIndexOutOfBoundsException exc)
            {
                return false;
            }
            catch (java.lang.NullPointerException exc)
            {
                return false;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    
    
    
    /**
     * Method to draw Chessboard and their elements (pieces etc.)
     */
    public void draw()
    {
     
    	this.getGraphics().drawImage(image, this.getTopLeftPoint().x, this.getTopLeftPoint().y, null);//draw an Image of chessboard
       if(playerCount==2){
    	this.drawLabels();
       }
        this.repaint();
    }/*--endOf-draw--*/


    /**
     * Annotations to superclass Game updateing and painting the crossboard
     */
    @Override
    public void update(Graphics g)
    {
        repaint();
    }

    public Point getTopLeftPoint()
    {
        if (this.settings.renderLabels && this.playerCount == 2)
        {
            return new Point(this.upDownLabel.getHeight(null), this.upDownLabel.getHeight(null));
        }
        return new Point(0, 0);
    }

    @Override
    public void paintComponent(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform old = g2d.getTransform();
        
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Point topLeftPoint = this.getTopLeftPoint();
        if (this.settings.renderLabels && this.playerCount == 2)
        {
            if(topLeftPoint.x <= 0 && topLeftPoint.y <= 0) //if renderLabels and (0,0), than draw it! (for first run)
            {
                this.drawLabels();
            }
            g2d.drawImage(this.upDownLabel, 0, 0, null);
            g2d.drawImage(this.upDownLabel, 0, this.image.getHeight(null) + topLeftPoint.y, null);
            g2d.drawImage(this.LeftRightLabel, 0, 0, null);
            g2d.drawImage(this.LeftRightLabel, this.image.getHeight(null) + topLeftPoint.x, 0, null);
        }

        g2d.drawImage(image, topLeftPoint.x, topLeftPoint.y, null);//draw an Image of chessboard

 
        if (this.activeSquare != null) //if some square is active
        {
        	int x=0;
        	int y=0;
        	if(this.playerCount==2)
        	{
        		
        		x = (this.activeSquare.getPozX() * (int) square_height) + topLeftPoint.x;
                y = (this.activeSquare.getPozY() * (int) square_height) + topLeftPoint.y;
        		
        	}
        	else
        	{
        	
                int center = this.getCenterOfImage();
            	double rotangle = this.activeSquare.getPozX() * 15+187.5;
            	double distance = (5 - this.activeSquare.getPozY())*this.getSquare_height()+this.getCenterOffset()+47;
            	log.log( Level.INFO, "active square x: "+this.activeSquare.getPozX());
            	x = (int)(Math.sin(Math.toRadians(rotangle))*distance)+center;
            	y = (int)(Math.cos(Math.toRadians(rotangle))*distance)+center;

            	double rotangle2 =37.5-this.activeSquare.getPozX()*15;
            	
            	System.out.println(rotangle2);
            	g2d.rotate(Math.toRadians(rotangle2),x,y);
        	}


        	
            g2d.drawImage(sel_square, x,y, 40, 40, null);//draw image of selected square
            g2d.setTransform(old);
            
            Square tmpSquare = this.squares[this.activeSquare.getPozX()][this.activeSquare.getPozY()];
            
            if (tmpSquare.getPiece() != null)
            {
                this.moves = this.squares[this.activeSquare.getPozX()][this.activeSquare.getPozY()].getPiece().allMoves();
            }
            
            if(this.playerCount==2){
	            for (Iterator<?> it = moves.iterator(); moves != null && it.hasNext();)
	            {
	                Square sq = (Square) it.next();
	                g2d.drawImage(able_square, 
	                        (sq.getPozX() * (int) square_height) + topLeftPoint.x,
	                        (sq.getPozY() * (int) square_height) + topLeftPoint.y, null);
	            }
            }
            else
            {
                for (Iterator<?> it = moves.iterator(); moves != null && it.hasNext();)
                {
                	Square sq = (Square) it.next();

                    
                    
                    int center = this.getCenterOfImage();
                	double rotangle = sq.getPozX() * 15+187.5;
                	double distance = (5 - sq.getPozY())*this.getSquare_height()+this.getCenterOffset()+50;
   
                	x = (int)(Math.sin(Math.toRadians(rotangle))*distance)+center;
                	y = (int)(Math.cos(Math.toRadians(rotangle))*distance)+center;

                	double rotangle2 =37.5-sq.getPozX()*15;

                	g2d.rotate(Math.toRadians(rotangle2),x,y);
                    
                    
                    g2d.drawImage(able_square,x,y,45,45, null);
                    g2d.setTransform(old);
                }
            }
        }
        
        for (int x = 0; x < this.squares.length; x++)
        {
        	for (int y = 0; y < this.squares[x].length; y++)
        	{
        		Piece currentPiece = this.squares[x][y].getPiece();
        		if (currentPiece != null)
        		{
        			currentPiece.draw(g);//draw image of Piece
        		}
        	}
        }
    }/*--endOf-paint--*/

//needs to be changed
    /**
     * Method for resizing the chessboard
     * @param height The new height of the chessboard
     */
    public void resizeChessboard(int height)
    {
        BufferedImage resized = new BufferedImage(height, height, BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics g = resized.createGraphics();
 
        g.drawImage(this.image, 0, 0, height, height, null);
        g.dispose();
        this.image = resized.getScaledInstance(height, height, 0);
        this.square_height = (float) (height / 8);

        this.setSize(height, height);

        resized = new BufferedImage((int) square_height, (int) square_height, BufferedImage.TYPE_INT_ARGB_PRE);
        g = resized.createGraphics();
        g.drawImage(this.able_square, 0, 0, (int) square_height, (int) square_height, null);
        g.dispose();
        this.able_square = resized.getScaledInstance((int) square_height, (int) square_height, 0);

        resized = new BufferedImage((int) square_height, (int) square_height, BufferedImage.TYPE_INT_ARGB_PRE);
        g = resized.createGraphics();

        g.drawImage(this.sel_square, 0, 0, (int) square_height, (int) square_height, null);
        g.dispose();
        this.sel_square = resized.getScaledInstance((int) square_height, (int) square_height, 0);
        this.drawLabels();
    }

    protected void drawLabels()
    {
        this.drawLabels((int) this.square_height);
    }

    //there are no added labels for the 3 player chess
    /** Method drawing of letters, labels on chessboard
    * @param square_height height of a single square
    * */
    protected final void drawLabels(int square_height)
    {

        int min_label_height = 20;
        int labelHeight = (int) Math.ceil(square_height / 4);
        labelHeight = Math.max(labelHeight, min_label_height);
        int labelWidth =  (int) Math.ceil(square_height * 8 + (2 * labelHeight)); 
        BufferedImage uDL = new BufferedImage(labelWidth + min_label_height, labelHeight, BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D uDL2D = (Graphics2D) uDL.createGraphics();
        uDL2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        uDL2D.setColor(Color.white);

        uDL2D.fillRect(0, 0, labelWidth + min_label_height, labelHeight);
        uDL2D.setColor(Color.black);
        uDL2D.setFont(new Font("Arial", Font.BOLD, 12));
        int addX = (square_height / 2);
        if (this.settings.renderLabels)
        {
            addX += labelHeight;
        }

        String[] letters =
        {
            "a", "b", "c", "d", "e", "f", "g", "h"
        };
        if (!this.settings.upsideDown)
        {
            for (int i = 1; i <= letters.length; i++)
            {
                uDL2D.drawString(letters[i - 1], (square_height * (i - 1)) + addX, 10 + (labelHeight / 3));
            }
        }
        else
        {
            int j = 1;
            for (int i = letters.length; i > 0; i--, j++)
            {
                uDL2D.drawString(letters[i - 1], (square_height * (j - 1)) + addX, 10 + (labelHeight / 3));
            }
        }
        uDL2D.dispose();
        this.upDownLabel = uDL;

        uDL = new BufferedImage(labelHeight, labelWidth + min_label_height, BufferedImage.TYPE_3BYTE_BGR);
        uDL2D = (Graphics2D) uDL.createGraphics();
        uDL2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        uDL2D.setColor(Color.white);
  
        uDL2D.fillRect(0, 0, labelHeight, labelWidth + min_label_height);
        uDL2D.setColor(Color.black);
        uDL2D.setFont(new Font("Arial", Font.BOLD, 12));

        if (this.settings.upsideDown)
        {
            for (int i = 1; i <= 8; i++)
            {
                uDL2D.drawString(new Integer(i).toString(), 3 + (labelHeight / 3), (square_height * (i - 1)) + addX);
            }
        }
        else
        {
            int j = 1;
            for (int i = 8; i > 0; i--, j++)
            {
                uDL2D.drawString(new Integer(i).toString(), 3 + (labelHeight / 3), (square_height * (j - 1)) + addX);
            }
        }
        uDL2D.dispose();
        this.LeftRightLabel = uDL;
    }

    
    public int getCenterOfImage(){
    	return this.getImg_height() / 2;
    	
    }
    
    /**
     * Method provides the radius of the void in the middle of the chessboard
     * @return the radius of the void in the middle of the chessboard in pixel
     */
    public double getCenterOffset(){
    	return Math.round(this.getCenterOfImage()*0.255);
    	
    }
    
    public Square[][] getSquares(){
    	return this.squares;
    }
    
    
    public King getKing(Player.colors color){
    	
    	switch(color)
    	{
	    	case white: 
	    		return this.kingWhite;
	    	case black: 
	    		return this.kingBlack;
	    	case gray: 
	    		return this.kingGray;
	    	default:
	    		return null;
    	}
    	
    }
    
    public int getPlayercount(){
    	return this.playerCount;
    }
    
    
    
    public Image getImage() {
		return image;
	}


	public void setImage(Image image) {
		this.image = image;
	}


	public Image getSel_square() {
		return sel_square;
	}


	public void setSel_square(Image sel_square) {
		this.sel_square = sel_square;
	}


	public Image getAble_square() {
		return able_square;
	}


	public void setAble_square(Image able_square) {
		this.able_square = able_square;
	}


	public Square getActiveSquare() {
		return activeSquare;
	}


	public void setActiveSquare(Square activeSquare) {
		this.activeSquare = activeSquare;
	}


	public Image getUpDownLabel() {
		return upDownLabel;
	}


	public void setUpDownLabel(Image upDownLabel) {
		this.upDownLabel = upDownLabel;
	}


	public Image getLeftRightLabel() {
		return LeftRightLabel;
	}


	public void setLeftRightLabel(Image leftRightLabel) {
		LeftRightLabel = leftRightLabel;
	}


	public float getSquare_height() {
		return square_height = Math.round(this.getCenterOfImage()*0.11f);
	}


	public void setSquare_height(float square_height) {
		this.square_height = square_height;
	}


	public float getSquare_width() {
		return square_width;
	}


	public void setSquare_width(float square_width) {
		this.square_width = square_width;
	}


	public int getImg_widht() {
		return img_widht;
	}



	public int getImg_height() {
		
		return this.image.getHeight(null);
	}



	public ArrayList<?> getMoves() {
		return moves;
	}


	public void setMoves(ArrayList<?> moves) {
		this.moves = moves;
	}


	public Settings getSettings() {
		return settings;
	}


	public void setSettings(Settings settings) {
		this.settings = settings;
	}


	public King getKingWhite() {
		return kingWhite;
	}


	public void setKingWhite(King kingWhite) {
		this.kingWhite = kingWhite;
	}


	public King getKingBlack() {
		return kingBlack;
	}


	public void setKingBlack(King kingBlack) {
		this.kingBlack = kingBlack;
	}


	public King getKingGray() {
		return kingGray;
	}


	public void setKingGray(King kingGray) {
		this.kingGray = kingGray;
	}

	public Square getUndo1_sq_begin() {
		return undo1_sq_begin;
	}


	public void setUndo1_sq_begin(Square undo1_sq_begin) {
		this.undo1_sq_begin = undo1_sq_begin;
	}


	public Square getUndo1_sq_end() {
		return undo1_sq_end;
	}


	public void setUndo1_sq_end(Square undo1_sq_end) {
		this.undo1_sq_end = undo1_sq_end;
	}


	public Piece getUndo1_piece_begin() {
		return undo1_piece_begin;
	}


	public void setUndo1_piece_begin(Piece undo1_piece_begin) {
		this.undo1_piece_begin = undo1_piece_begin;
	}


	public Piece getUndo1_piece_end() {
		return undo1_piece_end;
	}


	public void setUndo1_piece_end(Piece undo1_piece_end) {
		this.undo1_piece_end = undo1_piece_end;
	}


	public Piece getIfWasEnPassant() {
		return ifWasEnPassant;
	}


	public void setIfWasEnPassant(Piece ifWasEnPassant) {
		this.ifWasEnPassant = ifWasEnPassant;
	}


	public Piece getIfWasCastling() {
		return ifWasCastling;
	}


	public void setIfWasCastling(Piece ifWasCastling) {
		this.ifWasCastling = ifWasCastling;
	}


	public boolean isBreakCastling() {
		return breakCastling;
	}


	public void setBreakCastling(boolean breakCastling) {
		this.breakCastling = breakCastling;
	}


	public Pawn getTwoSquareMovedPawn() {
		return twoSquareMovedPawn;
	}


	public void setTwoSquareMovedPawn(Pawn twoSquareMovedPawn) {
		this.twoSquareMovedPawn = twoSquareMovedPawn;
	}


	public Pawn getTwoSquareMovedPawn2() {
		return twoSquareMovedPawn2;
	}


	public void setTwoSquareMovedPawn2(Pawn twoSquareMovedPawn2) {
		this.twoSquareMovedPawn2 = twoSquareMovedPawn2;
	}


	public Moves getMoves_history() {
		return moves_history;
	}


	public void setMoves_history(Moves moves_history) {
		this.moves_history = moves_history;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public static Logger getLog() {
		return log;
	}


	public static int getTop() {
		return top;
	}


	public static int getBottom() {
		return bottom;
	}
	


	public void setPlayercount(int playercount) {
		this.playerCount = playercount;
	}


	public void setSquares(Square[][] squares) {
		this.squares = squares;
	}


	public void componentMoved(ComponentEvent e)
    {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void componentShown(ComponentEvent e)
    {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void componentHidden(ComponentEvent e)
    {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
