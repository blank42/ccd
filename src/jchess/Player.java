package jchess;

import java.io.Serializable;
import java.util.ArrayList;

import jchess.pieces.Piece;


/**
 * Class representing the player in the game
 */
public class Player implements Serializable
{
	private static final long serialVersionUID = 2319652568935547732L;
	
	public String name;
	private int playerNumber;
	private ArrayList<Piece> Pieces;

	


    public enum colors
    {

        white, black, gray
    }
    
    public colors color;
    


    public enum playerTypes
    {

        localUser, networkUser, computer
    }
    public playerTypes playerType;
    public boolean goDown;



    public Player(String name, String color, int playerNumber)
    {

    	this.setPlayerNumber(playerNumber);
        this.name = name;
        this.color = colors.valueOf(color);
        this.goDown = false;
        this.Pieces=new ArrayList<Piece>();
    }

    /** Method setting the players name
     *  @param name name of player
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /** Method getting the players name
     *  @return name of player
     */
    String getName()
    {
        return this.name;
    }

    /** Method setting the players type
     *  @param type type of player - enumerate
     */
    public void setType(playerTypes type)
    {
        this.playerType = type;
    }

	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}

	public ArrayList<Piece> getPieces() {
		return Pieces;
	}

	public void setPieces(ArrayList<Piece> pieces) {
		Pieces = pieces;
	}

	public colors getColor() {
		return color;
	}

	public void setColor(colors color) {
		this.color = color;
	}

	public playerTypes getPlayerType() {
		return playerType;
	}

	public void setPlayerType(playerTypes playerType) {
		this.playerType = playerType;
	}

	public boolean isGoDown() {
		return goDown;
	}

	public void setGoDown(boolean goDown) {
		this.goDown = goDown;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
