/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sławomir Lach ( matlak, msl )
 * Damian Marciniak
 */
package jchess;

import java.io.Serializable;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


/** Class representings game settings available for the current player
 */
public class Settings implements Serializable
{
	private static final long serialVersionUID = -4472295987572402658L;
	
	private static final Logger log = Logger.getLogger( Settings.class.getName() );
    private static ResourceBundle loc = null;
    public int timeForGame;
    public boolean runningChat;
    public boolean runningGameClock;
    public boolean timeLimitSet;//tel us if player choose time 4 game or it's infinity
    public boolean upsideDown;

    public enum gameStatus
    {
        newGame, loadGame
    }
    public gameStatus gameStat;
    public enum gameModes
    {
        standard, circle
    }
    public gameStatus gameMode;
    public Player[] player;

    public enum gameTypes
    {
        local, network
    }
    public gameTypes gameType;
    public boolean renderLabels = true;

    public Settings(int playerCount)
    {
    	String[] playerNames = {"white", "black", "gray"};
        //temporally
        this.player = new Player[playerCount];
        for (int i = 0; i < this.player.length; i++)
        {
        	this.player[i] = new Player("", playerNames[i],i);
        }

        gameStat = gameStatus.newGame;
    }

    /** Method to get game time set by player
     *  @return int with how long the game will lasts
     */
    public int getTimeForGame()
    {
        return this.timeForGame;
    }

    /** Method which return value language resource (default English)
     * @param key to search in language resource
     * @return string with value of key in selected language
     */
    public static String lang(String key)
    {
        if (Settings.loc == null)
        {
            Settings.loc = PropertyResourceBundle.getBundle("jchess.resources.i18n.main");
            Locale.setDefault(Locale.ENGLISH);
        }
        String result = "";
        try
        {
            result = Settings.loc.getString(key);
        }
        catch (java.util.MissingResourceException exc)
        {
            result = key;
        }
        log.log( Level.INFO, Settings.loc.getLocale().toString());
        return result;
    }
}
