/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sławomir Lach ( matlak, msl )
 * Damian Marciniak
 */
package jchess;

import java.awt.*;
import javax.swing.JPanel;

import java.util.logging.Level;
import java.util.logging.Logger;

/** 
 * Class to representing the full game time
 */
public class GameClock extends JPanel implements Runnable
{
	private static final long serialVersionUID = -3448239491901370506L;
	
	private static final Logger log = Logger.getLogger( GameClock.class.getName() );
    public Clock[] clocks;
    private Clock runningClock;
    private Settings settings;
    private Thread thread;
    private Game game;
    private String[] playerClocks;

    GameClock(Game game)
    {
        super();
        clocks = new Clock[game.playerCount];
        playerClocks = new String[game.playerCount];
        for (int i = 0; i < game.playerCount; i++)
        {
        	clocks[i] = new Clock();
        }
        this.runningClock = this.clocks[0];//running/active clock
        this.game = game;
        this.settings = game.settings;
        int time = this.settings.getTimeForGame();

        this.setTimes(time);
        
        this.setPlayers(this.settings.player);

        this.thread = new Thread(this);
        if (this.settings.timeLimitSet)
        {
            thread.start();
        }
        this.setDoubleBuffered(true);
    }

    /** Method to initialize game clock
     */
    public void start()
    {
        this.thread.start();
    }

    /** Method to stop game clock
     */
    public void stop()
    {
        this.runningClock = null;

        try
        {//block this thread
            this.thread.wait();
        }
        catch (java.lang.InterruptedException exc)
        {
        	log.log( Level.SEVERE, "Error blocking thread: " + exc);
        }
        catch (java.lang.IllegalMonitorStateException exc1)
        {
        	log.log( Level.SEVERE, "Error blocking thread: " + exc1);
        }
    }

    /**
     * Draws the player names and their clocks.
     * @param g Graphics2D object to paint
     */
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        for(int i = 0; i < game.playerCount; i++)
        {
        	playerClocks[i] = this.clocks[i].prepareString();
        }
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        // Draw background of player names
        g2d.setColor(Color.RED);
        g2d.fillRect(0, 30, 90, 30);
        g2d.setColor(Color.BLUE);
        g2d.fillRect(90, 30, 90, 30);
        if (game.playerCount >= 3)
        {
        	g2d.setColor(Color.GREEN);
            g2d.fillRect(180, 30, 90, 30);
        }
        
        // Draw frames of player names and their clocks
        g2d.setColor(Color.BLACK);
        for(int i = 0; i < game.playerCount; i++)
        {
        	g2d.drawRect(i * 90, 30, 90, 30);
        	g2d.drawRect(i * 90, 60, 90, 30);
        }
        
        // Draw player names
        Font font = new Font("Serif", Font.ITALIC, 14);
        g2d.setFont(font);
        g.drawString(settings.player[0].getName(), 10, 50); //plaxerWhite
        g.setColor(Color.WHITE);
        g.drawString(settings.player[1].getName(), 100, 50); //playerBlack
        g.setColor(Color.BLACK);
        for (int i = 2; i < game.playerCount; i++)
        {
        	g.drawString(settings.player[2].getName(), 10 + i * 90, 50); //plaxerWhite
            g.setColor(Color.GRAY);
        }
        
        // Draw clocks of each player
        for (int i = 0; i < playerClocks.length; i++)
        {
        	g2d.drawString(playerClocks[i], 10 + i * 90, 80);
        }
    }

    /**
    Annotation to superclass Graphics updateing clock graphisc
     * @param g Graphics2D Capt object to paint
     */
    @Override
    public void update(Graphics g)
    {
        paint(g);
    }

    /** Method of swiching the players clocks
     */
    public void switch_clocks()
    {
        /*in documentation this method is called 'switch', but it's restricted name
        to switch block (in pascal called "case") - this've to be repaired in documentation by Wąsu:P*/
        if (this.runningClock == this.clocks[0])
        {
            this.runningClock = this.clocks[1];
        }
        else if (this.runningClock == this.clocks[1])
        {
        	// set to clock 0 if 2 player, set to clock 2 if 3 player
            this.runningClock = this.clocks[2%game.playerCount];
        }
        else
        {
        	this.runningClock = this.clocks[0];
        }
    }

    /** Method with is setting the players clocks time
     * @param t Contains times of players
     */
    public void setTimes(int t)
    {
        /*rather in chess game players got the same time 4 game, so why in documentation
         * this method've 2 parameters ? */
    	
    	for(int i = 0; i < game.playerCount; i++)
    	{
    		this.clocks[i].init(t);
    	}
    }

    /** Method with is setting the players clocks
     * @param p Contains player informations
     */
    private void setPlayers(Player[] p)
    {
        /*in documentation it's called 'setPlayer' but when we've 'setTimes' better to use
         * one convention of naming methods - this've to be repaired in documentation */
    	if(game.playerCount != p.length)
    	{
    		log.log(Level.SEVERE, "Transfered variables length (Player) unequals playerCount!");
    	}
    	
		for (int i = 0; i < p.length; i++)
		{
			this.clocks[i].setPlayer(p[i]);
		}
    }

    /** Method with is running the time on clock
     */
    public void run()
    {
        while (true)
        {
            if (this.runningClock != null)
            {
                if (this.runningClock.decrement())
                {
                    repaint();
                    try
                    {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e)
                    {
                    	log.log( Level.SEVERE, "Some error in gameClock thread: " + e);
//                        System.out.println("Some error in gameClock thread: " + e);
                    }
                    //if(this.game.blockedChessboard)
                    //  this.game.blockedChessboard = false;
                }
                if (this.runningClock != null && this.runningClock.get_left_time() == 0)
                {
                    this.timeOver();
                }
            }
        }
    }

    /** Method of checking is the time of the game is not over
     */
    private void timeOver()
    {
        String color = new String();
        if (this.clocks[0].get_left_time() == 0)
        {//Check which player win
            color = this.clocks[1].getPlayer().color.toString();
        }
        else if (this.clocks[1].get_left_time() == 0)
        {
            color = this.clocks[0].getPlayer().color.toString();
        }
        else
        {//if called in wrong moment
        	log.log( Level.WARNING, "Time over called when player got time 2 play");
        }
        this.game.endGame("Time is over! " + color + " player win the game.");
        this.stop();

        //JOptionPane.showMessageDialog(this, "koniec czasu");
    }
}
