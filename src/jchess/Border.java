package jchess;

import java.util.ArrayList;

/**
 * Class for the border datatype which is needed for balancing on the circular chessboard.
 */
public class Border {

	private ArrayList<Square>leftBorderSquares;
	private ArrayList<Square>rightBorderSquares;
	private int borderHeight;
	
	public Border(ArrayList<Square> rightBorderSquares,ArrayList<Square> leftBorderSquares, int borderHeight){
		this.leftBorderSquares=leftBorderSquares;
		this.rightBorderSquares=rightBorderSquares;
		this.borderHeight= borderHeight;
	}

	public ArrayList<Square> getLeftBorderSquares() {
		return leftBorderSquares;
	}


	public void setLeftBorderSquares(ArrayList<Square> leftBorderSquares) {
		this.leftBorderSquares = leftBorderSquares;
	}


	public ArrayList<Square> getRightBorderSquares() {
		return rightBorderSquares;
	}


	public void setRightBorderSquares(ArrayList<Square> rightBorderSquares) {
		this.rightBorderSquares = rightBorderSquares;
	}


	public int getBorderHeight() {
		return borderHeight;
	}


	public void setBorderHeight(int borderHeight) {
		this.borderHeight = borderHeight;
	}
}
