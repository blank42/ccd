package jchess;

import jchess.pieces.Piece;


/**
 * Class to represent a chessboard square
 */
public class Square
{

	private int pozX; 			// 0-7, because 8 squares for row/column
    private int pozY; 			// 0-7, because 8 squares for row/column
    public Piece piece = null;	//object Piece on square (and extending Piece)

    private Chessboard chessboard;

    
    public Square(int pozX, int pozY, Piece piece, Chessboard chessboard)
    {
    	this.chessboard = chessboard;
        this.pozX = pozX;
        this.pozY = pozY;
        this.piece = piece;
    }
    
    public Square(Square square, Chessboard chessboard)
    {
    	this.chessboard = chessboard;
        this.pozX = square.pozX;
        this.pozY = square.pozY;
        this.piece = square.piece;
    }
    
    
    public Square clone(Square square)
	{
        return new Square(square, this.chessboard);
    }

    public void setPiece(Piece piece)
    {
    	this.piece = piece;
    	if(piece != null)
    	{
    		this.piece.setSquare(this);
    	}
	}

    public Square getSquareInTheEastDir(int distance){
 	   
		if(this.pozX+distance <= this.chessboard.getSquares().length-1)
		{
			return this.chessboard.getSquares()[this.pozX+distance][this.pozY];
		}
		else
		{
			if(this.chessboard.getPlayercount()==2)
			{
				return null;
			}
			else
			{
				int distanceToRightBorder = this.chessboard.getSquares().length-this.pozX;
				int distanceFromLeftBorder = distance-distanceToRightBorder;
				return this.chessboard.getSquares()[distanceFromLeftBorder][this.pozY];
			}
		}
	}
   
	public Square getSquareInTheWestDir(int distance){
		// if we've not reached the left end of the chessboard
		if(this.pozX-distance >= 0)
		{
			return this.chessboard.getSquares()[this.pozX-distance][this.pozY];  
		}
		else
		{
			if(this.chessboard.getPlayercount()==2)
			{
				return null;
			}
			else
			{
				int distanceToLeftBorder = distance-this.pozX;
				int distanceFromRightBorder = this.chessboard.getSquares().length-distanceToLeftBorder;
				return this.chessboard.getSquares()[distanceFromRightBorder][this.pozY];
			}
		}
	}
   
	public Square getSquareInTheNorthDir(int distance){
		 //if we've not reached the center of the chessboard
		if(this.pozY+distance <= this.chessboard.getSquares()[this.pozX].length-1)
		{
			return this.chessboard.getSquares()[this.pozX][this.pozY+distance];
		}
		else
		{
			// if two player chess we're at the end of the chessboard
			if(this.chessboard.getPlayercount()==2)
			{
				
				return null;
			}
			else // if players>2 we're on the otherside of the chessboard
			{
				int distanceToTopBorder = (this.chessboard.getSquares()[0].length-1) - this.pozY;
				int distanceFromTopBorder = this.chessboard.getSquares()[0].length - distance - distanceToTopBorder;
				
				if(this.pozX+12 <= this.chessboard.getSquares().length-1)
				{
					return this.chessboard.getSquares()[this.pozX+12][distanceFromTopBorder];
				}
				else
				{
					int disttorightborder = this.chessboard.getSquares().length-this.pozX;
			   
					int x = 12 - disttorightborder;
					return this.chessboard.getSquares()[x][distanceFromTopBorder];
				}
			}
		}
	}
   
	public Square getSquareInTheSouthDir(int distance){
		if(this.pozY-distance >= 0)
		{
			return this.chessboard.getSquares()[this.pozX][this.pozY-distance];
		}
		else
		{
			return null;
		}
	}
   

	public Square getSquareInDir(Chessboard.cardinalDirections direction){
		return getSquareInDir(1, direction);
	}
   
   public Square getSquareInDir(int distance, Chessboard.cardinalDirections direction){

	   switch(direction){
	   case east: return this.getSquareInTheEastDir(distance);
	   case north: return this.getSquareInTheNorthDir(distance);
	   case west: return this.getSquareInTheWestDir(distance);
	   case south: return this.getSquareInTheSouthDir(distance);
	   case northwest: 
		   if(this.getSquareInTheNorthDir(distance)!=null)return this.getSquareInTheNorthDir(distance).getSquareInTheWestDir(distance);
		   break;
	   case northeast: 
		   if(this.getSquareInTheNorthDir(distance)!=null)return this.getSquareInTheNorthDir(distance).getSquareInTheEastDir(distance);
		   break;
	   case southwest: 
		   if(this.getSquareInTheSouthDir(distance)!=null)return this.getSquareInTheSouthDir(distance).getSquareInTheWestDir(distance);
		   break;
	   case southeast: 
		   if(this.getSquareInTheSouthDir(distance)!=null)return this.getSquareInTheSouthDir(distance).getSquareInTheEastDir(distance);
		   break;
	   default: return null;
	   }
	   
	   return null;
   }
   
   public int getPozX(){
	   return this.pozX;
	}
   
	public int getPozY()
	{
	   return this.pozY;
	}
   
	public Piece getPiece()
	{
	   return this.piece;
	}
   
	public void setPozX(int pozX)
	{
	   this.pozX=pozX;
	}
   
	public void setPozY(int pozY)
	{
	   this.pozY=pozY;
	}

	public Chessboard getChessboard()
	{
		return chessboard;
	}

	public void setChessboard(Chessboard chessboard)
	{
		this.chessboard = chessboard;
	}
}
