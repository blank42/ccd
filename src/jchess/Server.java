/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jchess;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jchess.server.*;

/**
 * Class responsible for server references: For running the server,
 * settings of the players, for clients references on the server
 * and references of observators
 */

public class Server implements Runnable
{
	private static final Logger log = Logger.getLogger( Server.class.getName() );
    public static boolean isPrintEnable = true; //print all messages (print function)

    private static Map<Integer, Table> tables;
    public static int port=4449;
    private static ServerSocket ss;
    private static boolean isRunning=false;

    public static enum connection_info
    {
        all_is_ok(0),
        err_bad_table_ID(1),
        err_table_is_full(2),
        err_game_without_observer(3),
        err_bad_password(4);

        private int value;

        connection_info(int value)
        {
            this.value = value;
        }

        public static connection_info get(int id)
        {
            switch(id)
            {
                case 0:
                    return connection_info.all_is_ok;
                case 1:
                    return connection_info.err_bad_table_ID;
                case 2:
                    return connection_info.err_table_is_full;
                case 3:
                    return connection_info.err_game_without_observer;
                case 4:
                    return connection_info.err_bad_password;
                default:
                    return null;
            }
        }

        public int getValue()
        {
            return value;
        }
    }

    public Server()
    {
        if(!Server.isRunning) //run server if isn't running previous
        {
            runServer();

            Thread thread = new Thread(this);
            thread.start();

            Server.isRunning = true;
        }
    }

    /*
     * Method with is checking is the server is running
     * @return bool true if server is running, else false
     */
    public static boolean isRunning() //is server running?
    {
        return isRunning;
    }

    /*
     * Method to starting a new server
     * It's running a new game server
     */

    private static void runServer() //run server
    {
        try
        {
            ss = new ServerSocket(port);
            print("running");
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

        tables = new HashMap<Integer, Table>();
    }

    public void run() //listening
    {
        print("listening port: "+port);
        while(true)
        {
            Socket s;
            ObjectInputStream input;
            ObjectOutputStream output;

            try
            {
                s = ss.accept();
                input = new ObjectInputStream(s.getInputStream());
                output = new ObjectOutputStream(s.getOutputStream());

                print("new connection");

                //readed all data
                int tableID = input.readInt();
                print("readed table ID: "+tableID);
                boolean joinAsPlayer = input.readBoolean();
                print("readed joinAsPlayer: "+joinAsPlayer);
                String nick = input.readUTF();
                print("readed nick: "+nick);
                String password = input.readUTF();
                print("readed password: "+password);
                //---------------

                if(!tables.containsKey(tableID))
                {
                    print("bad table ID");
                    output.writeInt(connection_info.err_bad_table_ID.getValue());
                    output.flush();
                    continue;
                }
                Table table = tables.get(tableID);

                if(!table.password.equals(password))
                {
                    print("bad password");
                    output.writeInt(connection_info.err_bad_password.getValue());
                    output.flush();
                    continue;
                }
                 
                if(joinAsPlayer)
                {
                    print("join as player");
                    if(table.isAllPlayers())
                    {
                        print("error: was all players at this table");
                        output.writeInt(connection_info.err_table_is_full.getValue());
                        output.flush();
                        continue;
                    }
                    else
                    {
                        print("wasn't all players at this table");

                        output.writeInt(connection_info.all_is_ok.getValue());
                        output.flush();

                        table.addPlayer(new SClient(s, input, output, nick, table));
                        table.sendMessageToAll("** Gracz "+nick+" dołączył do gry **");

                        if(table.isAllPlayers())
                        {
                            table.generateSettings();

                            print("Send settings to all");
                            table.sendSettingsToAll();

                            table.sendMessageToAll("** Nowa gra, zaczna: "+table.clientPlayer1.nick+"**");
                        }
                        else
                        {
                            table.sendMessageToAll("** Oczekiwanie na drugiego gracza **");
                        }
                    }
                }
                else//join as observer
                {
                    print("join as observer");
                    if(!table.canObserversJoin())
                    {
                        print("Observers can't join");
                        output.writeInt(connection_info.err_game_without_observer.getValue());
                        output.flush();
                        continue;
                    }
                    else
                    {
                        output.writeInt(connection_info.all_is_ok.getValue());
                        output.flush();

                        table.addObserver(new SClient(s, input, output, nick, table));

                        if(table.clientPlayer2 != null) //all players is playing
                        {
                            table.sendSettingsAndMovesToNewObserver();
                        }

                        table.sendMessageToAll("** Obserwator "+nick+" dołączył do gry **");
                    }
                }
            }
            catch (IOException ex)
            {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
        }
    }

    /*
     * Method with is printing the servers message
     *
     */
    private static void print(String str)
    {
        if(isPrintEnable)
        	log.log( Level.INFO, "Server: "+str);
//            System.out.println("Server: "+str);
    }

    /*
     * Method with is creating a new table
     * @param idTable int witch number of the table
     * @param password String with password
     * @param withObserver bool true if observers available, else false
     * @param enableChat bool true if chat enable, else false
     */
    public void newTable(int idTable, String password, boolean withObserver, boolean enableChat) //create new table
    {
        print("create new table - id: "+idTable);
        tables.put(idTable, new Table(password, withObserver, enableChat));
    }
}
