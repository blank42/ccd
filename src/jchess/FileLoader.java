package jchess;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
* Class responsible to correctly load the resources.
*/
public class FileLoader {

	private static final Logger log = Logger.getLogger( FileLoader.class.getName() );
	static final public Properties configFile = FileLoader.getConfigFile();
	
    public static JarFile jarFile;
	
    /**
     * Method that checks if program is running from IDE or jar.
     * @return true if program is running from jar
     */
    public static boolean runningFromJar()
    {
    	URL referenceURL = FileLoader.class.getResource("FileLoader.class");
    	if(referenceURL.toString().startsWith("jar:"))
    		return true;
    	return false;
    }
    
	/**
	 * Method that gets the path of a specific folder or file.
	 * @param pathEnding specified folder or file as String
	 * 		  path should start from the parent project folder (for example "jchess/resources/mode")
	 * @return path of the specified folder or file
	 */
	public static String getJarPath(String pathEnding)
    {
    	URL referenceURL = FileLoader.class.getResource("FileLoader.class");
    	String path = "";
    	
    	if(runningFromJar())
    	{
			try {
				JarURLConnection jarConnection = (JarURLConnection)referenceURL.openConnection();
				String testPath = jarConnection.getJarFileURL().toString();
				testPath = testPath.replace("%20", " ");
				testPath = testPath.replace("/", File.separator);

    			jarFile = jarConnection.getJarFile();
    			path = testPath;
    			//log.log(Level.INFO, "Jar path: " + path);
			}
			catch (IOException e)
			{
				log.log(Level.INFO, "Error loading jar file from URL");
				e.printStackTrace();
			}
    	} else {
            path = FileLoader.class.getProtectionDomain().getCodeSource().getLocation().getFile();
            path = path.replaceAll("[a-zA-Z0-9%!@#$%^&*\\(\\)\\[\\]\\{\\}\\.\\,\\s]+\\.jar", "");
            int lastSlash = path.lastIndexOf(File.separator); 
            if(path.length()-1 == lastSlash)
            {
                path = path.substring(0, lastSlash);
            }
            path = path.replace("%20", " ");
            path += pathEnding;
    	}
        return path;
    }
	
	/**
	 * Method that gets the configuration file from the jchess package.
	 * @return configuration file with properties
	 */
	public static Properties getConfigFile()
    {
        Properties defConfFile = new Properties();
        Properties confFile = new Properties();
        File outFile = new File(FileLoader.getJarPath("") + File.separator + "config.txt");
        try
        {
            defConfFile.load(FileLoader.class.getResourceAsStream("config.txt"));
        }
        catch (java.io.IOException exc)
        {
        	log.log( Level.SEVERE, "Error loading config file as resource stream! What occured: " + exc);
            exc.printStackTrace();
        }
        if (outFile.exists())
        {
            try
            {
                defConfFile.store(new FileOutputStream(outFile), null);
            }
            catch (java.io.IOException exc)
            {
            	log.log( Level.SEVERE, "Error storing config file! What occured: " + exc);
                exc.printStackTrace();
            }
        }
        try
        {   
            confFile.load(new FileInputStream("config.txt"));
        }
        catch (java.io.IOException exc)
        {
        	log.log( Level.INFO, "Error loading config file! Maybe it does not exist yet.");
        }
        return confFile;
    }
	
	/**
	 * Method that loads a specific image from a specific folder within the resources folder.
	 * @param path String of the relative path to the file
	 * @param name String of the specific image to load for example "chessboard.jpg"
	 * @return the searched image or null if it cannot be found
	 */
    public static Image loadImageFromFolder(String path, String name)
    {
        if (configFile == null)
        {
            return null;
        }
        Image img = null;
        URL url = null;
        Toolkit tk = Toolkit.getDefaultToolkit();
        try
        {
            String imageLink = "resources/" + path + "/" + name;
            log.log( Level.INFO, "Path is: "+ imageLink);
            url = JChessApp.class.getResource(imageLink);
            img = tk.getImage(url);

        }
        catch (Exception e)
        {
            System.out.println("some error loading image!");
            e.printStackTrace();
        }
        return img;
    }
    
    /**
     * Method that loads a specific image from a given theme.
     * @param name string of image to load for example "chessboard.jpg"
     * @return the searched image or null if it cannot be found
     */
    public static Image loadImageFromTheme(String name)
    {
        if (configFile == null)
        {
            return null;
        }
        
        Image img = null;
        URL url = null;
        try
        {
            String imageLink = "resources/" + "theme/" + configFile.getProperty("THEME", "default") + "/images/" + name;
//            log.log( Level.INFO, configFile.getProperty("THEME"));
            url = JChessApp.class.getResource(imageLink);
            img = ImageIO.read(url);

        }
        catch (Exception e)
        {
        	log.log( Level.SEVERE, "some error loading image!");
            e.printStackTrace();
        }
        return img;
    }
    
    /**
     * Method that stores the entries of a folder in a file array.
	 * @param relativePath specified folder or file as String
	 * 		  path should start from the parent project folder (for example "jchess/resources/mode")
	 * @param areFolderNames defines whether the wanted entries are folders or files
     * @return array with file entries
     */
    public static File[] getEntryList(String relativePath, boolean areFolderNames)
    {
    	File[] files = new File[0];
        File dir = new File("");
    	
        if(FileLoader.runningFromJar())
        {
            Enumeration<JarEntry> entries = FileLoader.jarFile.entries();
            Set<String> result = new HashSet<String>();
            while(entries.hasMoreElements())
            {
                String name = entries.nextElement().getName();
                
                if(areFolderNames)
                {
                    if (name.startsWith(relativePath) && name.endsWith("/")) //filter according to the path
                    {
                    	String entry = name.substring(relativePath.length());                	
                    	int count = 0;
                    	for(int i=0; i <entry.length();i++)
                    	{
                    	  if(entry.charAt(i) == '/') count++;
                    	}
                    	if(count <= 2) result.add(entry);
                    }
                }
                else
                {
                    if (name.startsWith(relativePath))
                    	result.add(name);
                }
            }
            String[] names = result.toArray(new String[result.size()]);
            
            // fill file[] with all names
            files = Arrays.stream(names).map(s -> new File(s)).toArray(size -> new File[names.length]);
            
        }
        else
        {
 	        dir = new File(FileLoader.getJarPath(relativePath));
 	        log.log( Level.INFO, "Mode path: "+dir.getPath());
        	
        	files = dir.listFiles();
        }
    	return files;
    }
}
