package jchess;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;

import jchess.pieces.King;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameCircle extends Game implements MouseListener, ComponentListener {

	private static final long serialVersionUID = 1049592013985247634L;

	private static final Logger log = Logger.getLogger( GameCircle.class.getName() );
	public GameCircle()
    {
		this.playerCount = 3;
		
        this.setLayout(null);
        this.moves = new Moves(this);
        settings = new Settings(3);
        
        chessboard = new Chessboard(this, this.settings, this.moves, playerCount, 2);
        
        chessboard.addMouseListener(this);
        this.add(chessboard);
        //this.chessboard.
        gameClock = new GameClock(this);
        gameClock.setSize(new Dimension(playerCount * 100, 100));
        gameClock.setLocation(new Point(500, 0));
        this.add(gameClock);

        JScrollPane movesHistory = this.moves.getScrollPane();
        movesHistory.setSize(new Dimension(playerCount * 90, 350));
        movesHistory.setLocation(new Point(500, 121));
        this.add(movesHistory);

        this.chat = new Chat();
        this.chat.setSize(new Dimension(380, 100));
        this.chat.setLocation(new Point(0, 500));
        this.chat.setMinimumSize(new Dimension(400, 100));

        this.blockedChessboard = false;
        this.setLayout(null);
        this.addComponentListener(this);
        this.setDoubleBuffered(true);
    }
	
	/**
	 * Method to save actual state of game
     * @param path address of place where game will be saved
     */
	@Override
    public void saveGame(File path)
    {
        File file = path;
        FileWriter fileW = null;
        try
        {
            fileW = new FileWriter(file);
        }
        catch (java.io.IOException exc)
        {
            System.err.println("error creating fileWriter: " + exc);
            JOptionPane.showMessageDialog(this, Settings.lang("error_writing_to_file")+": " + exc);
            return;
        }
        Calendar cal = Calendar.getInstance();
        String str = new String("");
        String info = new String("[Event \"GameCircle\"]\n[Date \"" + cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1) + "." + cal.get(Calendar.DAY_OF_MONTH) + "\"]\n");
        for(int i = 0; i < playerCount; i++)
        {
        	info += "[" + this.settings.player[i].color + " \"" + this.settings.player[i].name + "\"]\n";
        }
        str += info;
        str += this.moves.getMovesInString();
        try
        {
            fileW.write(str);
            fileW.flush();
            fileW.close();
        }
        catch (java.io.IOException exc)
        {
            System.out.println("error writing to file: " + exc);
            JOptionPane.showMessageDialog(this, Settings.lang("error_writing_to_file")+": " + exc);
            return;
        }
        JOptionPane.showMessageDialog(this, Settings.lang("game_saved_properly"));
    }
	
	/**
	 * Loading game method(loading game state from the earlier saved file)
     * @param file File where is saved game
     */
    static public void loadGame(File file)
    {
        FileReader fileR = null;
        try
        {
            fileR = new FileReader(file);
        }
        catch (java.io.IOException exc)
        {
        	log.log( Level.SEVERE,"Something wrong reading file: " + exc);
            return;
        }
        BufferedReader br = new BufferedReader(fileR);
        String tempStr = new String();
        String blackName, whiteName, grayName;
        try
        {
            tempStr = getLineWithVar(br, new String("[white"));
            whiteName = getValue(tempStr);
            tempStr = getLineWithVar(br, new String("[black"));
            blackName = getValue(tempStr);
            tempStr = getLineWithVar(br, new String("[gray"));
            grayName = getValue(tempStr);
            tempStr = getLineWithVar(br, new String("1."));
        }
        catch (ReadGameError err)
        {
        	log.log( Level.SEVERE,"Error reading file: " + err);

            return;
        }
        Game newGUI = (GameCircle) JChessApp.jcv.addNewTab(whiteName + " vs. " + blackName + " vs. " + grayName, "circle");
        Settings locSetts = newGUI.settings;
        locSetts.player[0].name = whiteName;
        locSetts.player[1].name = blackName;
        locSetts.player[2].name = grayName;
        locSetts.player[0].setType(Player.playerTypes.localUser);
        locSetts.player[1].setType(Player.playerTypes.localUser);
        locSetts.player[2].setType(Player.playerTypes.localUser);
        locSetts.gameStat = Settings.gameStatus.loadGame;
        locSetts.gameType = Settings.gameTypes.local;

        newGUI.newGame();
        newGUI.blockedChessboard = true;
        newGUI.moves.setMoves(tempStr);
        newGUI.blockedChessboard = false;
        newGUI.chessboard.repaint();
    }

    /**
     * Method to Start new game
     */
    public void newGame()
    {
        chessboard.setPieces4NewGame("", settings.player);

 
        
        activePlayer = settings.player[0]; //set active player white
        if (activePlayer.playerType != Player.playerTypes.localUser)
        {
            this.blockedChessboard = true;
        }
        
        GameCircle activeGame = (GameCircle)JChessApp.jcv.getActiveTabGame();
        if( activeGame != null && JChessApp.jcv.getNumberOfOpenedTabs() == 0 )
        {
            activeGame.chessboard.resizeChessboard(activeGame.chessboard.get_height(false));
            activeGame.chessboard.repaint();
            activeGame.repaint();
        }
        chessboard.repaint();
        this.repaint();
    }

    /**
     * Method to swich active players after move
     */
    @Override
    public void switchActive(boolean forward)
    {
    	Image img=null;
        
        Player player = null;
        int i = 1;
        int prevActivePlayerNr=activePlayer.getPlayerNumber();
        
        while(player!=activePlayer){

        	// when redo or normal move is performed
        	if (forward)
        	{
	        	if(prevActivePlayerNr+i<=settings.player.length-1){
	        		player=settings.player[prevActivePlayerNr+i];
	        	}else{
	        		player=settings.player[0];	
	        	}
        	}
        	// when undo or normal move is performed
        	else
        	{
        		if(prevActivePlayerNr-i>=0){
	        		player=settings.player[prevActivePlayerNr-i];
	        	}else{
	        		player=settings.player[settings.player.length-1];	
	        	}
        	}
        	
        	if(player!=null){
        		activePlayer = player;
        		break;
        	}
        	i++;
        }
        
        switch(activePlayer.getPlayerNumber()){
        case 0:
        	img=FileLoader.loadImageFromTheme("chessboard_redturn.png");
        	break;
        case 1:
        	img=FileLoader.loadImageFromTheme("chessboard_blueturn.png");
        	break;
        case 2:
        	img=FileLoader.loadImageFromTheme("chessboard_greenturn.png");
        	break;
        }
        
        img=img.getScaledInstance(this.computeChessboardHeight(), this.computeChessboardHeight(), 1);
        this.chessboard.setImage(img);
        repaint();
        this.gameClock.switch_clocks();
    }

    /**
     * Method to go to next move (checks if game is local/network etc.)
     */
    @Override
    public void nextMove()
    {
        switchActive(true);

        System.out.println("next move, active player: " + activePlayer.name + ", color: " + activePlayer.color.name() + ", type: " + activePlayer.playerType.name());
        if (activePlayer.playerType == Player.playerTypes.localUser)
        {
            this.blockedChessboard = false;
        }
        else if (activePlayer.playerType == Player.playerTypes.networkUser)
        {
            this.blockedChessboard = true;
        }
        else if (activePlayer.playerType == Player.playerTypes.computer)
        {
        }
    }

    /** 
     * Method to simulate Move to check if it's correct etc. (usable for network game).
     * @param beginX from which X (on chessboard) move starts
     * @param beginY from which Y (on chessboard) move starts
     * @param endX   to   which X (on chessboard) move go
     * @param endY   to   which Y (on chessboard) move go
     * */
    @Override
    public boolean simulateMove(int beginX, int beginY, int endX, int endY)
    {
        try 
        {
            chessboard.select(chessboard.getSquares()[beginX][beginY]);
            if (chessboard.getActiveSquare().getPiece().allMoves().indexOf(chessboard.getSquares()[endX][endY]) != -1) //move
            {
                chessboard.move(chessboard.getSquares()[beginX][beginY], chessboard.getSquares()[endX][endY]);
            }
            else
            {
                System.out.println("Bad move");
                return false;
            }
            chessboard.unselect();
            nextMove();

            return true;
            
        } 
        catch(StringIndexOutOfBoundsException exc) 
        {
            return false;
        }    
        catch(ArrayIndexOutOfBoundsException exc) 
        {
            return false;
        }
        catch(NullPointerException exc)
        {
            return false;
        }
        finally
        {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, "ERROR");
        }
    }
    
    @Override
    public boolean undo()
    {
        boolean status = false;
        
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            status = chessboard.undo();
            if( status )
            {
                this.switchActive(false);
            }
            else
            {
                chessboard.repaint();//repaint for sure
            }
        }
        else if( this.settings.gameType == Settings.gameTypes.network )
        {
            this.client.sendUndoAsk();
            status = true;
        }
        return status;
    }
    
    @Override
    public boolean rewindToBegin()
    {
        boolean result = false;
        
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            while( chessboard.undo() )
            {
                result = true;
            }
        }
        else
        {
            throw new UnsupportedOperationException( Settings.lang("operation_supported_only_in_local_game") );
        }
        
        return result;
    }
    
    @Override
    public boolean rewindToEnd() throws UnsupportedOperationException
    {
        boolean result = false;
        
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            while( chessboard.redo() )
            {
                result = true;
            }
        }
        else
        {
            throw new UnsupportedOperationException( Settings.lang("operation_supported_only_in_local_game") );
        }
        
        return result;
    }
    
    @Override
    public boolean redo()
    {
        boolean status = chessboard.redo();
        if( this.settings.gameType == Settings.gameTypes.local )
        {
            if ( status )
            {
                this.nextMove();
            }
            else
            {
                chessboard.repaint();
            }
        }
        else
        {
            throw new UnsupportedOperationException( Settings.lang("operation_supported_only_in_local_game") );
        }
        return status;
    }
    
    
    @Override
    public void mousePressed(MouseEvent event)
    {
        if (event.getButton() == MouseEvent.BUTTON3) //right button
        {
            this.undo();
        }
        else if (event.getButton() == MouseEvent.BUTTON2 && settings.gameType == Settings.gameTypes.local)
        {
            this.redo();
        }
        else if (event.getButton() == MouseEvent.BUTTON1) //left button
        {

            if (!blockedChessboard)
            {
                try 
                {
                    int x = event.getX();//get X position of mouse
                    int y = event.getY();//get Y position of mouse

                    sq = chessboard.getClickedSquare(x, y);
                    if ((sq == null && sq.getPiece() == null && chessboard.getActiveSquare() == null)
                            || (this.chessboard.getActiveSquare() == null && sq.getPiece() != null && sq.getPiece().getPlayer() != this.activePlayer))
                    {
                        return;
                    }
                    
                    boolean selectedIsNotNinja = true;
                    if (chessboard.getActiveSquare() != null)
                    {
                    	selectedIsNotNinja = !chessboard.getActiveSquare().getPiece().getName().equals("Ninja");
                    }

                    if (sq.getPiece() != null && sq.getPiece().getPlayer() == this.activePlayer && sq != chessboard.getActiveSquare() && selectedIsNotNinja)
                    {

                        chessboard.unselect();
                        chessboard.select(sq);
                    }
                    else if (chessboard.getActiveSquare() == sq) //unselect
                    {
                        chessboard.unselect();
                    }
                    else if (chessboard.getActiveSquare() != null && chessboard.getActiveSquare().getPiece() != null
                            && chessboard.getActiveSquare().getPiece().allMoves().indexOf(sq) != -1) //move
                    {
                        if (settings.gameType == Settings.gameTypes.local)
                        {
                        	
                            chessboard.move(chessboard.getActiveSquare(), sq);
                        }
                        else if (settings.gameType == Settings.gameTypes.network)
                        {
                        	
                            client.sendMove(chessboard.getActiveSquare().getPozX(), chessboard.getActiveSquare().getPozY(), sq.getPozX(), sq.getPozY());
                            chessboard.move(chessboard.getActiveSquare(), sq);
                        }

                        chessboard.unselect();

                        System.out.println(activePlayer.color);

                        
                        for(Player player : settings.player){
                        	if(player!=null && !player.equals(activePlayer)){
                            King king=chessboard.getKing(player.color);

                            switch (king.isCheckmatedOrStalemated())
                            {
                                case checkmated:
                                    this.endGame("Checkmate! " + king.getPlayer().color.toString() + " player is checkmated! Everybody else has won the game, congrats ;)");
                                    break;
                                case stalemated:
                                    this.endGame("Stalemate! Draw!");
                                    break;
                                default:
                                	break;
                            }
                        	}
                        }
                        this.nextMove();
                        
                    }
                    
                } 
                catch(NullPointerException exc)
                {
                    System.err.println(exc.getMessage());
                    chessboard.repaint();
                    return;
                }
            }
            else if (blockedChessboard)
            {
                System.out.println("Chessboard is blocked");
            }
        }

    }
    
    public void endGame(Player player, String message)
    {
    	settings.player[player.getPlayerNumber()]=null;
        log.log( Level.INFO,message);
        JOptionPane.showMessageDialog(null, message);
        int counter = 0;
        
        Player lastRemainingPlayer = null;
        for(Player p : settings.player){
        	if(p!=null){
        		counter++;

        		if(counter==2){
        			break;
        		}
        		lastRemainingPlayer=p;
        	}
        }
        
        if(counter==1){
            log.log( Level.INFO,"Das Spiel ist vorbei. Spieler "+lastRemainingPlayer.getPlayerNumber()+ " hat gewonnen!");
            JOptionPane.showMessageDialog(null, "Das Spiel ist vorbei. Spieler "+lastRemainingPlayer.getPlayerNumber()+  " hat gewonnen!");
            this.blockedChessboard=true;
        }
    }

    
    
    @Override
    public void componentResized(ComponentEvent e)
    {

        int chess_height = this.computeChessboardHeight();

        this.chessboard.resizeChessboard((int)chess_height);
        chess_height = this.chessboard.getHeight();
        this.moves.getScrollPane().setLocation(new Point(chess_height + 5, 100));
        this.moves.getScrollPane().setSize(this.moves.getScrollPane().getWidth(), chess_height - 100);
        this.gameClock.setLocation(new Point(chess_height + 5, 0));
        if (this.chat != null)
        {
            this.chat.setLocation(new Point(0, chess_height + 5));
            this.chat.setSize(new Dimension(chess_height, this.getHeight() - (chess_height + 5))); 
        }
    }
}
