package jchess.dialog;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;

import jchess.FileLoader;
import jchess.JChessApp;
import jchess.Settings;

import javax.swing.event.ListSelectionEvent;
import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.FileOutputStream;

/** Class which handles window too choose game Mode
 */
public class NewGameChooseModeWindow  extends JDialog implements ActionListener, ListSelectionListener
{
	private static final long serialVersionUID = -5944497249431339556L;
	private static final Logger log = Logger.getLogger( ThemeChooseWindow.class.getName());
	
     JList<String> modesList;
     ImageIcon modePreview;
     GridBagLayout gbl;
     GridBagConstraints gbc;
     JLabel modePreviewLabel;
     JButton okButton;

     /** Constructor which creates window too choose game Mode
 	  * @param parent JFrame from calling FrameView
 	  * @throws Exception if no files can be loaded
 	  */
     public NewGameChooseModeWindow(Frame parent) throws Exception
     {
         super(parent);

         File dir = new File("");
         if(!FileLoader.runningFromJar())
         {
  	        dir = new File(FileLoader.getJarPath("jchess/resources/mode"));
  	        log.log( Level.INFO, "Mode path: "+dir.getPath());
         }
         File[] files = FileLoader.getEntryList("jchess/resources/mode", false);
         
         boolean environmentSwitch = false;
         if(!FileLoader.runningFromJar())
         {
         	environmentSwitch = (files != null && dir.exists());
         }
         else
         {
         	environmentSwitch = (files != null && FileLoader.jarFile != null);
         }
         
         if (environmentSwitch)
         {
             this.setTitle(Settings.lang("choose_mode_window_title"));
             Dimension winDim = new Dimension(550, 380);
             this.setMinimumSize(winDim);
             this.setMaximumSize(winDim);
             this.setSize(winDim);
             this.setResizable(false);
             this.setLayout(null);
             this.setDefaultCloseOperation(DISPOSE_ON_CLOSE); 
             
             String[] pngFiles = new String[files.length];
             for (int i = 0; i < files.length; i++)
             {
            	 String name = files[i].getName();
            	 if (name.endsWith(".png"))
            	 {
            		 pngFiles[i] = name.replace(".png", "");
            	 }
             }
             this.modesList = new JList<String>(pngFiles);
             this.modesList.setLocation(new Point(10, 10));
             this.modesList.setSize(new Dimension(100, 120));
             this.add(this.modesList);
             this.modesList.setSelectionMode(0);
             this.gbl = new GridBagLayout();
             this.gbc = new GridBagConstraints();
             
             Properties prp = FileLoader.getConfigFile();
             try
             {
            	 String preSM = prp.getProperty("MODE", "1vs1");	// previous session mode
                 this.modePreview = new ImageIcon(FileLoader.loadImageFromFolder("mode", preSM + ".png"));
                 this.modesList.setSelectedValue(preSM, false);
                 this.modesList.addListSelectionListener(this);
             }
             catch (java.lang.NullPointerException exc)
             {
            	 log.log( Level.WARNING, "Cannot find preview image: " + exc);
                 this.modePreview = new ImageIcon(JChessApp.class.getResource("theme/noPreview.png"));
                 return;
             }
             this.modePreviewLabel = new JLabel(this.modePreview);
             this.modePreviewLabel.setLocation(new Point(110, 10));
             this.modePreviewLabel.setSize(new Dimension(300, 200));
             this.add(this.modePreviewLabel);
             this.okButton = new JButton("OK");
             this.okButton.setLocation(new Point(175, 220));
             this.okButton.setSize(new Dimension(200, 50));
             this.add(this.okButton);
             this.okButton.addActionListener(this);
             this.setModal(true);
         }
         else
         {
             throw new Exception(Settings.lang("error_when_creating_theme_config_window"));
         }
         log.log(Level.INFO, "Creating newGamgeChooseMModeWindow was successfull.");
     }

     /**
	  * Method which sets the image of mode after change of the selected list element
	  * @param e Event of list element which is clicked
	  */
     @Override
     public void valueChanged(ListSelectionEvent e)
     {
         String element = this.modesList.getModel().getElementAt(this.modesList.getSelectedIndex()).toString();
         this.modePreview = new ImageIcon(FileLoader.loadImageFromFolder("mode", element + ".png"));
         this.modePreviewLabel.setIcon(this.modePreview);
     }

     /** Method which is changing a pawn into queen, rook, bishop or knight
      * @param e keep information about performed action
      */
     public void actionPerformed(ActionEvent e)
     {
         if (e.getSource() == this.okButton)
         {
             Properties prp = FileLoader.getConfigFile();
             int element = this.modesList.getSelectedIndex();
             log.log( Level.INFO, Integer.toString(element));
             String name = this.modesList.getModel().getElementAt(element).toString();

             prp.setProperty("MODE", name);
             try
             {
                 //FileOutputStream fOutStr = new FileOutputStream(ThemeChooseWindow.class.getResource("config.txt").getFile());
                 FileOutputStream fOutStr = new FileOutputStream("config.txt");
                 prp.store(fOutStr, null);
                 fOutStr.flush();
                 fOutStr.close();
             }
             catch (java.io.IOException exc)
             {
             }
             
             this.setVisible(false);
             if (this.modesList.getSelectedValue().equals("1vs1")) {
	    		 try {
	    			 NewGameWindow newGame = new NewGameWindow(2);
	                 JChessApp.getApplication().show(newGame);
	    		 }
	    		 catch(Exception exc)
	             {
	                 JOptionPane.showMessageDialog(JChessApp.getApplication().getMainFrame(), exc.getMessage());
	                 log.log( Level.SEVERE, "Something wrong creating newGameWindow");
	             }
	    	 }
             else if (this.modesList.getSelectedValue().equals("3person")) {
            	 try {
	    			 NewGameWindow newGame = new NewGameWindow(3);
	                 JChessApp.getApplication().show(newGame);
	    		 }
	    		 catch(Exception exc)
	             {
	                 JOptionPane.showMessageDialog(JChessApp.getApplication().getMainFrame(), exc.getMessage());
	                 log.log( Level.SEVERE, "Something wrong creating newGameWindow");
	             }
             }
             else {
            	 JOptionPane.showMessageDialog(this, Settings.lang("Not implemented yet!"));
             }
             
             log.log( Level.INFO, prp.getProperty("THEME"));
         }
     }
}
