/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sławomir Lach ( matlak, msl )
 */
package jchess.dialog;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import jchess.Game;
import jchess.JChessApp;
import jchess.Player;
import jchess.Settings;

/**
 * Class responsible for drawing the fold with local game settings
 */
public class DrawLocalSettings extends JPanel implements ActionListener, DocumentListener
{
	private static final long serialVersionUID = -1069877342691802551L;
	
	private static final Logger log = Logger.getLogger( DrawLocalSettings.class.getName() );
    JDialog parent;//needed to close newGame window
    JRadioButton oponentComp;//choose opponent
    JRadioButton oponentHuman;//choose opponent (human)
    ButtonGroup oponentChoos;//group 4 radio buttons
    JFrame localPanel;
    JLabel compLevLab;
    JSlider computerLevel;//slider to choose jChess Engine level
    java.util.List<JTextField> playerNames;
    java.util.List<JLabel> playerNameLabs;
    java.util.List<JComboBox<String>> playerColors; //to choose color of player
    JLabel firstNameLab;
    JLabel secondNameLab;
    GridBagLayout gbl;
    GridBagConstraints gbc;
    Container cont;
    JSeparator sep;
    JButton okButton;
    JCheckBox timeGame;
    JComboBox<?> time4Game;
    String times[] =
    {
        "1", "3", "5", "8", "10", "15", "20", "25", "30", "60", "120"
    };
    String gameMode;
    
    /** Constructor which applies the settings defined by the player count.
     * @param parent overlying dialog entity
     * @param playerCount number of players 
     */
    public DrawLocalSettings(JDialog parent, int playerCount)
    {
        super();
        //this.setA//choose opponent
        this.parent = parent;
        this.gbl = new GridBagLayout();
        this.gbc = new GridBagConstraints();
        this.sep = new JSeparator();
        this.okButton = new JButton(Settings.lang("ok"));
        this.compLevLab = new JLabel(Settings.lang("computer_level"));

        if (playerCount == 2)
        	gameMode = "standard";
        else
        	gameMode = "circle";
        
        String[] colors = new String[playerCount];
        for (int i = 0; i < playerCount; i++)
        {
        	colors[i] = Settings.lang("color_" + (i+1));
        }
        this.playerNames = new java.util.ArrayList<JTextField>();
        this.playerNameLabs = new java.util.ArrayList<JLabel>();
        this.playerColors = new java.util.ArrayList<JComboBox<String>>();
        for (int i = 0; i < playerCount; i++)
        {
        	JTextField player = new JTextField("", 10);
        	this.playerNames.add(player);
            this.playerNames.get(i).setSize(new Dimension(200, 50));
            this.playerNames.get(i).setText("Player" + i);
            
            this.playerNameLabs.add(new JLabel(Settings.lang("player_name_" + (i+1)) + ": "));
            this.playerColors.add(new JComboBox<String>(colors));
        }
        this.oponentChoos = new ButtonGroup();
        this.computerLevel = new JSlider();
        this.timeGame = new JCheckBox(Settings.lang("time_game_min"));
        this.time4Game = new JComboBox<String>(times);

        this.oponentComp = new JRadioButton(Settings.lang("against_computer"), false);
        this.oponentHuman = new JRadioButton(Settings.lang("against_other_human"), true);

        this.setLayout(gbl);
        this.oponentComp.addActionListener(this);
        this.oponentHuman.addActionListener(this);
        this.okButton.addActionListener(this);

        for (int i = 0; i < playerCount; i++)
        {
        	this.playerNames.get(i).getDocument().addDocumentListener(this);
        }

        this.oponentChoos.add(oponentComp);
        this.oponentChoos.add(oponentHuman);
        this.computerLevel.setEnabled(false);
        this.computerLevel.setMaximum(3);
        this.computerLevel.setMinimum(1);

        this.gbc.gridx = 0;
        this.gbc.gridy = 0;
        this.gbc.insets = new Insets(3, 3, 3, 3);
        this.gbl.setConstraints(oponentComp, gbc);
        this.add(oponentComp);
        this.gbc.gridx = 1;
        this.gbl.setConstraints(oponentHuman, gbc);
        this.add(oponentHuman);
        this.gbc.gridx = 0;
        this.gbc.gridy++;
        for (int i = 0; i < playerCount - 1; i++)
        {
        	this.gbl.setConstraints(this.playerNameLabs.get(i), gbc);
        	this.add(this.playerNameLabs.get(i));
            this.gbc.gridx = 0;
            this.gbc.gridy++;
            this.gbl.setConstraints(this.playerNames.get(i), gbc);
            this.add(this.playerNames.get(i));
            this.gbc.gridx = 1;
            this.gbl.setConstraints(this.playerColors.get(i), gbc);
            this.add(this.playerColors.get(i));
            this.gbc.gridx = 0;
            this.gbc.gridy++;
        }
        this.gbl.setConstraints(this.playerNameLabs.get(playerCount - 1), gbc);
        this.add(this.playerNameLabs.get(playerCount - 1));
        this.gbc.gridy++;
        this.gbl.setConstraints(this.playerNames.get(playerCount - 1), gbc);
        this.add(this.playerNames.get(playerCount - 1));
        this.gbc.gridy++;
        this.gbc.insets = new Insets(0, 0, 0, 0);
        this.gbl.setConstraints(compLevLab, gbc);
        this.add(compLevLab);
        this.gbc.gridy++;
        this.gbl.setConstraints(computerLevel, gbc);
        this.add(computerLevel);
        this.gbc.gridy++;
        this.gbc.gridwidth = 1;
        this.gbl.setConstraints(timeGame, gbc);
        this.add(timeGame);
        this.gbc.gridx = 1;
        this.gbc.gridwidth = 1;
        this.gbl.setConstraints(time4Game, gbc);
        this.add(time4Game);
        this.gbc.gridx = 1;
        this.gbc.gridy++;
        this.gbc.gridwidth = 0;
        this.gbl.setConstraints(okButton, gbc);
        this.add(okButton);
        this.oponentComp.setEnabled(false);//for now, because not implemented!

    }
    
    /** Method which occurs after a change in Label
     * @param e Event of Label where occurred change
     */
    @Override
	public void changedUpdate(DocumentEvent e) {
		log.log( Level.INFO, "ChangedUpdate");
		this.correctTextLength(e);
	}

    /** Method which occurs after a insert in Label
     * @param e Event of Label where occurred insert change
     */
	@Override
	public void insertUpdate(DocumentEvent e) {
		log.log( Level.INFO, "insertUpdate");
		this.correctTextLength(e);
	}

	/** Method which occurs after a remove in Label
     * @param e Event of Label where occurred remove change
     */
	@Override
	public void removeUpdate(DocumentEvent e) {
		log.log( Level.INFO, "removeUpdate");
	}
	
    /** Method which corrects text length of name fields
     * @param e Event of Label where occurred text changes
     */
    public void correctTextLength(DocumentEvent e)
    {
        Document target = e.getDocument();
        if (target == this.playerNames.get(0).getDocument() || target == this.playerNames.get(1).getDocument())
        {
            JTextField temp = new JTextField();
            if (target == this.playerNames.get(0).getDocument())
            {
                temp = this.playerNames.get(0);
            }
            else if (target == this.playerNames.get(1).getDocument())
            {
                temp = this.playerNames.get(1);
            }
            int len = temp.getText().length();
            if (len > 8)
            {
            	log.log( Level.INFO, "reset");
                try
                {
                    temp.setText(temp.getText(0, 7));
                }
                catch (BadLocationException exc)
                {
                	log.log( Level.SEVERE, "Something wrong in editables: \n" + exc);
//                    System.out.println("Something wrong in editables: \n" + exc);
                }
            }
        }
    }

    /** Method responsible for changing the options which can make a player
     * when he want to start new local game
     * @param e where is saving data of performed action
     */
    public void actionPerformed(ActionEvent e)
    {
        Object target = e.getSource(); 
        if (target == this.oponentComp) //toggle enabled of controls depends of opponent (if computer)
        {
            this.computerLevel.setEnabled(true);//enable level of computer abilities
            for (int i = 1; i < this.playerNames.size(); i++)
            {
            	this.playerNames.get(i).setEnabled(false);//disable field with name of player2
            }
        }
        else if (target == this.oponentHuman) //else if opponent will be HUMAN
        {
            this.computerLevel.setEnabled(false);//disable level of computer abilities
            for (int i = 1; i < this.playerNames.size(); i++)
            {
            	this.playerNames.get(1).setEnabled(true);//enable field with name of player2
            }
        }
        else if (target == this.okButton) //if clicked OK button (on finish)
        {
        	for (int i = 0; i < this.playerNames.size(); i++)
            {
        		int textLength = this.playerNames.get(i).getText().length();
        		
        		//make playerNames short to 10 digits
        		if (textLength > 9)
                {
                	this.playerNames.get(i).setText(this.trimString(this.playerNames.get(i), 9));
                }
        		
        		// check if playerName is filled in game against other players
        		if (textLength == 0)
                {
        			if (!this.oponentComp.isSelected())
        			{
	        			JOptionPane.showMessageDialog(this, Settings.lang("fill_playerNames"));
	                    return;
        			}
                }
            }
        	
        	// check if playerName is filled in game against Computer
            if ((this.oponentComp.isSelected() && this.playerNames.get(0).getText().length() == 0))
            {
                JOptionPane.showMessageDialog(this, Settings.lang("fill_name"));
                return;
            }
            
            String title = playerNames.get(0).getText();
            for (int i = 1; i < playerNames.size(); i++)
            {
            	title += " vs " + playerNames.get(i).getText();
            }
            Game newGUI = JChessApp.jcv.addNewTab(title, this.gameMode);
            log.log(Level.INFO, this.gameMode);
            Settings sett = newGUI.settings;//sett local settings variable
            
            Player[] player = new Player[newGUI.playerCount]; //set local player variables
            for (int i = 0; i < player.length; i++)
            {
            	player[i] = sett.player[i];
            	player[i].setName(this.playerNames.get(i).getText());//set name of player
            	player[i].setType(Player.playerTypes.localUser);//set type of player
            }
            sett.gameStat = Settings.gameStatus.newGame;
            sett.gameType = Settings.gameTypes.local;
            
            for (int i = 1; i < player.length; i++)
            {
	            if (this.oponentComp.isSelected()) //if computer opponent is checked
	            {
	            	player[1].setType(Player.playerTypes.computer);
	            }
            }
            
            if (this.timeGame.isSelected()) //if timeGame is checked
            {
                String value = this.times[this.time4Game.getSelectedIndex()];//set time for game
                Integer val = new Integer(value);
                sett.timeLimitSet = true;
                sett.timeForGame = (int) val * 60;//set time for game and mult it to seconds
                newGUI.gameClock.setTimes(sett.timeForGame);
                newGUI.gameClock.start();
            }
            log.log( Level.INFO, this.time4Game.getActionCommand());
            //this.time4Game.getComponent(this.time4Game.getSelectedIndex());
            log.log( Level.INFO, "****************\nStarting new game: " + player[0].name + " vs. " + player[1].name
                    + "\ntime 4 game: " + sett.timeForGame + "\ntime limit set: " + sett.timeLimitSet
                    + "\nwhite on top?: " + sett.upsideDown + "\n****************");

            //ToDo: Edit Game for Circle Game
            newGUI.newGame();//start new Game
            this.parent.setVisible(false);//hide parent
            newGUI.chessboard.repaint();
            //newGUI.chessboard.draw();
        }

    }

    /**
     * Method responsible for trimming white symbols from strings
     * @param txt Where is capt value to equal
     * @param length How long is the string
     * @return result trimmed String
     */
    public String trimString(JTextField txt, int length)
    {
        String result = new String();
        try
        {
            result = txt.getText(0, length);
        }
        catch (BadLocationException exc)
        {
        	log.log( Level.SEVERE, "Something wrong in editables: \n" + exc);
        }
        return result;
    }
}