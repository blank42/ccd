package jchess;

import java.awt.event.ComponentListener;
import java.awt.event.MouseListener;
import javax.swing.*;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameStandard extends Game implements MouseListener, ComponentListener {

	private static final long serialVersionUID = 1049592013985247634L;

	private static final Logger log = Logger.getLogger( GameStandard.class.getName() );
	public GameStandard()
    {
		this.playerCount = 2;
		
        this.setLayout(null);
        this.moves = new Moves(this);
        settings = new Settings(2);
        
        chessboard = new Chessboard(this, this.settings, this.moves, playerCount, 2);
        
        
        chessboard.addMouseListener(this);
        this.add(chessboard);
        gameClock = new GameClock(this);
        gameClock.setSize(new Dimension(200, 100));
        gameClock.setLocation(new Point(500, 0));
        this.add(gameClock);

        JScrollPane movesHistory = this.moves.getScrollPane();
        movesHistory.setSize(new Dimension(180, 350));
        movesHistory.setLocation(new Point(500, 121));
        this.add(movesHistory);

        this.chat = new Chat();
        this.chat.setSize(new Dimension(380, 100));
        this.chat.setLocation(new Point(0, 500));
        this.chat.setMinimumSize(new Dimension(400, 100));

        this.blockedChessboard = false;
        this.setLayout(null);
        this.addComponentListener(this);
        this.setDoubleBuffered(true);
    }
	
    /**
     * Method to Start new game
     */
    public void newGame()
    {
        chessboard.setPieces4NewGame("", settings.player);
        
        activePlayer = settings.player[0]; //set active player white
        if (activePlayer.playerType != Player.playerTypes.localUser)
        {
            this.blockedChessboard = true;
        }

        GameStandard activeGame = (GameStandard)JChessApp.jcv.getActiveTabGame();
        if( activeGame != null && JChessApp.jcv.getNumberOfOpenedTabs() == 0 )
        {
            activeGame.chessboard.resizeChessboard(activeGame.chessboard.get_height(false));
            activeGame.chessboard.repaint();
            activeGame.repaint();
        }
        chessboard.repaint();
        this.repaint();
    }
    
    /**
     * Method to save actual state of game
     * @param path address of place where game will be saved
     */
    @Override
    public void saveGame(File path)
    {
        File file = path;
        FileWriter fileW = null;
        try
        {
            fileW = new FileWriter(file);
        }
        catch (java.io.IOException exc)
        {
            System.err.println("error creating fileWriter: " + exc);
            JOptionPane.showMessageDialog(this, Settings.lang("error_writing_to_file")+": " + exc);
            return;
        }
        Calendar cal = Calendar.getInstance();
        String str = new String("");
        String info = new String("[Event \"GameStandard\"]\n[Date \"" + cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1) + "." + cal.get(Calendar.DAY_OF_MONTH) + "\"]\n"
                + "[white \"" + this.settings.player[0].name + "\"]\n[black \"" + this.settings.player[1].name + "\"]\n\n");
        str += info;
        str += this.moves.getMovesInString();
        try
        {
            fileW.write(str);
            fileW.flush();
            fileW.close();
        }
        catch (java.io.IOException exc)
        {
            System.out.println("error writing to file: " + exc);
            JOptionPane.showMessageDialog(this, Settings.lang("error_writing_to_file")+": " + exc);
            return;
        }
        JOptionPane.showMessageDialog(this, Settings.lang("game_saved_properly"));
    }
    
    /**
     * Loading game method(loading game state from the earlier saved file)
     * @param file File where is saved game
     */
    static public void loadGame(File file)
    {
        FileReader fileR = null;
        try
        {
            fileR = new FileReader(file);
        }
        catch (java.io.IOException exc)
        {
        	log.log( Level.SEVERE,"Something wrong reading file: " + exc);
            return;
        }
        BufferedReader br = new BufferedReader(fileR);
        String tempStr = new String();
        String blackName, whiteName;
        try
        {
            tempStr = getLineWithVar(br, new String("[white"));
            whiteName = getValue(tempStr);
            tempStr = getLineWithVar(br, new String("[black"));
            blackName = getValue(tempStr);
            tempStr = getLineWithVar(br, new String("1."));
        }
        catch (ReadGameError err)
        {
        	log.log( Level.SEVERE,"Error reading file: " + err);
            return;
        }
        Game newGUI = (GameStandard) JChessApp.jcv.addNewTab(whiteName + " vs. " + blackName, "standard");
        Settings locSetts = newGUI.settings;
        locSetts.player[0].name = whiteName;
        locSetts.player[1].name = blackName;
        locSetts.player[0].setType(Player.playerTypes.localUser);
        locSetts.player[1].setType(Player.playerTypes.localUser);
        locSetts.gameStat = Settings.gameStatus.loadGame;
        locSetts.gameType = Settings.gameTypes.local;

        newGUI.newGame();
        newGUI.blockedChessboard = true;
        newGUI.moves.setMoves(tempStr);
        newGUI.blockedChessboard = false;
        newGUI.chessboard.repaint();
    }

    /**
     * Method to swich active players after move
     */
    @Override
    public void switchActive(boolean forward)
    {
    	// check if active player is white
        if (activePlayer == settings.player[0])
        {
            activePlayer = settings.player[1]; //set active player black
        }
        else
        {
            activePlayer = settings.player[0]; //set active player white
        }

        this.gameClock.switch_clocks();
    }
}
