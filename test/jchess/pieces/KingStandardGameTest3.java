package jchess.pieces;


import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

/**
 * Test class for the piece King on the standard chess board.
 */
public class KingStandardGameTest3 {
	
	Game game;
	King king;
	
	/**
	 * Sets up the game and chessboard to enable testing of the King.
	 * For that the king of each player has to be defined.  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		Rook rookW1 = new Rook(game.chessboard, game.chessboard.getSettings().player[0]);
		Rook rookW2 = new Rook(game.chessboard, game.chessboard.getSettings().player[0]);
		
		king = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(king);
		game.chessboard.setKingBlack(kingBlack);
		
		game.chessboard.getSquares()[3][7].setPiece(king);
		king.setSquare(game.chessboard.getSquares()[3][7]);
		
		game.chessboard.getSquares()[5][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[5][0]);
		
		game.chessboard.getSquares()[0][7].setPiece(rookW1);
		rookW1.setSquare(game.chessboard.getSquares()[0][7]);
		
		game.chessboard.getSquares()[7][7].setPiece(rookW2);
		rookW2.setSquare(game.chessboard.getSquares()[7][7]);
		
		rookW1.wasMotion = false;
		rookW2.wasMotion = false;
		king.wasMotion = false;
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
    /**
     * Test that checks if moves list of the King contains all adjacent moves starting
     * at the position (11,0).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testCastlingMoves() throws Exception {
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList.add(game.chessboard.getSquares()[2][7]);
		expectedList.add(game.chessboard.getSquares()[2][6]);
		expectedList.add(game.chessboard.getSquares()[3][6]);
		expectedList.add(game.chessboard.getSquares()[4][6]);
		expectedList.add(game.chessboard.getSquares()[4][7]);
		expectedList.add(game.chessboard.getSquares()[1][7]);
		expectedList.add(game.chessboard.getSquares()[5][7]);
		
		ArrayList<Square> resultList = king.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
