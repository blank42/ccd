package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;
import jchess.Player;
import jchess.Square;

/**
 * Test class for the piece Pawn on the circular chess board.
 */
@RunWith(Parameterized.class)
public class PawnCircleGameTest {
	
	Game game;
	Pawn pawn;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Pawn position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Pawn position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{20,2},		// middle
			{19,5},		// inner edge and blocked
			{23,0},		// next to border
			{21,5}		// can capture
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Pawn.
	 * For that the king of each player has to be defined.
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		helper = new HelperClass();
		
		King kingWhite = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		King kingGray  = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		Pawn pawnWhite = new Pawn(game.chessboard, game.chessboard.getSettings().player[0]);
		Rook rookWhite = new Rook(game.chessboard, game.chessboard.getSettings().player[0]);
		
		pawn = new Pawn(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(kingWhite);
		game.chessboard.setKingBlack(kingBlack);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[3][0].setPiece(kingWhite);
		kingWhite.setSquare(game.chessboard.getSquares()[3][0]);
		
		game.chessboard.getSquares()[19][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[19][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
		game.chessboard.getSquares()[7][5].setPiece(pawnWhite);
		pawnWhite.setSquare(game.chessboard.getSquares()[7][5]);
		
		game.chessboard.getSquares()[0][1].setPiece(rookWhite);
		rookWhite.setSquare(game.chessboard.getSquares()[0][1]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the King contains all adjacent moves starting
     * at the position (x,y).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(pawn);
		pawn.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();

		int otherSideX = Math.floorMod(x+12, 24);
		int otherSideY = 5;
		
		boolean obstacleAhead   = helper.hasObstacleInDir(game, x, -0, y, +1);
		boolean obstacleLeftup  = helper.hasObstacleInDir(game, x, -1, y, +1);
		boolean obstacleRightup = helper.hasObstacleInDir(game, x, +1, y, +1);
		boolean obstacleBorderLeft  = helper.hasBorderAsObstacle(game, x-1, x, y+1, y);
		boolean obstacleBorderRight = helper.hasBorderAsObstacle(game, x+1, x, y+1, y);
		
		Player ownPlayer = game.chessboard.getSettings().player[1];
		
		// check for obstacles
		if(y+1 == 6)
		{
			obstacleAhead   = helper.hasObstacleInDir(game, otherSideX, -0, otherSideY, +0);
			obstacleLeftup  = helper.hasObstacleInDir(game, otherSideX, -2, otherSideY, +0);
			obstacleRightup = helper.hasObstacleInDir(game, otherSideX, +2, otherSideY, +0);
		}
		
		// add moves
		if(y+1 == 6)
		{
			if(!obstacleAhead          ) expectedList.add(game.chessboard.getSquares()[Math.floorMod(otherSideX-0, 24)][otherSideY]);
			if(obstacleLeftup && !obstacleBorderLeft ) if(!game.chessboard.getSquares()[Math.floorMod(otherSideX-2, 24)][otherSideY].getPiece().getPlayer().equals(ownPlayer)) expectedList.add(game.chessboard.getSquares()[Math.floorMod(otherSideX-2, 24)][otherSideY]);
			if(obstacleRightup&& !obstacleBorderRight) if(!game.chessboard.getSquares()[Math.floorMod(otherSideX+2, 24)][otherSideY].getPiece().getPlayer().equals(ownPlayer)) expectedList.add(game.chessboard.getSquares()[Math.floorMod(otherSideX+2, 24)][otherSideY]);
		}
		else
		{
			if(!obstacleAhead          ) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x-0, 24)][y+1]);
			if(!obstacleAhead && y == 1) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x-0, 24)][y+2]);
			if(obstacleLeftup && !obstacleBorderLeft ) if(!game.chessboard.getSquares()[Math.floorMod(x-1, 24)][y+1].getPiece().getPlayer().equals(ownPlayer)) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x-1, 24)][y+1]);
			if(obstacleRightup&& !obstacleBorderRight) if(!game.chessboard.getSquares()[Math.floorMod(x+1, 24)][y+1].getPiece().getPlayer().equals(ownPlayer)) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x+1, 24)][y+1]);
		}
		
		ArrayList<Square> resultList = pawn.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
