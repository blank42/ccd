package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;
import jchess.Square;

/**
 * Test class for the piece Ninja on the circular chess board.
 */
@RunWith(Parameterized.class)
public class NinjaCircleGameTest {
	
	Game game;
	Ninja ninja;	// J
	int friendsCount = 6;
	ArrayList<Pawn> friends;
	int enemiesCount = 3;
	ArrayList<Pawn> enemies;
	HelperClass helper;
	
	Random r;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Ninja position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Ninja position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
//			{2, 2},		// middle
//			{2, 0},		// outer edge
			{5, 5},		// inner edge
			{23,0},		// left from wall
			{0, 1}		// right from wall
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Ninja.
	 * For that the king of each player has to be defined.
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		helper = new HelperClass();
		
		King kingWhite = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		King kingGray  = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		
		ninja = new Ninja(game.chessboard, game.chessboard.getSettings().player[0]);
		
		friends = new ArrayList<Pawn>();
		enemies = new ArrayList<Pawn>();
		
		r = new Random();
		for (int i = 0; i < friendsCount; i++)
		{
			System.out.print(i +" ");
			friends.add(new Pawn(game.chessboard, game.chessboard.getSettings().player[0]));
		}
		System.out.println("");
		for (int i = 0; i < enemiesCount; i++)
		{
			System.out.print(i +" ");
			enemies.add(new Pawn(game.chessboard, game.chessboard.getSettings().player[1]));
		}
		System.out.println("");
		
		game.chessboard.setKingWhite(kingWhite);
		game.chessboard.setKingBlack(kingBlack);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[3][0].setPiece(kingWhite);
		kingWhite.setSquare(game.chessboard.getSquares()[3][0]);
		
		game.chessboard.getSquares()[19][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[19][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the Ninja contains all positions of the own player's
     * pieces. (Except the king)
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
		
		// place Ninja
		game.chessboard.getSquares()[x][y].setPiece(ninja);
		ninja.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		// place enemies and squares to expectedList
		for (int i = 0; i < enemies.size(); i++)
		{
			int xOffset = r.nextInt(3) - 1;
			int yOffset = r.nextInt(3) - 1;
			if (	   x + xOffset >= 0 
					&& x + xOffset <  game.chessboard.getSquares().length
					&& y + yOffset >= 0 
					&& y + yOffset <  game.chessboard.getSquares()[0].length
					&& game.chessboard.getSquares()[x + xOffset][y + yOffset].getPiece() == null)
			{
				game.chessboard.getSquares()[x + xOffset][y + yOffset].setPiece(enemies.get(i));
				enemies.get(i).setSquare(game.chessboard.getSquares()[x + xOffset][y + yOffset]);
				expectedList.add(game.chessboard.getSquares()[x + xOffset][ y + yOffset]);
			}
			else
			{
				i--;
			}
		}
		System.out.println("");
		
		// place friends and squares to expectedList
		for (int i = 0; i < friends.size(); i++)
		{
			int xFriend = r.nextInt(game.chessboard.getSquares().length);
			int yFriend = r.nextInt(game.chessboard.getSquares()[0].length);
			
			if (game.chessboard.getSquares()[xFriend][yFriend].getPiece() == null)
			{
				game.chessboard.getSquares()[xFriend][yFriend].setPiece(friends.get(i));
				friends.get(i).setSquare(game.chessboard.getSquares()[xFriend][yFriend]);
				expectedList.add(game.chessboard.getSquares()[xFriend][yFriend]);
			}
			else
			{
				i--;
			}
		}
		
		ArrayList<Square> resultList = ninja.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
