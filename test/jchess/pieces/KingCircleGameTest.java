package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;
import jchess.Square;

/**
 * Test class for the piece King on the circular chess board.
 */
@RunWith(Parameterized.class)
public class KingCircleGameTest {
	
	Game game;
	King king;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the King position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the King position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{2, 2},		// middle
			{2, 0},		// outer edge
			{5, 5},		// inner edge
			{23,0},		// left from wall
			{0, 1}		// right from wall
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the King.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
     * - - - - - - - - 0   23 - - - - - - 
     * � � � � � � 1 � � | � � 22� � � � �		0
     * � � � � 2 � � � � | � � � � 21� � �		1
     * � � � � � � � � � | � � � � � � � �		2
     * � � 3 � � � KW� � | � � � � � � 20�		3
     * � � � � � � � � � | � � � � � � � �		4
     * � 4 � � � � � � � | � � � � � � KB19		5
     * � � � � � � � � �---� � � � � � � �
     * 5 � � � � � � �       � � � � � � �|18
     *   - - - - - -|- --�-- -|- - - - - - 
     * 6 | � � � � � �       � � � � � � �|17
     * � � � � � � � � �---� � � � � � � �
     * � 7 � � � � � � � | � � � � � � � 16
     * � � � � � � � � � | � � � � � � � �
     * � � 8 � � � � � � | � � � � � � 15�
     * � � � � � � � � � | � � � � � � � �
     * � � � � 9 � � � � | � � � � 14� � �
     * � � � � � � 10�KG | � � 13� � � � �
     * - - - - - - - -11   12 - - - - - -
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		helper = new HelperClass();
		
		king = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		King kingGray  = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		
		game.chessboard.setKingWhite(king);
		game.chessboard.setKingBlack(kingBlack);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[19][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[19][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the King contains all adjacent moves starting
     * at the position (x,y).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(king);
		king.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();

		int otherSideX = Math.floorMod(x+12, 24);
		int otherSideY = 5;
		
		boolean obstacleOn1 = helper.hasObstacleInDir(game, x, -1, y, -1);
		boolean obstacleOn2 = helper.hasObstacleInDir(game, x, -1, y, +0);
		boolean obstacleOn3 = helper.hasObstacleInDir(game, x, -1, y, +1);
		boolean obstacleOn4 = helper.hasObstacleInDir(game, x, +0, y, -1);
		boolean obstacleOn5 = helper.hasObstacleInDir(game, x, +0, y, +1);
		boolean obstacleOn6 = helper.hasObstacleInDir(game, x, +0, y, -1);
		boolean obstacleOn7 = helper.hasObstacleInDir(game, x, +1, y, -0);
		boolean obstacleOn8 = helper.hasObstacleInDir(game, x, +1, y, +1);
		
		// check for obstacles
		if(y+1 == 6)
		{
			obstacleOn3 = helper.hasObstacleInDir(game, otherSideX, +2, otherSideY, +0);
			obstacleOn5 = helper.hasObstacleInDir(game, otherSideX, +0, otherSideY, +0);
			obstacleOn8 = helper.hasObstacleInDir(game, otherSideX, -2, otherSideY, +0);
		}
		
		// add moves
		if(y+1 == 6)
		{
			if(!obstacleOn3) expectedList.add(game.chessboard.getSquares()[Math.floorMod(otherSideX-2, 24)][otherSideY]);	// leftup
			if(!obstacleOn5) expectedList.add(game.chessboard.getSquares()[Math.floorMod(otherSideX+0, 24)][otherSideY]);	// up
			if(!obstacleOn8) expectedList.add(game.chessboard.getSquares()[Math.floorMod(otherSideX+2, 24)][otherSideY]);	// rightup
		}
		else
		{
			if(!obstacleOn3) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x-1, 24)][y+1]);					// leftup
			if(!obstacleOn5) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x+0, 24)][y+1]);					// up
			if(!obstacleOn8) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x+1, 24)][y+1]);					// rightup
		}
		
		if(y-1 >= 0 && !obstacleOn1) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x-1, 24)][y-1]);			// leftdown
		if(			   !obstacleOn2) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x-1, 24)][y+0]);			// left
		if(y-1 >= 0 && !obstacleOn4) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x+0, 24)][y-1]);			// down
		if(y-1 >= 0 && !obstacleOn6) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x+1, 24)][y-1]);			// rightdown
		if(			   !obstacleOn7) expectedList.add(game.chessboard.getSquares()[Math.floorMod(x+1, 24)][y-0]);			// right
		
		
		ArrayList<Square> resultList = king.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
