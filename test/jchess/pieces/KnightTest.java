package jchess.pieces;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

/**
 * Test class for the piece Knight on the standard chessboard.
 */
public class KnightTest {
	
	Game game;
	Knight knight;
	
	/**
	 * Sets up the game and chessboard to enable testing of the Knight.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KB|__|__|__|__|7
	 * |__|__|__|__|__|__|__|__|6
	 * |__|__|__|3_|__|__|__|__|5
	 * |__|__|__|__|__|__|__|__|4
	 * |__|__|NB|__|__|__|__|__|3
	 * |__|__|__|__|__|__|__|__|2
	 * |__|__|__|6_|__|__|__|__|1
	 * |__|__|__|RW|KW|__|__|__|0
	 *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		knight = new Knight(game.chessboard, game.chessboard.getSettings().player[1]);
		Rook rookW = new Rook(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[4][0].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[3][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[3][7]);
		
		game.chessboard.getSquares()[2][3].setPiece(knight);
		knight.setSquare(game.chessboard.getSquares()[2][3]);
		
		game.chessboard.getSquares()[3][0].setPiece(rookW);
		rookW.setSquare(game.chessboard.getSquares()[3][0]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
	/**
     * Test that checks if moves list of the Knight contains all possible squares to move
     * on from a specific position. (2,3)
     * No move should be in the list if the own King will not be safe anymore after moving.
	 * @throws Exception if the expected moves do not agree with the output moves list
	 */
	@Test
	public void testAllMovesKingNotSafe() throws Exception {
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList.add(game.chessboard.getSquares()[3][5]);	// 3
		expectedList.add(game.chessboard.getSquares()[3][1]);	// 6
		
		ArrayList<Square> resultList = knight.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
