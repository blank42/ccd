package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

/**
 * Test class for the piece King on the standard chessboard.
 */
@RunWith(Parameterized.class)
public class KingStandardGameTest {

	Game game;
	King king;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the King position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the King position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{0, 0}, {0, 7}, {7, 0}, {7, 7},	// corners
			{3, 0}, {0, 3}, {7, 2},			// edges
			{4, 4}							// middle
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the King.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KB|__|__|__|__|7
	 * |__|__|__|__|__|__|__|__|6
	 * |__|__|__|__|__|__|__|__|5
	 * |__|__|__|3_|5_|8_|__|__|4
	 * |__|__|__|2_|KW|7_|__|__|3
	 * |__|__|__|1_|4_|6_|__|__|2
	 * |__|__|__|__|__|__|__|__|1
	 * |__|__|__|__|__|__|__|__|0
	 *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		helper = new HelperClass();
		
		king = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(king);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[3][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[3][7]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
	/**
	 * Test that checks if moves list of the King contains all adjacent squares to move
     * on from a specific position. (x,y)
	 * @throws Exception if the expected moves do not agree with the output moves list
	 */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(king);
		king.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		boolean obstacleOn1 = helper.hasObstacleInDir(game, x, -1, y, -1);
		boolean obstacleOn2 = helper.hasObstacleInDir(game, x, -1, y, +0);
		boolean obstacleOn3 = helper.hasObstacleInDir(game, x, -1, y, +1);
		boolean obstacleOn4 = helper.hasObstacleInDir(game, x, +0, y, -1);
		boolean obstacleOn5 = helper.hasObstacleInDir(game, x, +0, y, +1);
		boolean obstacleOn6 = helper.hasObstacleInDir(game, x, +1, y, -1);
		boolean obstacleOn7 = helper.hasObstacleInDir(game, x, +1, y, -0);
		boolean obstacleOn8 = helper.hasObstacleInDir(game, x, +1, y, +1);
		
		if(x-1 >= 0 && y-1 >= 0 && !obstacleOn1) expectedList.add(game.chessboard.getSquares()[x-1][y-1]);	// 1
		if(x-1 >= 0 && y+0 <= 7 && !obstacleOn2) expectedList.add(game.chessboard.getSquares()[x-1][y+0]);	// 2
		if(x-1 >= 0 && y+1 <= 7 && !obstacleOn3) expectedList.add(game.chessboard.getSquares()[x-1][y+1]);	// 3
		if(x+0 <= 7 && y-1 >= 0 && !obstacleOn4) expectedList.add(game.chessboard.getSquares()[x+0][y-1]);	// 4
		if(x+0 <= 7 && y+1 <= 7 && !obstacleOn5) expectedList.add(game.chessboard.getSquares()[x+0][y+1]);	// 5
		if(x+1 <= 7 && y-1 >= 0 && !obstacleOn6) expectedList.add(game.chessboard.getSquares()[x+1][y-1]);	// 6
		if(x+1 <= 7 && y-0 >= 0 && !obstacleOn7) expectedList.add(game.chessboard.getSquares()[x+1][y-0]);	// 7
		if(x+1 <= 7 && y+1 <= 7 && !obstacleOn8) expectedList.add(game.chessboard.getSquares()[x+1][y+1]);	// 8
		
		ArrayList<Square> resultList = king.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
