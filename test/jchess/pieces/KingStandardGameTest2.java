package jchess.pieces;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;

/**
 * Test class for the piece King on the standard chessboard.
 */
public class KingStandardGameTest2 {
	
	Game game;
	King king;
	
	/**
	 * Sets up the game and chessboard to enable testing of the King.
	 * For that the king of each player has to be defined.  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		Queen queenB = new Queen(game.chessboard, game.chessboard.getSettings().player[1]);
		king = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(king);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[5][5].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[5][5]);
		
		game.chessboard.getSquares()[7][4].setPiece(queenB);
		queenB.setSquare(game.chessboard.getSquares()[7][4]);
		
		game.chessboard.getSquares()[6][7].setPiece(king);
		king.setSquare(game.chessboard.getSquares()[6][7]);
		
		game.setActivePlayer(game.chessboard.getSettings().player[0]);

	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
    /**
     * Test that checks if the King is in the state where he is not checkmated or stalemated.
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testIsOk() throws Exception {
		
		Assert.assertEquals(King.states.ok, king.isCheckmatedOrStalemated());
	}

}
