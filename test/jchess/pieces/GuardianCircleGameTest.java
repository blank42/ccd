package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;
import jchess.Square;

/**
 * Test class for the piece Guardian on the circular chess board.
 */
@RunWith(Parameterized.class)
public class GuardianCircleGameTest {
	
	Game game;
	King kingWhite;
	Guardian guardian;	// G
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Guardian position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Guardian position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
//			{2, 2},		// middle
//			{7, 0},		// between king and outer edge
//			{6, 2},		// diagonal beside king
//			{8, 1},		// beside king, but behind wall
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Guardian.
	 * For that the king of each player has to be defined.
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		helper = new HelperClass();
		
		kingWhite = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		King kingGray  = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		
		guardian = new Guardian(game.chessboard, game.chessboard.getSettings().player[0]);
		Bishop bishop = new Bishop(game.chessboard, game.chessboard.getSettings().player[1]);
		Pawn pawn = new Pawn(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(kingWhite);
		game.chessboard.setKingBlack(kingBlack);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[7][1].setPiece(kingWhite);
		kingWhite.setSquare(game.chessboard.getSquares()[7][1]);
		
		game.chessboard.getSquares()[19][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[19][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
		game.chessboard.getSquares()[9][3].setPiece(bishop);
		bishop.setSquare(game.chessboard.getSquares()[9][3]);
		
		game.chessboard.getSquares()[7][2].setPiece(pawn);
		pawn.setSquare(game.chessboard.getSquares()[7][2]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the Guardian contains all moves starting at the
     * position (x,y).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
			
		game.chessboard.getSquares()[x][y].setPiece(guardian);
		guardian.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		// add positions around Guardian
		for (int i = -1; i <= 1; i++)
		{
			if (	   x + i >= 0 
					&& x + i <  game.chessboard.getSquares().length)
			{
				for (int j = -1; j <= 1; j++)
				{
					if (	y + j >= 0 
							&& y + j <  game.chessboard.getSquares()[0].length
							&& game.chessboard.getSquares()[x + i][y + j].getPiece() == null)
					{
						if (y > 1
								|| (x == 0  && x + i > 0)
								|| (x == 8  && x + i > 7)
								|| (x == 16 && x + i > 15)
								|| (x == 7  && x + i < 8)
								|| (x == 15 && x + i < 16)
								|| (x == 23 && x + i < 24))
						{
							expectedList.add(game.chessboard.getSquares()[x + i][y + j]);
						}
					}
				}
			}
		}
		
		int kingX = kingWhite.getSquare().getPozX();
		int kingY = kingWhite.getSquare().getPozY();
		// place square behind king
		if (Math.abs(kingX - x) == 1 || Math.abs(kingY - y) == 1)
		{
			if (y > 1
					|| (x == 0  && kingX > 0)
					|| (x == 8  && kingX > 7)
					|| (x == 16 && kingX > 15)
					|| (x == 7  && kingX < 8)
					|| (x == 15 && kingX < 16)
					|| (x == 23 && kingX < 24))
			{
				if (!game.chessboard.getSquares()[Math.floorMod(kingX + 1, 24)][kingY].getPiece().getPlayer().equals(game.chessboard.getSettings().player[0])
						&& !game.chessboard.getSquares()[Math.floorMod(kingX + 1, 24)][kingY].getPiece().equals(guardian))
					expectedList.add(game.chessboard.getSquares()[Math.floorMod(kingX + 1, 24)][kingY]);
				if (!game.chessboard.getSquares()[Math.floorMod(kingX + 23, 24)][kingY].getPiece().getPlayer().equals(game.chessboard.getSettings().player[0])
						&& !game.chessboard.getSquares()[Math.floorMod(kingX + 23, 24)][kingY].getPiece().equals(guardian)) // -1
					expectedList.add(game.chessboard.getSquares()[Math.floorMod(kingX + 23, 24)][kingY]);
				if (kingY+1 < 6 && !game.chessboard.getSquares()[kingX][kingY+1].getPiece().getPlayer().equals(game.chessboard.getSettings().player[0])
						&& !game.chessboard.getSquares()[kingX][kingY+1].getPiece().equals(guardian))
					expectedList.add(game.chessboard.getSquares()[kingX][kingY+1]);
				if (kingY-1 >= 0 && !game.chessboard.getSquares()[kingX][kingY-1].getPiece().getPlayer().equals(game.chessboard.getSettings().player[0])
						&& !game.chessboard.getSquares()[kingX][kingY-1].getPiece().equals(guardian))
					expectedList.add(game.chessboard.getSquares()[kingX][kingY-1]);
			}
		}
		
		ArrayList<Square> resultList = guardian.allMoves();
		
		helper.deleteDuplicates(expectedList, resultList);
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
