package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

/**
 * Test class for the piece Bishop on the standard chessboard.
 */
@RunWith(Parameterized.class)
public class BishopStandardGameTest {
	
	Game game;
	Bishop bishop;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Bishop position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Bishop position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{0, 0}, {0, 7}, {7, 0}, {7, 7},		// corners
			{3, 0}, {0, 3}, {2, 7}, {7, 2},		// edges
			{4, 4},								// middle
			{7, 3}								// blocked
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Bishop.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KB|__|__|__|X_|7
     * |X_|__|__|__|__|__|X_|__|6
     * |__|X_|__|__|__|X_|__|__|5
     * |__|__|X_|__|X_|__|__|__|4
     * |__|__|__|BW|__|__|__|__|3
     * |__|__|X_|__|X_|__|__|__|2
     * |__|X_|__|__|__|X_|__|__|1
     * |X_|__|__|__|KW|__|X_|__|0
     *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		helper = new HelperClass();

		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		bishop = new Bishop(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[4][0].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[3][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[3][7]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the Bishop contains all diagonal moves
     * starting at the position (x,y).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(bishop);
		bishop.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList = helper.getDiagonalMoves2Person(game, bishop.getSquare());

		ArrayList<Square> resultList = bishop.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}
}
