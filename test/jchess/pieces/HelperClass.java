package jchess.pieces;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import jchess.Game;
import jchess.GameCircle;
import jchess.GameStandard;
import jchess.Square;

/**
 * Helper class that includes all methods needed for different tests of pieces.
 * This should held to avoid redundancy.
 */
class HelperClass {

	HelperClass() { }
	
	/**
	 * Method that tells if the square on the position (X,Y) of the chessboard is occupied.
	 * @param game the game needed to determine which type of chessboard is considered
	 * @param X the x-position of the considered square
	 * @param Y the y-position of the considered square
	 * @return true if square (X,Y) of the chessboard is occupied
	 */
	boolean hasObstacleInDir(Game game, int x, int xAddend, int y, int yAddend)
	{
		int X = x + xAddend;
		int Y = y + yAddend;
		
		if (game.getClass().equals(GameStandard.class))
		{
			if ((X >= 0 && X <=  7) && (Y >= 0 && Y <= 7))
				return (game.chessboard.getSquares()[X][Y].getPiece() != null);
		}
		
		if (game.getClass().equals(GameCircle.class))
		{	
			if (this.hasBorderAsObstacle(game, X, x, Y, y))
				return true;
			
			if (Math.floorMod(Y, 6) >= 0 && Math.floorMod(Y, 6) <= 5)
				return (game.chessboard.getSquares()[Math.floorMod(X, 24)][Math.floorMod(Y, 6)].getPiece() != null);
		}
		
		return false;
	}
	
	/**
	 * Method that checks if a piece would go through a border. Moving through a border means
	 * either going from the left side to the right side of the border or the other way around.
	 * For this it has to be checked if the last move resulted on one side whereas the current
	 * move will result on the other side.
	 * @param game the game needed to determine which type of chessboard is considered
	 * @param destinationX the x-Value of the destination square
	 * @param startX the x-Value of the start square
	 * @param destinationY the y-Value of the destination square
	 * @param startY the y-Value of the start square
	 * @return true if the piece would cross a border
	 */
	boolean hasBorderAsObstacle(Game game, int destinationX, int startX, int destinationY, int startY)
	{
		int xDirection = 0;
		int yDirection = 0;
		
		if (startX - destinationX > 0) xDirection = -1;
		if (startX - destinationX < 0) xDirection = +1;
		if (startY - destinationY > 0) yDirection = -1;
		if (startY - destinationY < 0) yDirection = +1;
		
		Square destinationSquare = game.chessboard.getSquares()[Math.floorMod(destinationX, 24)][Math.floorMod(destinationY, 6)];
		Square lastSquare = game.chessboard.getSquares()[Math.floorMod(destinationX - xDirection, 24)][Math.floorMod(destinationY - yDirection, 6)];
				
		for (int i = 0; i < game.playerCount; i++ )
		{
			int xRightFromBorder = 0 + (8*i);
			int xLeftFromBorder  = Math.floorMod(xRightFromBorder-1, 24);
			
			ArrayList<Square> leftFromBorder  = new ArrayList<Square>();
			ArrayList<Square> rightFromBorder = new ArrayList<Square>();
			
			leftFromBorder.add (game.chessboard.getSquares()[xLeftFromBorder ][0]);
			leftFromBorder.add (game.chessboard.getSquares()[xLeftFromBorder ][1]);
			rightFromBorder.add(game.chessboard.getSquares()[xRightFromBorder][0]);
			rightFromBorder.add(game.chessboard.getSquares()[xRightFromBorder][1]);
			
			if ((leftFromBorder.contains(destinationSquare) && rightFromBorder.contains(lastSquare))
			    || (leftFromBorder.contains(lastSquare) && rightFromBorder.contains(destinationSquare)))
				return true;
		}
		
		return false;
	}
	
	/**
	 * Method that gets all horizontal and vertical moves on the standard chessboard starting on a certain
	 * position on the chessboard. The moves are stored in an ArrayList containing squares.
	 * @param game the already set up game with its standard chessboard
	 * @param startSquare the square which possible moves are wanted
	 * @return the ArrayList containing the reachable squares
	 */
	ArrayList<Square> getHorizontalAndVerticalMoves2Person(Game game, Square startSquare)
	{
		ArrayList<Square> movesList = new ArrayList<Square>();
		
		int x = startSquare.getPozX();
		int y = startSquare.getPozY();
		
		boolean obstacleUp   = false;
		boolean obstacleDown = false;
		boolean obstacleLeft = false;
		boolean obstacleRight= false;
		
		for (int i = 1; i <= 7; i++)
		{
			if (!obstacleUp   ) obstacleUp   = this.hasObstacleInDir(game, x,  0, y, +i);
			if (!obstacleDown ) obstacleDown = this.hasObstacleInDir(game, x,  0, y, -i);
			if (!obstacleLeft ) obstacleLeft = this.hasObstacleInDir(game, x, -i, y,  0);
			if (!obstacleRight) obstacleRight= this.hasObstacleInDir(game, x, +i, y,  0);
			
			if (!obstacleUp    && i <= (7-y)&& y+i <= 7) movesList.add(game.chessboard.getSquares()[x  ][y+i]);	// up
			if (!obstacleDown  && i <=    y && y-i >= 0) movesList.add(game.chessboard.getSquares()[x  ][y-i]);	// down
			if (!obstacleLeft  && i <=	  x && x-i >= 0) movesList.add(game.chessboard.getSquares()[x-i][y  ]);	// left
			if (!obstacleRight && i <= (7-x)&& x+i <= 7) movesList.add(game.chessboard.getSquares()[x+i][y  ]);	// right
		}
		
		return movesList;
	}
	
	/**
	 * Method that gets all horizontal and vertical moves on the circular chessboard starting on a certain
	 * position on the chessboard. The moves are stored in an ArrayList containing squares.
	 * @param game the already set up game with its circular chessboard
	 * @param startSquare the square which possible moves are wanted
	 * @return the ArrayList containing the reachable squares
	 */
	ArrayList<Square> getHorizontalAndVerticalMoves3Person(Game game, Square startSquare)
	{
		ArrayList<Square> movesList = new ArrayList<Square>();
		
		int x = startSquare.getPozX();
		int y = startSquare.getPozY();
		
		int otherSideX = 0;
		int otherSideY = 0;
		
		boolean obstacleUp   		= false;
		boolean obstacleDown 		= false;
		boolean obstacleLeft 		= false;
		boolean obstacleRight		= false;
		
		for (int i = 0; i <= 11; i++)
		{
			if (y+i+1 > 5				) otherSideX = Math.floorMod(    x+12	 , 24);
			if (y+i+1 > 5 && i < (5-y)+6) otherSideY = Math.floorMod(-(i+1-(5-y)),  6);

			// check for obstacles
			if (!obstacleUp && y+i+1 <= 5) obstacleUp	= this.hasObstacleInDir(game, 		   x,    0, 		 y, +i+1);
			if (!obstacleUp && y+i+1 >  5) obstacleUp	= this.hasObstacleInDir(game, otherSideX,	 0, otherSideY,    0);
			if (!obstacleDown 			 ) obstacleDown = this.hasObstacleInDir(game, 		   x,    0, 		 y, -i-1);
			if (!obstacleLeft 			 ) obstacleLeft = this.hasObstacleInDir(game, 		   x, -i-1, 		 y,    0);
			if (!obstacleRight			 ) obstacleRight= this.hasObstacleInDir(game, 		   x, +i+1, 		 y,    0);
			
			// add moves
			if (i < (5-y)+6)
			{				
				if (!obstacleUp   && y+i+1 <= 5) movesList.add(game.chessboard.getSquares()[	x	  ][   y+i+1  ]);	// up to center
				if (!obstacleUp   && y+i+1 >  5) movesList.add(game.chessboard.getSquares()[otherSideX][otherSideY]);	// up otherside
				if (!obstacleDown && y-i-1 >= 0) movesList.add(game.chessboard.getSquares()[	x     ][   y-i-1  ]);	// down
			}
			if (i < 12)
			{
				if(!obstacleLeft ) movesList.add(game.chessboard.getSquares()[Math.floorMod(x-i-1, 24)][	y	  ]);	// left
				if(!obstacleRight) movesList.add(game.chessboard.getSquares()[Math.floorMod(x+i+1, 24)][	y	  ]);	// right
			}
		}
		
		return movesList;
	}
	
	/**
	 * Method that gets all diagonal moves on the standard chessboard starting on a certain
	 * position on the chessboard. The moves are stored in an ArrayList containing squares.
	 * @param game the already set up game with its standard chessboard
	 * @param startSquare the square which possible moves are wanted
	 * @return the ArrayList containing the reachable squares
	 */
	ArrayList<Square> getDiagonalMoves2Person(Game game, Square startSquare)
	{
		ArrayList<Square> movesList = new ArrayList<Square>();
		
		int x = startSquare.getPozX();
		int y = startSquare.getPozY();
		
		boolean obstacleLeftup   = false;
		boolean obstacleLeftdown = false;
		boolean obstacleRightUp  = false;
		boolean obstacleRightDown= false;
		
		for (int i = 1; i <= 7; i++)
		{
			if (!obstacleLeftup   ) obstacleLeftup   = this.hasObstacleInDir(game, x, -i, y, +i);
			if (!obstacleLeftdown ) obstacleLeftdown = this.hasObstacleInDir(game, x, -i, y, -i);
			if (!obstacleRightUp  ) obstacleRightUp  = this.hasObstacleInDir(game, x, +i, y, +i);
			if (!obstacleRightDown) obstacleRightDown= this.hasObstacleInDir(game, x, +i, y, -i);
			
			if (i <= 	x )
			{
				if (!obstacleLeftup    && y+i <= 7) movesList.add(game.chessboard.getSquares()[x-i][y+i]);	// leftup
				if (!obstacleLeftdown  && y-i >= 0) movesList.add(game.chessboard.getSquares()[x-i][y-i]);	// leftdown
			}
			if (i <= (7-x))
			{
				if (!obstacleRightUp   && y+i <= 7) movesList.add(game.chessboard.getSquares()[x+i][y+i]);	// rightup
				if (!obstacleRightDown && y-i >= 0) movesList.add(game.chessboard.getSquares()[x+i][y-i]);	// rightdown
			}
		}
		
		return movesList;
	}
	
	/**
	 * Method that gets all diagonal moves on the circular chessboard starting on a certain
	 * position on the chessboard. The moves are stored in an ArrayList containing squares.
	 * @param game the already set up game with its circular chessboard
	 * @param startSquare the square which possible moves are wanted
	 * @return the ArrayList containing the reachable squares
	 */
	ArrayList<Square> getDiagonalMoves3Person(Game game, Square startSquare)
	{
		ArrayList<Square> movesList = new ArrayList<Square>();
		
		int x = startSquare.getPozX();
		int y = startSquare.getPozY();
		
		int sameSideLeftupX = 0;
		int sameSideRightupX= 0;
		int leftOtherSideX  = 0;
		int rightOtherSideX = 0;
		int otherSideY = 0;
		
		boolean obstacleLeftup   = false;
		boolean obstacleLeftdown = false;
		boolean obstacleRightUp  = false;
		boolean obstacleRightDown= false;
		
		for (int i = 0; i <= 10; i++)
		{
			// calculate values when crossing middle circle
			if (y+i+1 == 6)
			{
				sameSideLeftupX = Math.floorMod(x-i, 24);
				sameSideRightupX= Math.floorMod(x+i, 24);
			}
			
			if(i < (5-y)+6 && y+i+1 > 5) otherSideY = Math.floorMod(-(i+1-(5-y)), 6);
			
			int xAddendLeft = 0;
			int xAddendRight= 0;
			if (y+i+1 > 5)
			{
				if (y == 5)
				{
					xAddendLeft = -i-14;
					xAddendRight= +i+14;
				}
				else
				{
					xAddendLeft = -i-14+(5-y);
					xAddendRight= +i+14-(5-y);
				}
				
				leftOtherSideX  = Math.floorMod(sameSideLeftupX + xAddendLeft, 24);
				rightOtherSideX = Math.floorMod(sameSideRightupX+xAddendRight, 24);
			}
			
			// check for obstacles
			if (!obstacleLeftup   && y+i+1 <= 5) obstacleLeftup   = this.hasObstacleInDir(game, 			   x, 		  -i-1, y, +i+1		 );
			if (!obstacleRightUp  && y+i+1 <= 5) obstacleRightUp  = this.hasObstacleInDir(game, 			   x, 		  +i+1, y, +i+1		 );
			if (!obstacleLeftdown ) 		     obstacleLeftdown = this.hasObstacleInDir(game, 			   x, 		  -i-1, y, -i-1		 );
			if (!obstacleRightDown) 		     obstacleRightDown= this.hasObstacleInDir(game, 			   x, 		  +i+1, y, -i-1		 );
			if (!obstacleLeftup   && y+i+1 >  5) obstacleLeftup   = this.hasObstacleInDir(game,  sameSideLeftupX,  xAddendLeft, y, -i-1+5-2*y);
			if (!obstacleRightUp  && y+i+1 >  5) obstacleRightUp  = this.hasObstacleInDir(game, sameSideRightupX, xAddendRight, y, -i-1+5-2*y);
			
			
			// add moves
			if(i < (5-y)+6)
			{	
				if (y+i+1 <= 5 && !obstacleLeftup   ) movesList.add(game.chessboard.getSquares()[Math.floorMod(x-i-1, 24)][	y+i+1	 ]);	// leftup to center
				if (y+i+1 >  5 && !obstacleLeftup   ) movesList.add(game.chessboard.getSquares()[leftOtherSideX			 ][otherSideY]);	// leftup otherside
				if (y+i+1 <= 5 && !obstacleRightUp  ) movesList.add(game.chessboard.getSquares()[Math.floorMod(x+i+1, 24)][	y+i+1	 ]);	// rightup to center
				if (y+i+1 >  5 && !obstacleRightUp  ) movesList.add(game.chessboard.getSquares()[rightOtherSideX		 ][otherSideY]);	// rightup otherside
			}
			if(i < y)
			{
				if (y-i-1 >= 0 && !obstacleLeftdown ) movesList.add(game.chessboard.getSquares()[Math.floorMod(x-i-1, 24)][	y-i-1	 ]);	// leftdown
				if (y-i-1 >= 0 && !obstacleRightDown) movesList.add(game.chessboard.getSquares()[Math.floorMod(x+i+1, 24)][	y-i-1	 ]);	// rightdown
			}
		}
		
		return movesList;
	}
	
	/**
	 * Method that deletes redundant elements from the input lists. The input lists are ArrayLists
	 * containing squares of the chessboard. Deleting duplicates is needed to test the possible
	 * movements of pieces which vertical and horizontal moves are intersecting (e.g. the piece Queen.)
	 * @param expected list of expected moves
	 * @param result list of real calculated moves
	 */
	void deleteDuplicates(ArrayList<Square> expected, ArrayList<Square> result)
	{
		Set<Square> hExpected = new HashSet<>();
		hExpected.addAll(expected);
		expected.clear();
		expected.addAll(hExpected);
		
		Set<Square> hResult = new HashSet<>();
		hResult.addAll(result);
		result.clear();
		result.addAll(hResult);
	}
}
