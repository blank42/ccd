package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import jchess.Game;
import jchess.GameStandard;
import jchess.Player;
import jchess.Square;

/**
 * Test class for the piece Pawn on the standard chessboard.
 */
@RunWith(Parameterized.class)
public class PawnStandardGameTest {

	Game game;
	Pawn pawn;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Pawn position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Pawn position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{3, 1},				// startposition
			{3, 3},				// middle
			{4, 3},				// blocked
			{5, 3}				// can capture
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Pawn.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KB|__|__|__|__|7
	 * |__|__|__|__|__|__|__|__|6
	 * |__|__|__|__|__|__|__|__|5
	 * |__|__|__|__|NB|__|__|__|4
	 * |__|__|__|X_|__|__|__|__|3
	 * |__|__|__|X_|__|__|__|__|2
	 * |__|__|__|PW|__|__|__|__|1
	 * |__|__|__|__|KW|__|__|__|0
	 *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		helper = new HelperClass();
		
		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		Knight knightB = new Knight(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.getSettings().player[0].goDown = true;
		
		pawn = new Pawn(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[4][0].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[3][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[3][7]);
		
		game.chessboard.getSquares()[4][4].setPiece(knightB);
		knightB.setSquare(game.chessboard.getSquares()[4][4]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
	/**
	 * Test that checks if moves list of the Pawn contains all squares to move on from
	 * a specific position. (x,y)
	 * @throws Exception if the expected moves do not agree with the output moves list
	 */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(pawn);
		pawn.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		boolean obstacleAhead   = helper.hasObstacleInDir(game, x,  0, y, +1);
		boolean obstacleLeftup  = helper.hasObstacleInDir(game, x, -1, y, +1);
		boolean obstacleRightup = helper.hasObstacleInDir(game, x, +1, y, +1);
		
		Player ownPlayer = game.chessboard.getSettings().player[0];
		
		if(y+1 <= 7 && 			  !obstacleAhead  ) expectedList.add(game.chessboard.getSquares()[x  ][y+1]);
		if(y   == 1 && 			  !obstacleAhead  ) expectedList.add(game.chessboard.getSquares()[x  ][y+2]);
		if(y+1 <= 7 && x-1 >= 0 && obstacleLeftup ) if(!game.chessboard.getSquares()[x-1][y+1].getPiece().getPlayer().equals(ownPlayer)) expectedList.add(game.chessboard.getSquares()[x-1][y+1]);
		if(y+1 <= 7 && x+1 <= 7 && obstacleRightup) if(!game.chessboard.getSquares()[x+1][y+1].getPiece().getPlayer().equals(ownPlayer)) expectedList.add(game.chessboard.getSquares()[x+1][y+1]);
		
		ArrayList<Square> resultList = pawn.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
