package jchess.pieces;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

/**
 * Test class for the piece Bishop on the standard chessboard.
 */
public class BishopTest {
	
	Game game;
	Bishop bishop;
	
	/**
	 * Sets up the game and chessboard to enable testing of the Bishop.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KB|__|__|__|__|7
	 * |__|__|__|__|__|__|__|__|6
	 * |__|__|__|__|__|__|__|__|5
	 * |BB|__|__|__|__|__|__|__|4
	 * |__|X_|__|__|__|__|__|__|3
	 * |__|__|X_|__|__|__|__|__|2
	 * |__|__|__|BW|__|__|__|__|1
	 * |__|__|__|__|KW|__|__|__|0
	 *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		
		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		Bishop bishopB = new Bishop(game.chessboard, game.chessboard.getSettings().player[1]);
		bishop = new Bishop(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[4][0].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[3][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[3][7]);
		
		game.chessboard.getSquares()[0][4].setPiece(bishopB);
		bishopB.setSquare(game.chessboard.getSquares()[0][4]);
		
		game.chessboard.getSquares()[3][1].setPiece(bishop);
		bishop.setSquare(game.chessboard.getSquares()[3][1]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
	/**
     * Test that checks if moves list of the Bishop contains all diagonal squares on the
     * standard chessboard to move on from a specific position. (3,1)
     * No move should be in the list if the own King will not be safe anymore after moving. 
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMovesKingNotSafe() throws Exception {
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList.add(game.chessboard.getSquares()[2][2]);
		expectedList.add(game.chessboard.getSquares()[1][3]);
		expectedList.add(game.chessboard.getSquares()[0][4]);
		
		ArrayList<Square> resultList = bishop.allMoves();
		
		// DEBUG
		for(int i = 0; i< resultList.size(); i++)
		{
			if(resultList.get(i)   != null) System.out.println(i + " Result   x: " + resultList.get(i).getPozX() +   " y:" + resultList.get(i).getPozY() +   " length:" + resultList.size());
		}
		for(int i = 0; i< expectedList.size(); i++)
		{
			if(expectedList.get(i) != null) System.out.println(i + " Expected x: " + expectedList.get(i).getPozX() + " y:" + expectedList.get(i).getPozY() + " length:" + expectedList.size());
		}
		
		Assert.assertArrayEquals(expectedList.toArray(), resultList.toArray());
	}

}
