package jchess.pieces;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;

/**
 * Test class for the piece King on the circular chess board.
 */
public class KingCircleGameTest2 {
	
	Game game;
	King king;
	
	/**
	 * Sets up the game and chessboard to enable testing of the King.
	 * For that the king of each player has to be defined.  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		
		King kingWhite = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingGray = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		Queen queenG = new Queen(game.chessboard, game.chessboard.getSettings().player[2]);
		Rook rookB1 = new Rook(game.chessboard, game.chessboard.getSettings().player[1]);
		Rook rookB2 = new Rook(game.chessboard, game.chessboard.getSettings().player[1]);
		
		king = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(kingWhite);
		game.chessboard.setKingBlack(king);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[4][0].setPiece(kingWhite);
		kingWhite.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
		game.chessboard.getSquares()[19][2].setPiece(queenG);
		queenG.setSquare(game.chessboard.getSquares()[19][2]);
		
		game.chessboard.getSquares()[18][0].setPiece(rookB1);
		rookB1.setSquare(game.chessboard.getSquares()[18][0]);
		
		game.chessboard.getSquares()[20][0].setPiece(rookB2);
		rookB2.setSquare(game.chessboard.getSquares()[20][0]);
		
		game.chessboard.getSquares()[19][0].setPiece(king);
		king.setSquare(game.chessboard.getSquares()[19][0]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
    /**
     * Test that checks if the King is checked by an enemies piece.
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testChecked() throws Exception {
		
		boolean result = king.isChecked();
		
		Assert.assertTrue(result);
	}

}
