package jchess.pieces;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;
import jchess.Square;

/**
 * Test class for the piece Guardian on the circular chess board.
 */
public class GuardianCircleGameTest2 {
	
	Game game;
	King kingWhite;
	Guardian guardian;	// G
	HelperClass helper;
	
	/**
	 * Sets up the game and chessboard to enable testing of the Guardian.
	 * For that the king of each player has to be defined.
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		helper = new HelperClass();
		
		kingWhite = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		King kingGray  = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		
		guardian = new Guardian(game.chessboard, game.chessboard.getSettings().player[0]);
		Bishop bishop = new Bishop(game.chessboard, game.chessboard.getSettings().player[1]);
		Pawn pawn = new Pawn(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(kingWhite);
		game.chessboard.setKingBlack(kingBlack);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[7][1].setPiece(kingWhite);
		kingWhite.setSquare(game.chessboard.getSquares()[7][1]);
		
		game.chessboard.getSquares()[19][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[19][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
		game.chessboard.getSquares()[9][3].setPiece(bishop);
		bishop.setSquare(game.chessboard.getSquares()[9][3]);
		
		game.chessboard.getSquares()[7][2].setPiece(pawn);
		pawn.setSquare(game.chessboard.getSquares()[7][2]);
		
		game.chessboard.getSquares()[6][2].setPiece(guardian);
		guardian.setSquare(game.chessboard.getSquares()[6][2]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the Guardian contains all moves starting at the
     * position (6,2).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
			
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList.add(game.chessboard.getSquares()[6][1]);
		
		ArrayList<Square> resultList = guardian.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
