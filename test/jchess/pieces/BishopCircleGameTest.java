package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameCircle;
import jchess.Square;

/**
 * Test class for the piece Bishop on the circular chess board.
 */
@RunWith(Parameterized.class)
public class BishopCircleGameTest {
	
	Game game;
	Bishop bishop;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Bishop position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Bishop position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{3, 3},			// middle
			{2, 0},			// outer edge
			{5, 5},			// inner edge
			{23,0}, {23,1},	// left from wall
			{0, 0}, {0, 1},	// right from wall
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Bishop.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
     * - - - - - - - - 0   23 - - - - - - 
     * � � � � � � 1 � � | � � 22� � � � �		0
     * � � � � 2 � � � � | � � � � 21� � �		1
     * � � � � � � � � � | � � � � � � � �		2
     * � � 3 KW� � � � � | � � � � � � 20�		3
     * � � � � BW� � � � | � � � � � � � �		4
     * � 4 � � � � � � � | � � � � � � KB19		5
     * � � � � � � � � �---� � � � � � � �
     * 5 � � � � � � �       � � � � � � �|18
     *   - - - - - -|- --�-- -|- - - - - - 
     * 6 | � � � � � �       � � � � � � �|17
     * � � � � � � � � �---� � � � � � � �
     * � 7 � � � � � � � | � � � � � � � 16
     * � � � � � � � � � | � � � � � � � �
     * � � 8 � � � � � � | � � � � � � 15�
     * � � � � � � � � � | � � � � � � � �
     * � � � � 9 � � � � | � � � � 14� � �
     * � � � � � � 10�KG | � � 13� � � � �
     * - - - - - - - -11   12 - - - - - -
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameCircle();
		helper = new HelperClass();
		
		King kingWhite = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingBlack = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		King kingGray  = new King(game.chessboard, game.chessboard.getSettings().player[2]);
		
		bishop = new Bishop(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(kingWhite);
		game.chessboard.setKingBlack(kingBlack);
		game.chessboard.setKingGray(kingGray);
		
		game.chessboard.getSquares()[3][0].setPiece(kingWhite);
		kingWhite.setSquare(game.chessboard.getSquares()[3][0]);
		
		game.chessboard.getSquares()[19][0].setPiece(kingBlack);
		kingBlack.setSquare(game.chessboard.getSquares()[19][0]);
		
		game.chessboard.getSquares()[11][0].setPiece(kingGray);
		kingGray.setSquare(game.chessboard.getSquares()[11][0]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the Bishop contains all diagonal moves
     * starting at the position (x,y).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(bishop);
		bishop.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList = helper.getDiagonalMoves3Person(game, bishop.getSquare());
		
		ArrayList<Square> resultList = bishop.allMoves();
		
		helper.deleteDuplicates(expectedList, resultList);
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}

}
