package jchess.pieces;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

public class PawnTest {

	Game game;
	Pawn pawn;
	
	/**
	 * Sets up the game and chessboard to enable testing of the Pawn.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KW|__|__|__|__|7
	 * |__|__|__|__|__|__|__|__|6
	 * |__|__|__|__|RW|__|__|__|5
	 * |__|__|__|__|__|__|__|__|4
	 * |__|__|__|__|X_|__|__|__|3
	 * |__|__|__|__|X_|BW|__|__|2
	 * |__|__|__|__|PB|__|__|__|1
	 * |__|__|__|__|KB|__|__|__|0
	 *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		
		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		Rook rookW = new Rook(game.chessboard, game.chessboard.getSettings().player[0]);
		Bishop bishopW = new Bishop(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.getSettings().player[1].goDown = true;
		
		pawn = new Pawn(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[3][7].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[3][7]);
		
		game.chessboard.getSquares()[4][0].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[4][5].setPiece(rookW);
		rookW.setSquare(game.chessboard.getSquares()[4][5]);
		
		game.chessboard.getSquares()[5][2].setPiece(bishopW);
		bishopW.setSquare(game.chessboard.getSquares()[5][2]);
		
		game.chessboard.getSquares()[4][1].setPiece(pawn);
		pawn.setSquare(game.chessboard.getSquares()[4][1]);
		
		pawn.wasMotion = false;
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
	/**
	 * Test that checks if moves list of the Pawn contains all possible squares to move
     * on from a specific position. (4,1)
     * No move should be in the list if the own King will not be safe anymore after moving.
	 * @throws Exception if the expected moves do not agree with the output moves list
	 */
	@Test
	public void testAllMovesKingNotSafe() throws Exception {
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList.add(game.chessboard.getSquares()[4][2]);
		expectedList.add(game.chessboard.getSquares()[4][3]);
		
		ArrayList<Square> resultList = pawn.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}
}
