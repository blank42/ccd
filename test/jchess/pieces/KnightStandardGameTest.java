package jchess.pieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

/**
 * Test class for the piece Knight on the standard chessboard.
 */
@RunWith(Parameterized.class)
public class KnightStandardGameTest {
	
	Game game;
	Knight knight;
	HelperClass helper;
	
	/**
	 * Defines the first element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * x is the x-coordinate of the Knight position.
	 */
	@Parameter(0)
	public int x;
	
	/**
	 * Defines the second element of a parameter set of the parameter list.
	 * It is used to parameterize the moves positions for the piece.
	 * y is the y-coordinate of the Knight position.
	 */
	@Parameter(1)
	public int y;
	
	/**
	 * Defines sets of parameters to execute as own test case.
	 * @return list of parameter sets
	 */
	@Parameters
	public static Collection<Object[]> data() {
		
		return Arrays.asList(new Object[][] {
			{0, 0}, {0, 7}, {7, 0}, {7, 7},		// corners
			{3, 0}, {0, 3}, {2, 7}, {7, 2},		// edges
			{4, 4}								// middle
		});
	}
	
	/**
	 * Sets up the game and chessboard to enable testing of the Knight.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
	 * |__|__|__|KB|__|__|__|__|7
	 * |__|__|__|__|__|__|__|__|6
	 * |__|__|2_|__|3_|__|__|__|5
	 * |__|1_|__|__|__|4_|__|__|4
	 * |__|__|__|NW|__|__|__|__|3
	 * |__|8_|__|__|__|5_|__|__|2
	 * |__|__|7_|__|6_|__|__|__|1
	 * |__|__|__|__|KW|__|__|__|0
	 *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		helper = new HelperClass();
		
		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		knight = new Knight(game.chessboard, game.chessboard.getSettings().player[0]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[4][0].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[3][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[3][7]);
		
		game.chessboard.getSquares()[3][3].setPiece(knight);
		knight.setSquare(game.chessboard.getSquares()[3][3]);
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		helper = null;
		
	}
	
    /**
     * Test that checks if moves list of the Knight contains all 8 possible moves starting
     * at the position (x,y).
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMoves() throws Exception {
		
		game.chessboard.getSquares()[x][y].setPiece(knight);
		knight.setSquare(game.chessboard.getSquares()[x][y]);
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		boolean obstacleOn1 = helper.hasObstacleInDir(game, x, -2, y, +1);
		boolean obstacleOn2 = helper.hasObstacleInDir(game, x, -1, y, +2);
		boolean obstacleOn3 = helper.hasObstacleInDir(game, x, +1, y, +2);
		boolean obstacleOn4 = helper.hasObstacleInDir(game, x, +2, y, +1);
		boolean obstacleOn5 = helper.hasObstacleInDir(game, x, +2, y, -1);
		boolean obstacleOn6 = helper.hasObstacleInDir(game, x, +1, y, -2);
		boolean obstacleOn7 = helper.hasObstacleInDir(game, x, -1, y, -2);
		boolean obstacleOn8 = helper.hasObstacleInDir(game, x, -2, y, -1);
		
		if(x-2 >= 0 && y+1 <= 7 && !obstacleOn1) expectedList.add(game.chessboard.getSquares()[x-2][y+1]);	// 1
		if(x-1 >= 0 && y+2 <= 7 && !obstacleOn2) expectedList.add(game.chessboard.getSquares()[x-1][y+2]);	// 2
		if(x+1 <= 7 && y+2 <= 7 && !obstacleOn3) expectedList.add(game.chessboard.getSquares()[x+1][y+2]);	// 3
		if(x+2 <= 7 && y+1 <= 7 && !obstacleOn4) expectedList.add(game.chessboard.getSquares()[x+2][y+1]);	// 4
		if(x+2 <= 7 && y-1 >= 0 && !obstacleOn5) expectedList.add(game.chessboard.getSquares()[x+2][y-1]);	// 5
		if(x+1 <= 7 && y-2 >= 0 && !obstacleOn6) expectedList.add(game.chessboard.getSquares()[x+1][y-2]);	// 6
		if(x-1 >= 0 && y-2 >= 0 && !obstacleOn7) expectedList.add(game.chessboard.getSquares()[x-1][y-2]);	// 7
		if(x-2 >= 0 && y-1 >= 0 && !obstacleOn8) expectedList.add(game.chessboard.getSquares()[x-2][y-1]);	// 8

		ArrayList<Square> resultList = knight.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}
}
