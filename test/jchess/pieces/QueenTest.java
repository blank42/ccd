package jchess.pieces;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jchess.Game;
import jchess.GameStandard;
import jchess.Square;

public class QueenTest {
	
	Game game;
	Queen queen;
	
	/**
	 * Sets up the game and chessboard to enable testing of the Queen.
	 * For that the king of each player has to be defined.
	 * 
	 * An example set up is given by:
	 * 
     * |__|__|__|__|KB|__|__|__|7
     * |__|__|__|__|__|__|__|__|6
     * |__|__|__|__|__|__|__|__|5
     * |__|__|__|__|__|__|__|__|4
     * |__|__|__|__|__|__|__|BB|3
     * |__|__|__|__|__|__|QW|__|2
     * |__|__|__|__|__|__|__|__|1
     * |__|__|__|__|KW|__|__|__|0
     *  0  1  2  3  4  5  6  7
     *  
	 * @throws Exception if something went wrong during the setting up process
	 */
	@Before
	public void setUp() throws Exception {
		
		game = new GameStandard();
		King kingW = new King(game.chessboard, game.chessboard.getSettings().player[0]);
		King kingB = new King(game.chessboard, game.chessboard.getSettings().player[1]);
		queen = new Queen(game.chessboard, game.chessboard.getSettings().player[0]);
		Bishop bishopB = new Bishop(game.chessboard, game.chessboard.getSettings().player[1]);
		
		game.chessboard.setKingWhite(kingW);
		game.chessboard.setKingBlack(kingB);
		
		game.chessboard.getSquares()[4][0].setPiece(kingW);
		kingW.setSquare(game.chessboard.getSquares()[4][0]);
		
		game.chessboard.getSquares()[4][7].setPiece(kingB);
		kingB.setSquare(game.chessboard.getSquares()[4][7]);
		
		game.chessboard.getSquares()[7][3].setPiece(bishopB);
		bishopB.setSquare(game.chessboard.getSquares()[7][3]);
		
		game.chessboard.getSquares()[6][2].setPiece(queen);
		queen.setSquare(game.chessboard.getSquares()[6][2]);
		
	}
	
	/**
	 * Deletes the game which was temporarily set up for the test.
	 * @throws Exception if something went wrong during deletion process
	 */
	@After
	public void tearDown() throws Exception {
		
		game = null;
		
	}
	
	/**
     * Test that checks if moves list of the Queen contains all possible squares to move
     * on from a specific position. (3,3)
     * No move should be in the list if the own King will not be safe anymore after moving. 
     * @throws Exception if the expected moves do not agree with the output moves list
     */
	@Test
	public void testAllMovesKingNotSafe() throws Exception {
		
		ArrayList<Square> expectedList = new ArrayList<Square>();
		
		expectedList.add(game.chessboard.getSquares()[5][1]);
		expectedList.add(game.chessboard.getSquares()[7][3]);
		
		ArrayList<Square> resultList = queen.allMoves();
		
		Assert.assertTrue(expectedList.containsAll(resultList) && resultList.containsAll(expectedList));
	}
}
